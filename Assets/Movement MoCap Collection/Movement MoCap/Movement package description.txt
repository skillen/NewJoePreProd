Movement MoCap

This package features a collection of 38 fbx animations for a bipedal humanoid character walking, running and performing other general movement actions.

Walking 01: Standard walking animation.

Walking 02: Alternate standard walking animation.

Cautious Walk: Walking cautiously forward and then backwards.

Cautious Walk Forward: Walking cautiously forward.

Cautious Walk Backward: Walking cautiously backward.

Nervous Walk 01: Walking forward nervously then turn around and walk the in opposite direction.

Nervous Walk 01 Forward: Walking forward nervously.

Nervous Walk 01 Turn Around: Turning around then walking forward nervously.

Nervous Walk 02: Pacing back and forth cautiously.

Running 01: Running back and forth.

Running 02: Alternate running back and forth.

Running 03: Standard running animation.

Running 04 v02: (Updated) Alternate standard running animation.

Running 05: Fast Running Animation.

Running Stop: Coming to an abrupt stop while running.

Standing Turns Left: Left stationary turns at 90, 135 and 180 degrees.

Standing Turns Left 90: Left stationary turns at 90 degrees.

Standing Turns Left 135: Left stationary turns at 135 degrees.

Standing Turns Left 180: Left stationary turns at 180 degrees.

Standing Turns Right: Right stationary turns at 90, 135 and 180 degrees.

Standing Turns Right 90: Right stationary turns at 90 degrees.

Standing Turns Right 135: Right stationary turns at 135 degrees.

Standing Turns Right 180: Right stationary turns at 180 degrees.

Turn and Walk Left 90: Turning left at 90 degrees and walking.

Turn and Walk Left 135: Turning left at 135 degrees and walking.

Turn and Walk Left 180: Turning left at 180 degrees and walking.

Turn and Walk Right 90: Turning right at 90 degrees and walking.

Turn and Walk Right 135: Turning right at 135 degrees and walking.

Turn and Walk Right 180: Turning right at 180 degrees and walking.

Wade Through Water 01: Wading through knee deep water.

Wade Through Water 02: Wading through waist deep water.

Walk and Look Around 01: Observing environment while walking.

Walk and Look Around 02: Alternate observing environment while walking.

Walk Stop Look Around: Walking forward to a stop then looking around.

Walk Stop Look Down: Walking forward to a stop then looking down.

Walking Across Beam: Walking across a thin beam.

Walking with Object 01: Walking while holding an object.

Walking with Object 02: Alternate walking while holding an object.

If you have any questions feel free to contact us at support@morromotion.com
