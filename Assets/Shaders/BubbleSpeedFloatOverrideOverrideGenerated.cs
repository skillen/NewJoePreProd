using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_BubbleSpeed", MaterialPropertyFormat.Float)]
    struct BubbleSpeedFloatOverride : IComponentData
    {
        public float Value;
    }
}
