using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_Plane", MaterialPropertyFormat.Float4)]
    struct PlaneVector4Override : IComponentData
    {
        public float4 Value;
    }
}
