using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_BubbleBrightness", MaterialPropertyFormat.Float)]
    struct BubbleBrightnessFloatOverride : IComponentData
    {
        public float Value;
    }
}
