using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_Fill", MaterialPropertyFormat.Float)]
    struct FillFloatOverride : IComponentData
    {
        public float Value;
    }
}
