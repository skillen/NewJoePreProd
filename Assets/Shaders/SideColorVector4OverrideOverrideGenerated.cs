using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_SideColor", MaterialPropertyFormat.Float4)]
    struct SideColorVector4Override : IComponentData
    {
        public float4 Value;
    }
}
