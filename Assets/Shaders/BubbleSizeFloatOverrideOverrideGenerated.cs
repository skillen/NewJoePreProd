using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_BubbleSize", MaterialPropertyFormat.Float)]
    struct BubbleSizeFloatOverride : IComponentData
    {
        public float Value;
    }
}
