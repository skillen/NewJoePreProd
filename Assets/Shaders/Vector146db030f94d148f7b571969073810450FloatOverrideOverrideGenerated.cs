using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("Vector1_46db030f94d148f7b571969073810450", MaterialPropertyFormat.Float)]
    struct Vector146db030f94d148f7b571969073810450FloatOverride : IComponentData
    {
        public float Value;
    }
}
