using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_TopColor", MaterialPropertyFormat.Float4)]
    struct TopColorVector4Override : IComponentData
    {
        public float4 Value;
    }
}
