using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_WobbleX", MaterialPropertyFormat.Float)]
    struct WobbleXFloatOverride : IComponentData
    {
        public float Value;
    }
}
