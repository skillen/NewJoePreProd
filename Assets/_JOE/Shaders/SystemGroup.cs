using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

[ExecuteAlways]
[UpdateAfter(typeof(TransformSystemGroup))]
class AnimateMatOverride : AnimateMatOverrideBase
{
}

