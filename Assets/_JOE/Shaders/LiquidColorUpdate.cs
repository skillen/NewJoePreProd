using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class LiquidColorUpdate : MonoBehaviour
{
    [SerializeField]
    private ConvertedEntityHolder convertedEntityHolder;
    [SerializeField]
    private VolumetricFlow vol;

    public Color liquidColor;


    Entity entity;
    EntityManager entityManager;




    //public Material mat;
    //public MaterialOverride mat;


    // Start is called before the first frame update
    void Start()
    {
        entity = convertedEntityHolder.GetEntity();
        entityManager = convertedEntityHolder.GetEntityManager();
    }

    // Update is called once per frame
    void Update()
    {
        Plane plane = new Plane(transform.up, transform.position);

        Vector4 planeRepresentation = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);

        if (vol != null)
        {
            liquidColor = vol.liquidColor;
        }

        float4 ColorValues = new float4(liquidColor.r, liquidColor.g, liquidColor.b, liquidColor.a);

        entityManager.SetComponentData(entity, new CutoffColor { Value = ColorValues });


    }
}
