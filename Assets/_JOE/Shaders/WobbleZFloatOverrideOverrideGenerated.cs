using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_WobbleZ", MaterialPropertyFormat.Float)]
    struct WobbleZFloatOverride : IComponentData
    {
        public float Value;
    }
}
