using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_ShallowColor", MaterialPropertyFormat.Float4)]
    struct ShallowColorVector4Override : IComponentData
    {
        public float4 Value;
    }
}
