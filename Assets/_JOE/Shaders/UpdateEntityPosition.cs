using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class UpdateEntityPosition : MonoBehaviour
{
    [SerializeField]
    private ConvertedEntityHolder convertedEntityHolder;

    Entity entity;
    EntityManager entityManager;




    //public Material mat;
    //public MaterialOverride mat;


    // Start is called before the first frame update
    void Start()
    {
        entity = convertedEntityHolder.GetEntity();
        entityManager = convertedEntityHolder.GetEntityManager();
    }

    // Update is called once per frame
    void Update()
    {
       

        entityManager.SetComponentData(entity, new Rotation { Value = gameObject.transform.rotation });
        entityManager.SetComponentData(entity, new Translation { Value = gameObject.transform.position });



    }
}
