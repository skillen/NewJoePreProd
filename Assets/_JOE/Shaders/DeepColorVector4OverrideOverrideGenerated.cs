using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_DeepColor", MaterialPropertyFormat.Float4)]
    struct DeepColorVector4Override : IComponentData
    {
        public float4 Value;
    }
}
