using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class MoveLiquidSurface : MonoBehaviour
{

    [SerializeField]
    private ConvertedEntityHolder convertedEntityHolder;

    Entity entity;
    EntityManager entityManager;


    // Start is called before the first frame update
    void Start()
    {
        entity = convertedEntityHolder.GetEntity();
        entityManager = convertedEntityHolder.GetEntityManager();
    }

    // Update is called once per frame
    void Update()
    {
       


        CutoffPlane planeLocation = entityManager.GetComponentData<CutoffPlane>(entity);

        float surfaceHeight = -1 * planeLocation.Value.w;

        Debug.Log(" plane location from entity is " + surfaceHeight);

        

        transform.position = new Vector3(transform.position.x, surfaceHeight, transform.position.z);



    }
}
