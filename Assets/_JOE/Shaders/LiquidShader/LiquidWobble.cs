using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;
using Unity.Transforms;

[MaterialProperty("_WobbleX", MaterialPropertyFormat.Float)]
public struct WobbleX : IComponentData
{
    public float Value;

}

[MaterialProperty("_WobbleZ", MaterialPropertyFormat.Float)]
public struct WobbleZ : IComponentData
{
    public float Value;

}



public class LiquidWobble : MonoBehaviour
{
    public GameObject wobbleObject;


    [SerializeField]
    private ConvertedEntityHolder convertedEntityHolder;

    Entity entity;
    EntityManager entityManager;

    Renderer rend;
    Vector3 lastPos;
    Vector3 velocity;
    Vector3 lastRot;
    Vector3 angularVelocity;
    public float MaxWobble = 0.03f;
    public float WobbleSpeed = 1f;
    public float Recovery = 1f;
    [SerializeField]
    float wobbleAmountX;
    [SerializeField]
    float wobbleAmountZ;
    float wobbleAmountToAddX;
    float wobbleAmountToAddZ;
    float pulse;
    float time = 0.5f;

    // Use this for initialization
    void Start()
    {

        entity = convertedEntityHolder.GetEntity();
        entityManager = convertedEntityHolder.GetEntityManager();


        //rend = GetComponent<Renderer>();
    }
    private void Update()
    {
        time += Time.deltaTime;
        // decrease wobble over time
        wobbleAmountToAddX = Mathf.Lerp(wobbleAmountToAddX, 0, Time.deltaTime * (Recovery));
        wobbleAmountToAddZ = Mathf.Lerp(wobbleAmountToAddZ, 0, Time.deltaTime * (Recovery));

        // make a sine wave of the decreasing wobble
        pulse = 2 * Mathf.PI * WobbleSpeed;
        wobbleAmountX = wobbleAmountToAddX * Mathf.Sin(pulse * time);
        wobbleAmountZ = wobbleAmountToAddZ * Mathf.Sin(pulse * time);

        ////update the rotation and position of the entity

        entityManager.SetComponentData(entity, new Rotation { Value = gameObject.transform.rotation });
        entityManager.SetComponentData(entity, new Translation { Value = gameObject.transform.position });


        // send it to the shader

        entityManager.SetComponentData(entity, new WobbleX { Value = wobbleAmountX });
        entityManager.SetComponentData(entity, new WobbleZ { Value = wobbleAmountZ });

        //wobble the gameobject if it exists

        if (wobbleObject != null)
        {



            //Vector3 finalRotation = wobbleObject.transform.localEulerAngles +
            //   (new Vector3(wobbleObject.transform.localEulerAngles.x, 0, 0) * wobbleAmountX) +
            //   (new Vector3(0, 0, wobbleObject.transform.localEulerAngles.z) * wobbleAmountZ);


            //(Vector3.right * wobbleAmountZ) + (Vector3.back * wobbleAmountX) ;



            //wobbleObject.transform.RotateAround(wobbleObject.transform.position, Vector3.right, wobbleAmountX);
            //wobbleObject.transform.RotateAround(wobbleObject.transform.position, Vector3.forward, wobbleAmountZ);
            //wobbleObject.transform.Rotate(Vector3.forward, wobbleAmountX, Space.Self);

        }

        //rend.material.SetFloat("_WobbleX", wobbleAmountX);
        //rend.material.SetFloat("_WobbleZ", wobbleAmountZ);

        // velocity
        velocity = (lastPos - transform.position) / Time.deltaTime;
        angularVelocity = transform.rotation.eulerAngles - lastRot;


        // add clamped velocity to wobble
        wobbleAmountToAddX += Mathf.Clamp((velocity.x + (angularVelocity.z * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);
        wobbleAmountToAddZ += Mathf.Clamp((velocity.z + (angularVelocity.x * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);

        // keep last position
        lastPos = transform.position;
        lastRot = transform.rotation.eulerAngles;
    }



}