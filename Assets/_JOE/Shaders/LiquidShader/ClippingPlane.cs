﻿using Unity.Entities;
using UnityEngine;




public class ClippingPlane : MonoBehaviour
{

    [SerializeField]
    private ConvertedEntityHolder convertedEntityHolder;
    [SerializeField]
    private VolumetricFlow vol;


    Entity entity;
    EntityManager entityManager;

    [Range(0, 1)]
    public float fillPercent;



    //public Material mat;
    //public MaterialOverride mat;


    // Start is called before the first frame update
    void Start()
    {
        entity = convertedEntityHolder.GetEntity();
        entityManager = convertedEntityHolder.GetEntityManager();
    }

    // Update is called once per frame
    void Update()
    {
        Plane plane = new Plane(transform.up, transform.position);

        Vector4 planeRepresentation = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);

        if (vol != null)
        {
            fillPercent = vol.FillPercent;
        }


        entityManager.SetComponentData(entity, new FillLevel { Value = fillPercent });

        entityManager.GetComponentData<CutoffPlane>(entity);


        //entityManager.SetComponentData(entity, new CutoffPlane { Value = planeRepresentation });

    }
}
