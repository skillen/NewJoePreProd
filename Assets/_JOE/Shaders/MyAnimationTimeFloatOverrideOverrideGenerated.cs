using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_MyAnimationTime", MaterialPropertyFormat.Float)]
    struct MyAnimationTimeFloatOverride : IComponentData
    {
        public float Value;
    }
}
