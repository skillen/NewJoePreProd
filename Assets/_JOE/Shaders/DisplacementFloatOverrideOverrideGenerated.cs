using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_Displacement", MaterialPropertyFormat.Float)]
    struct DisplacementFloatOverride : IComponentData
    {
        public float Value;
    }
}
