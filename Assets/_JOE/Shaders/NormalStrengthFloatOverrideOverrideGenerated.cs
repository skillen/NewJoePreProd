using Unity.Entities;
using Unity.Mathematics;

namespace Unity.Rendering
{
    [MaterialProperty("_NormalStrength", MaterialPropertyFormat.Float)]
    struct NormalStrengthFloatOverride : IComponentData
    {
        public float Value;
    }
}
