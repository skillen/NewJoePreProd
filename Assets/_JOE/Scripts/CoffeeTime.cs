﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using com.ootii.Messages;

namespace Coffee {

	public class CoffeeTime : MonoBehaviour {

		[SerializeField] private float minTimeScale = 0.3f;
		[SerializeField] private float slowTimeSeconds = 2.0f;

		private float _elapsedUnscaledTime = Mathf.Infinity;
		private bool _isPaused;
		private float _timeScaleBeforePause;    // Used for restoring proper time scale.

		private static CoffeeTime _instance = null;
		public static CoffeeTime Instance { get { return _instance; } }

		void Awake () {

			if (_instance == null) {
				_instance = this;
			}
		}

		void OnEnable () {

            MessageDispatcher.AddListener(MessageDatabase.level_completed, OnLevelComplete);
			MessageDispatcher.AddListener(MessageDatabase.level_failed, OnGameOver);	// Fire, water, etc.
			MessageDispatcher.AddListener(MessageDatabase.level_paused, OnGamePaused);
			MessageDispatcher.AddListener(MessageDatabase.level_resumed, OnGameResumed);
		}

		void OnDisable () {

			MessageDispatcher.RemoveListener(MessageDatabase.level_failed, OnGameOver);    // Fire, water, etc.
			MessageDispatcher.RemoveListener(MessageDatabase.level_paused, OnGamePaused);
			MessageDispatcher.RemoveListener(MessageDatabase.level_resumed, OnGameResumed);
            MessageDispatcher.RemoveListener(MessageDatabase.level_completed, OnLevelComplete);
        }

		void Update () {

			if (!_isPaused) {

				float targetTimeScale = _elapsedUnscaledTime < slowTimeSeconds ? minTimeScale : 1f;
				_elapsedUnscaledTime += Time.unscaledDeltaTime;
				Time.timeScale = Mathf.MoveTowards(Time.timeScale, targetTimeScale, Time.unscaledDeltaTime);
			}
		}

		public void SlowTime () {

			_elapsedUnscaledTime = 0f;
		}

		public void ForceDefaultTimeScale () {

			Time.timeScale = 1f;
			_elapsedUnscaledTime = Mathf.Infinity;
		}

		private void OnGamePaused (IMessage message) {

			_isPaused = true;
			_timeScaleBeforePause = Time.timeScale;
			Time.timeScale = 0f;
		}

		private void OnGameResumed (IMessage message) {

			Time.timeScale = _timeScaleBeforePause;
			_isPaused = false;
		}

        private void OnLevelComplete (IMessage message) {

			_isPaused = true;
			Time.timeScale = 0f;
		}

		private void OnGameOver (IMessage message) {

			_isPaused = true;
			Time.timeScale = 0f;
		}
	}
}