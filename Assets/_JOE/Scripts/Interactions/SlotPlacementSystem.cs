﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Rewired;

public class SlotPlacementSystem : MonoBehaviour {

    [Header("Scene references")]
    public Transform placementPosition;


    [Header("Project references")]
    public Color defaultColor;
    public Color eligibleColor;
    public Color occupiedColor;
    public float gridBreakForce = 10f;
    public float gridBreakTorque = 10f;


    [Header("Debug")]
    public List<Slot> slotsList;
    public List<PlaceableItem> itemsOnGrid = new List<PlaceableItem>();

    private Slot[,] slotsArray;
    private bool placementEnabled = false;
    private PlaceableItem itemToPlace;
    private List<Slot> slotsToPlaceOn = new List<Slot>();


    private Slot FirstOpenSlot => slotsList.Find(x => x.IsOccupied == false);
    private float GetMaxLenght(Vector3 v3) => Mathf.Max(Mathf.Max(v3.x, v3.y), v3.z);
    private int NeededSlots(PlaceableItem item, Slot slot) => Mathf.CeilToInt(ObjectLegth(item) / slot.BoundsLegth);
    private float yOffset;

    private Player _playerInput;
    [SerializeField] private int PlayerID = 0;





    private void Start() {
        _playerInput = ReInput.players.GetPlayer(PlayerID);

        ListSlots();
    }


    private void Update() {
        if (placementEnabled) {
            if (_playerInput.GetButton("Place")) {
                StartCoroutine(Place());
            }
        }
    }


    private void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            PlayerInventory inventory = other.GetComponent<PlayerInventory>();
            if (inventory?.ItemsInHand.Count > 0) {

                itemToPlace = inventory.ItemsInHand[0].GetComponent<PlaceableItem>();

                List<Slot> slots = FindFreeSlots(NeededSlots(itemToPlace, FirstOpenSlot));
                if (slots.Count > 0) {
                    EnablePlacement(slots);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            DisablePlacement();
        }
    }

    private void ListSlots() {
        slotsList = GetComponentsInChildren<Slot>().ToList();

        int rows = slotsList.FindAll(x => x.transform.localPosition.z == slotsList[0].transform.localPosition.z).Count;
        int columns = slotsList.FindAll(x => x.transform.localPosition.x == slotsList[0].transform.localPosition.x).Count;

        slotsArray = new Slot[rows, columns];

        for (int i = 0; i < slotsList.Count; i++) {
            int row = Mathf.FloorToInt(i / columns);
            int column = i % columns;
            slotsArray[row, column] = slotsList[i];
            slotsList[i].PlaceOnGrid(row, column);
        }

        foreach (Slot slot in slotsList) {
            slot.FindNeighbors();
        }

        string msg = "";
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                msg += ($" | {slotsArray[r, c].gameObject.name}");
            }
            msg += "\n";
        }
        Debug.Log(msg + "\n\n");

    }

    private List<Slot> FindFreeSlots(int slotsNeeded) {

        List<Slot> slotsFound = new List<Slot>();

        foreach (Slot slot in slotsList.Where(x => x.IsOccupied == false)) {
            slotsFound = new List<Slot>();
            slotsFound.Add(slot);

            List<Slot> slotsHorizontal = slot.FreeSlotsHorizontal();
            if (slotsFound.Count + slotsHorizontal.Count >= slotsNeeded) {
                slotsFound.AddRange(slotsHorizontal);
                break;
            }

            List<Slot> slotsVertical = slot.FreeSlotsVertical();
            if (slotsFound.Count + slotsVertical.Count >= slotsNeeded) {
                slotsFound.AddRange(slotsVertical);
                break;
            }
        }

        List<Slot> necessarySlots = new List<Slot>();
        for (int i = 0; i < slotsNeeded; i++) {
            necessarySlots.Add(slotsFound[i]);
        }

        return necessarySlots;
    }

    private float ObjectLegth(PlaceableItem item) {

        yOffset = item.itemBounds.extents.y;

        return GetMaxLenght(item.itemBounds.size);
    }


    private void EnablePlacement(List<Slot> eligableSlots) {
        placementEnabled = true;

        Debug.Log($"EnablePlacement on slots: {string.Join(", ", eligableSlots.Select(x => x.gameObject.name).ToList())}" + "\n\n");

        Vector3 centre = new Vector3(
            eligableSlots.Average(s => s.BoundsCentre.x),
            eligableSlots.Average(s => s.BoundsCentre.y),
            eligableSlots.Average(s => s.BoundsCentre.z));

        Debug.Log($"Placement centre: {centre}" + "\n\n");

        placementPosition.position = new Vector3(centre.x, centre.y + yOffset, centre.z);

        Vector3 orientation = eligableSlots[eligableSlots.Count - 1].BoundsCentre - eligableSlots[0].BoundsCentre;

        Quaternion offset = Quaternion.Inverse(itemToPlace.pivot.rotation) * itemToPlace.meshes[0].transform.rotation;
        placementPosition.rotation = Quaternion.LookRotation(orientation) * offset;

        slotsToPlaceOn = eligableSlots;
        foreach (Slot slot in eligableSlots) {
            slot.SetColor(eligibleColor);
        }
    }


    private void DisablePlacement() {

        if (itemToPlace != null) {
            placementEnabled = false;
            itemToPlace = null;
            slotsToPlaceOn.Clear();

            foreach (Slot slot in slotsList.Where(x => x.IsOccupied == false)) {
                slot.SetColor(defaultColor);
            }
        }
    }

    private IEnumerator Place() {

        // Debug.Log("Place" + "\n\n");

        placementEnabled = false;
        itemToPlace.placementGrid = this;
        itemToPlace.placedOnSlots = new List<Slot>(slotsToPlaceOn);
        itemsOnGrid.Add(itemToPlace);

        foreach (Slot slot in slotsToPlaceOn) {
            slot.IsOccupied = true;
            slot.SetColor(occupiedColor);
        }

        Coroutine placement = StartCoroutine(itemToPlace.Place(placementPosition));

        while (itemToPlace.statePlacing) {
            yield return null;
        }

        AttachJoint(itemToPlace);

        // Debug.Log("Placement finished" + "\n\n");

        itemToPlace = null;
    }


    public void OnPickUp(PlaceableItem item, List<Slot> occupiedSlots) {

        RemoveJoint(item);

        foreach (Slot slot in occupiedSlots) {
            slot.IsOccupied = false;
            slot.SetColor(defaultColor);
        }

        itemsOnGrid.Remove(item);
        item.placementGrid = null;
        item.placedOnSlots.Clear();

        Debug.Log("OnPickUp" + "\n\n");
    }

    private void AttachJoint(PlaceableItem item) {

        Joint itemJoint = item.GetComponent<Joint>();
        if (itemJoint != null) {
            DestroyImmediate(itemJoint);
        }

        Rigidbody itemRb = item.GetComponent<Rigidbody>();
        if (itemRb != null) {
            DestroyImmediate(itemRb);
        }

        Joint joint = item.gameObject.AddComponent<FixedJoint>();
        Rigidbody rb = this.GetComponent<Rigidbody>();

        joint.connectedBody = rb;
        joint.breakForce = gridBreakForce;
        joint.breakTorque = gridBreakTorque;
    }

    private void RemoveJoint(PlaceableItem item) {
        Joint joint = item.GetComponent<FixedJoint>();
        // Rigidbody rb = item.GetComponent<Rigidbody>();
        joint.connectedBody = null;
        Destroy(joint);
        // Destroy(rb);
    }
}
