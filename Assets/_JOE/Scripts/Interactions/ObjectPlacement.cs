﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using RootMotion.FinalIK;
using System.Threading.Tasks;
using UnityEngine.Events;
using System;

[System.Serializable]
public class PlacementItemEvent : UnityEvent<GameObject>
{
}

public class ObjectPlacement : MonoBehaviour
{

    [Header("Scene references")]

    [TagSelector]
    public string[] triggerTags = new string[] { };

    public List<PlacementTypes> RelevantPlaceableTypes;


    public ColliderEventDispatcher trigger;
    public Transform meshesTransform;
    public Transform placePivot;

    public bool staticPlacement = true;

    public List<PlaceableItem> AttachedItems;

    public PlacementItemEvent ObjectPlacedEvent = new PlacementItemEvent();
    public PlacementItemEvent ObjectRemovedEvent = new PlacementItemEvent();

    [Header("Project references")]
    public Material highlightMaterial;

    [Header("Debug")]
    [SerializeField] int PlayerID = 0;


    private bool placementEnabled = false;
    private bool rotationEnabled = false;

    [SerializeField]
    private InteractionObject interactionObject;
    private InteractionSystem lastIS;
    public InteractionController interactionController;




    //private PlaceableItem placeableItem;

    public List<GameObject> placeableItems;


    private Player _playerInput;
    private MeshFilter placementMesh;
    private List<MeshFilter> meshes;
    private InteractionTrigger.Range[] triggerRanges;



    private void Start()
    {
        //_playerInput = ReInput.players.GetPlayer(PlayerID);

        //meshes = meshesTransform.GetComponentsInChildren<MeshFilter>(true).ToList();

        //foreach (MeshFilter mesh in meshes)
        //{
        //    MeshRenderer renderer = mesh.GetComponent<MeshRenderer>();
        //    renderer.material = highlightMaterial;
        //    renderer.enabled = false;
        //    mesh.gameObject.SetActive(true);
        //}

        //SubscribeToColliderEvents();
        //triggerRanges = trigger.GetComponent<InteractionTrigger>().ranges;
    }

    private void SubscribeToColliderEvents()
    {
        if (trigger != null)
        {
            //trigger.OnTriggerEnterEvent += TriggerEnter;
            trigger.OnTriggerStayEvent += TriggerStay;
            trigger.OnTriggerExitEvent += TriggerExit;
        }
    }

    private void OnDestroy()
    {
        if (trigger != null)
        {
            //trigger.OnTriggerEnterEvent -= TriggerEnter;
            trigger.OnTriggerStayEvent -= TriggerStay;
            trigger.OnTriggerExitEvent -= TriggerExit;
        }
    }


    //this needs to be largely extracted and decoupled
    private void Update()
    {

        //if (placementEnabled)
        //{
        //    if (_playerInput.GetButton("Place"))
        //    {
        //        //StartCoroutine(Place());
        //    }
        //}
        //if (rotationEnabled)
        //{

        //    if (_playerInput.GetButtonUp("Rotate Down"))
        //    {
        //        RotateMesh(Vector3.left);
        //    }
        //    else if (_playerInput.GetButtonUp("Rotate Up"))
        //    {
        //        RotateMesh(Vector3.right);
        //    }
        //    else if (_playerInput.GetButtonUp("Rotate Left"))
        //    {
        //        RotateMesh(Vector3.up);
        //    }
        //    else if (_playerInput.GetButtonUp("Rotate Right"))
        //    {
        //        RotateMesh(Vector3.down);
        //    }
        //    else if (_playerInput.GetButtonUp("Rotate CL"))
        //    {
        //        RotateMesh(Vector3.forward);
        //    }
        //    else if (_playerInput.GetButtonUp("Rotate CC"))
        //    {
        //        RotateMesh(Vector3.back);
        //    }
        //}
    }

    

    bool CheckTypeRelevance(GameObject PropToCheck, out PlaceableItem placeableScript)
    {
        placeableScript = PropToCheck.GetComponent<PlaceableItem>();

        bool propTypeMatches = false;
        if (RelevantPlaceableTypes.Count > 0 && PropToCheck != null)
        {

            if (placeableScript != null)
            {
                //check if the prop types of the prop matches the applicable prop type
                //propTypeMatches = RelevantPlaceableTypes.Intersect(placeableScript.PropTypes).Any();
            }

        }

        return propTypeMatches;
    }

    //activate/deactivate pickuptrigger/colliders for each attached item
    public void PropagatePickupToChildren()
    {
        foreach (PlaceableItem item in AttachedItems)
        {
            if (item.pickUpItem != null)
            {
                //Debug.Log("deactivating colliders and pickup trigger on " + item.pickUpItem.gameObject.name);
                item.pickUpItem.DeactivatePickupTrigger();
            }
        }
    }

    public void PropagateDropToChildren()
    {
        foreach (PlaceableItem item in AttachedItems)
        {
            if (item.pickUpItem != null)
            {
                //Debug.Log("reactivating colliders and pickup trigger on " + item.pickUpItem.gameObject.name);
                item.pickUpItem.ActivatePickupTrigger();
            }
        }
    }




    private void TriggerStay(Collider other)
    {


        //check that there's a relevant tag type
        //and not already contained within the list
        //this is just an early termination check
        if (triggerTags.Contains(other.tag) || placeableItems.Contains(other.gameObject))
        {

            PlaceableItem placeableItem = null;
            //check that there's a placeable item, and that it matches the typing system
            if (CheckTypeRelevance(other.gameObject, out placeableItem))
            {



                if (placeableItem != null && !placeableItem.statePlacing)
                {




                    //add to list of currently placeable items
                    if (!placeableItems.Contains(other.gameObject))
                    {
                        placeableItems.Add(other.gameObject);
                    }









                }
            }

            //retrieve the correct mesh
            MeshFilter matchingMesh = DisplayMesh(placeableItem);

            if (matchingMesh != null)
            {
                //display the mesh and enable placement
                //need to seperate the display from the placement
                EnablePlacement(matchingMesh, true, false);
            }





        }

    }

    private MeshFilter DisplayMesh(PlaceableItem placeableItem)
    {
        MeshFilter matchingMesh = null;

        //come back to this later


        MeshFilter otherMesh = placeableItem.meshRoot.GetComponentInChildren<MeshFilter>();

        //grab the other objects 'shared mesh' dynamically (maybe with an offset) rather than 
        //having a preset number of meshes. Use proptypes or componentTypes instead, then grab the mesh
        //maybe set the shared mesh in the scriptable object?
        foreach (MeshFilter mesh in meshes)
        {
            if (otherMesh.sharedMesh == mesh.sharedMesh)
            {
                matchingMesh = mesh;
            }
        }

        return matchingMesh;
    }

    private void TriggerExit(Collider other)
    {



        if (triggerTags.Contains(other.tag))
        {



            //check that there's a relevant tag type
            //and not already contained within the list
            //this is just an early termination check
            if (triggerTags.Contains(other.tag) || placeableItems.Contains(other.gameObject))
            {

                PlaceableItem placeableItem = null;
                //check that there's a placeable item, and that it matches the typing system
                if (CheckTypeRelevance(other.gameObject, out placeableItem))
                {

                    //placeable item should not be null if the previous check returned true, but why not be extra safe
                    if (placeableItem != null && !placeableItem.statePlacing)
                    {


                        //remove from list of currently placeable items
                        if (placeableItems.Contains(other.gameObject))
                        {
                            placeableItems.Remove(other.gameObject);
                        }



                        //come back to this later

                        /*
                        MeshFilter otherMesh = placeableItem.meshRoot.GetComponentInChildren<MeshFilter>();

                        //grab the other objects 'shared mesh' dynamically (maybe with an offset) rather than 
                        //having a preset number of meshes. Use proptypes or componentTypes instead, then grab the mesh
                        //maybe set the shared mesh in the scriptable object?
                        foreach (MeshFilter mesh in meshes)
                        {
                            if (otherMesh.sharedMesh == mesh.sharedMesh)
                            {
                                matchingMesh = mesh;
                            }
                        }

                        */



                    }
                }



                MeshFilter matchingMesh = null;

                if (matchingMesh != null)
                {
                    EnablePlacement(matchingMesh, false, true);
                }




            }


        }
    }


    private MeshFilter MatchingMesh(Collider other)
    {

        MeshFilter matchingMesh = null;



        PlaceableItem placeableItem = other.GetComponent<PlaceableItem>();

        //if (placeableItem != null && !placeableItem.statePlacing)
        //{

        //    //check if the object has any typing that matches with the acceptable prop types
        //    bool propTypeMatches = RelevantPlaceableTypes.Intersect(placeableItem.PropTypes).Any();

        //    if (propTypeMatches)
        //    {
        //        MeshFilter otherMesh = placeableItem.meshRoot.GetComponentInChildren<MeshFilter>();

        //        //grab the other objects 'shared mesh' dynamically (maybe with an offset) rather than 
        //        //having a preset number of meshes. Use proptypes or componentTypes instead, then grab the mesh
        //        //maybe set the shared mesh in the scriptable object?
        //        foreach (MeshFilter mesh in meshes)
        //        {
        //            if (otherMesh.sharedMesh == mesh.sharedMesh)
        //            {
        //                matchingMesh = mesh;
        //            }
        //        }

        //    }

        //}
        return matchingMesh;
    }


    private void EnablePlacement(MeshFilter mesh, bool enable, bool cancelled)
    {
        Debug.Log("EnablePlacement " + enable + "\n\n");

        placementEnabled = enable;

        // Show UI promt

        mesh.GetComponent<MeshRenderer>().enabled = enable;

        // This was disableing the trigger collider while player was in range. Stops OnTriggerExit call. Not sure why it was necessary. Maybe for multiple objects?
        // EnableTrigger(!enable);

        EnableRotation(enable);

        if (enable)
        {
            placementMesh = mesh;
        }

        else
        {
            placementMesh = null;
        }

        //if (cancelled)
        //{
        //    placeableItem = null;
        //}
    }


    //private IEnumerator Place()
    //{

    //    //Transform meshTransform = placementMesh.transform;

    //    //EnablePlacement(placementMesh, false, false);
    //    //Coroutine placement = StartCoroutine(placeableItem.Place(pivot));

    //    //while (placeableItem.statePlacing)
    //    //{
    //    //    yield return null;
    //    //}
    //    //Debug.Log("Placement finished" + "\n\n");
    //    //placeableItem = null;

    //    yield return null;
    //}





    /*
     * currently this is invoked by proptypefilter and passes in the first relevant
     * prop in the player's inventory
     * see PropTypeFilter proprfilter = new PropTypeFilter().OnAttemptInteraction();  
    */
    public async void PlaceInteractionAsync(GameObject PlacementObject)
    {
        PlaceableItem placeableItem = PlacementObject.GetComponent<PlaceableItem>();
        //potentially call the placement item.drop before calling placement
        //more annoying setup, but avoids the problem
        PickUpItem pickUpItem = placeableItem.pickUpItem;
        if (pickUpItem != null)
        {
            pickUpItem.PuppetMasterDrop();

        }


        Rigidbody rb = PlacementObject.GetComponent<Rigidbody>();

        while (rb == null)
        {
            rb = PlacementObject.GetComponent<Rigidbody>();
            await Task.Yield();
        }


        

        if (rb != null && placeableItem != null)
        {
            //puppetmaster pickup/drop - the puppetmaster system itself
            //adds and removes rigidbody, so this will not work here

            if (rb != null && staticPlacement == true)
            {
                rb.isKinematic = true;
            }


            //set the placeable item's mounting point reference to this
            placeableItem.mountingPoint = this;
            //deactivate the mounting point (since item was successfully placed)
            placeableItem.mountingPoint.gameObject.SetActive(false);



            //// BEHOLD THE PIVOT MADNESS
            PlacementObject.transform.position = placePivot.position - placeableItem.placementOffset;
            //PlacementObject.transform.rotation = Quaternion.Inverse(placePivot.rotation) * (Quaternion.Inverse(placePivot.rotation) /* meshes[0].transform.rotation */);
            PlacementObject.transform.localRotation = placePivot.localRotation;

            //but actually set the target transform as the parent
            PlacementObject.transform.parent = placePivot;

            // IS THIS STILL NECESSARY ???
            // pickUpItem.interactionTrigger.transform.localEulerAngles = -transform.rotation.eulerAngles;

            //invoke the item placed Event
            ObjectPlacedEvent.Invoke(placeableItem.gameObject);
            //invoke the placeable objects behaviors passing this as the gameobject
            placeableItem.ObjectPlacedEvent.Invoke(this.gameObject);

            AttachedItems.Add(placeableItem);
        }



    }

    public void RemoveItem(PlaceableItem placeableItem)
    {
        //reactivate the mounting point when removing the item
        gameObject.gameObject.SetActive(true);
        //invoke the item placed Event
        ObjectRemovedEvent.Invoke(placeableItem.gameObject);
        //invoke the placeable objects behaviors passing this as the gameobject
        placeableItem.ObjectRemovedEvent.Invoke(this.gameObject);
        AttachedItems.Remove(placeableItem);
    }


    private void EnableTrigger(bool active)
    {

        trigger.enabled = active;
    }

    private void EnableRotation(bool enable)
    {
        Debug.Log("EnableRotation: " + enable + "\n\n");
        rotationEnabled = enable;
        // Show UI promt
    }

    public void RotateMesh(Vector3 axis)
    {
        placementMesh.transform.RotateAround(placePivot.transform.position, axis, 90);
    }
}
