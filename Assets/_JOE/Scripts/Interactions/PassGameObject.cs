using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameObjectEvent : UnityEvent<GameObject>
{
}

public class PassGameObject : MonoBehaviour
{

    public GameObject objectToPass;

    public GameObjectEvent ObjectEvent;

    public void FireEvent()
    {
        ObjectEvent.Invoke(objectToPass);
    }
    

}
