using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeDispenser : MonoBehaviour
{
    public VolumetricFlow coffeeVolume;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DispenseCoffee(GameObject coffeeCupObj)
    {
        CoffeeCup coffeeCup = coffeeCupObj.GetComponent<CoffeeCup>();

        if (coffeeCup != null && coffeeVolume != null)
        {
            float cupVol = coffeeCup.VolLiquid.MaxVolume;

            //pass coffee to the coffee cup
            coffeeVolume.PassVolume(cupVol, coffeeCup.VolLiquid);
        }
    }
}
