using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using RootMotion.Dynamics;
using UnityEngine.Events;


public class PlacementInteractionBehav : MonoBehaviour
{

    [TagSelector]
    public string[] triggerTags = new string[] { };

    public List<PropTypes> RelevantPlaceableTypes;


    public List<PuppetMasterProp> usableProps;

    public string animationName;

    public PropInteractionEvent propInteractionEvent;

    private InteractionObject interactionObject;
    public InteractionController interactionController;
    private InteractionSystem lastIS;

    private List<InteractionTarget> targets;
    [SerializeField]
    private Animator charAnim;




    private bool propGrabbed = false;





    private void Awake()
    {
        interactionObject = GetComponentInChildren<InteractionObject>();
        targets = GetComponentsInChildren<InteractionTarget>().ToList();


    }


    public void OnAttemptPlacement()
    {
        usableProps.Clear();



        lastIS = interactionObject.lastUsedInteractionSystem;
        interactionController = lastIS.GetComponentInParent<InteractionController>();
        charAnim = lastIS.GetComponent<Animator>();



        PlayerInventory inventory = interactionController.GetComponent<PlayerInventory>();


        PlaceableItem newItem;
        //get props that are relevant, get their puppetmasterprop components as a list
        //usableProps = inventory.ItemsInHand.Where(item => CheckTypeRelevance(item.gameObject)).Select(item => item.GetComponent<PuppetMasterProp>()).ToList();




        //if either there are not relevant prop types (ie anything is fine)
        //or there is a relevant prop type in the players hand
        if (RelevantPlaceableTypes.Count <= 0 || usableProps.Count > 0)
        {
            //invoke the event with the appropriate prop type
            propInteractionEvent.Invoke(usableProps.First().gameObject);

            //charAnim.SetBool("HammerPose", true);
            if (animationName != null)
            {
                charAnim.Play(animationName);
            }

        }



    }

    public void OnDrop()
    {
        if (propGrabbed)
        {
            Debug.Log("Object dropped" + "\n\n");
            //charAnim.SetBool("HammerPose", false);


        }
    }

    public void SwitchColliders()
    {
        if (!propGrabbed)
        {
            Debug.Log("Switch colliders" + "\n\n");
        }
    }

    bool CheckTypeRelevance(GameObject PropToCheck/*, out PlaceableItem placeableScript*/)
    {
        bool propTypeMatches = false;

        PlaceableItem placeableScript = PropToCheck.GetComponent<PlaceableItem>();

        
        if (RelevantPlaceableTypes.Count > 0 && PropToCheck != null)
        {

            if (placeableScript != null)
            {
                //check if the prop types of the prop matches the applicable prop type
                //propTypeMatches = RelevantPlaceableTypes.Intersect(placeableScript.PropTypes).Any();
            }

        }

        return propTypeMatches;
    }



}

