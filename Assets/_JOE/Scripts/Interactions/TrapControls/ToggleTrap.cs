using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ToggleTrap : MonoBehaviour
{
    public List<ParticleSystem> ParticleSystems;

    public List<DisableEnemies> TrapScripts;

    [SerializeField]
    private bool trapActive;
    // Start is called before the first frame update
    void Start()
    {

        foreach (DisableEnemies trap in TrapScripts)
        {
            trap.TrapIsActive = trapActive;
        }

        foreach (ParticleSystem particleSystem in ParticleSystems)
        {
            if (trapActive)
            {
                particleSystem.Play();
            }
            else
            {
                particleSystem.Pause();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Toggle()
    {
        //toggle the trap state
        trapActive = !trapActive;

        //enable/disable the trap scripts
        foreach (DisableEnemies trap in TrapScripts)
        {
            trap.TrapIsActive = trapActive;
        }

        foreach (ParticleSystem particleSystem in ParticleSystems)
        {
            if (trapActive)
            {
                particleSystem.Play();
            }
            else
            {
                particleSystem.Stop();
            }
        }
    }
}
