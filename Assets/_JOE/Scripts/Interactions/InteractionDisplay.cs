using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionDisplay : MonoBehaviour
{
    public bool displaying = false;

    public MeshFilter DefaultMesh;

    public MeshFilter DisplayMesh;

    public MeshRenderer DisplayRenderer;

    public Transform pivot;

    public float AddToScale;

    [Header("Project references")]
    public Material highlightMaterial;


    // Start is called before the first frame update
    void Start()
    {
        //MeshFilter defaultMesh = meshesTransform.GetComponent<MeshFilter>();

        if (DefaultMesh != null)
        {
            DisplayMesh.mesh = DefaultMesh.mesh;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ActivateDisplay()
    {
        displaying = true;
        //UpdateDisplayMesh();

        //MeshRenderer renderer = pivot.GetComponent<MeshRenderer>();

        if (DisplayRenderer != null)
        {
            DisplayRenderer.material = highlightMaterial;
            //enable mesh renderer on the pivot
            DisplayRenderer.enabled = true;
        }
        else
        {
            Debug.LogError("no meshRenderer found on " + DisplayRenderer.gameObject.name + " from " + gameObject.name);
        }



    }

    public void UpdateDisplayMesh(MeshFilter newMesh)
    {
        GameObject thisGameObject = gameObject;

        //update dispaly mesh
        if (newMesh != null && DisplayRenderer != null)
        {
            //get refs to transforms to adjust them
            Transform sourceTransform = newMesh.GetComponent<Transform>();
            Transform displayTransform = DisplayMesh.GetComponent<Transform>();

            if (sourceTransform != null && displayTransform != null)
            {
                //Debug.Log("adjust scale etc here to " + sourceTransform.localScale.x + "rotation is " + sourceTransform.localEulerAngles.x);
                displayTransform.localRotation = sourceTransform.localRotation;
                displayTransform.localScale = sourceTransform.localScale;
            }

            DisplayMesh.mesh = newMesh.mesh;

        }


    }



    public void DeactivateDisplay()
    {
        displaying = false;

        //reset the display mesh to the default display mesh
        if (DisplayMesh != null && DefaultMesh != null)
        {
            DisplayMesh.mesh = DefaultMesh.mesh;
        }



        //MeshRenderer renderer = pivot.GetComponent<MeshRenderer>();

        if (DisplayRenderer != null)
        {

            //enable mesh renderer on the pivot
            DisplayRenderer.enabled = false;
        }
        else
        {
            Debug.LogError("no meshRenderer found on " + pivot.gameObject.name + " from " + gameObject.name);
        }

    }
}
