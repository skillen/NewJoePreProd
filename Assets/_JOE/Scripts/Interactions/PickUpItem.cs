﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RootMotion.FinalIK;
using RootMotion.Demos;
using RootMotion.Dynamics;
using Rewired;

//[RequireComponent(typeof(InteractionObject))]
// [RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(OffsetPose))]
public class PickUpItem : MonoBehaviour {


    public InteractionObject itemToPickUp;

    public bool holdInRightHand = true;

    public List<PropTypes> proptype;

    public ObjectPlacement PlacementSystem;
    public ComponentActivationManager componentManager;

    public OffsetPose holdPose;
    public float holdWeight;
    [SerializeField] private FullBodyBipedIK ik;

    [SerializeField] private UserControls userInput;

    //prevents immediate pickup after dropping object
    [SerializeField] private bool stateDropping;
    [SerializeField] int PlayerID = 0;
    private Player _playerInput;

    //interaction system parameters
    private InteractionSystem lastIS;
    [HideInInspector] public bool statePlacing = false;

    [HideInInspector] public InteractionTrigger interactionTrigger;

    [SerializeField]
    private bool propGrabbed = false;


    [Header("Puppetmaster settings")]

    [Tooltip("The Prop you wish to pick up.")]
    public bool isPuppetmasterProp;

    [Tooltip("The Prop you wish to pick up.")]
    public PuppetMasterProp prop;

    [Tooltip("The Prop Muscle to connect the pickup item to.")]
    public PropMuscle propMuscle;

    public InteractionTrigger trigger;

    [Header("placement item ref")]
    public PlaceableItem placeableItem;



    private void Start() {
        _playerInput = ReInput.players.GetPlayer(PlayerID);
        holdPose = GetComponent<OffsetPose>();

        if (itemToPickUp == null)
        {
            itemToPickUp = GetComponent<InteractionObject>();
        }

        if (prop == null)
        {
            prop = GetComponent<PuppetMasterProp>();
        }

        if (componentManager == null)
        {
            componentManager = GetComponent<ComponentActivationManager>();
        }

        interactionTrigger = GetComponentInChildren<InteractionTrigger>();

        if (placeableItem == null)
        {
            placeableItem = GetComponent<PlaceableItem>();
        }
        
    }




    private void LateUpdate() {

        if (ik == null) {
            return;
        }

        holdPose.Apply(ik.solver, holdWeight, ik.transform.rotation);
    }


    //called through the item interaction system event
    //event probably fired by interactionController.cs
    public void PickUp() {
        if (isPuppetmasterProp) {
            PuppetMasterPickUp();
        }
        else {
            StartCoroutine("OnPickUp");
        }

    }


    //non puppetmaster version of pick up method
    IEnumerator OnPickUp() {
        // Debug.Log("OnPickUp Message Fired" + "\n\n");

        if (!statePlacing && !stateDropping) {

            // Debug.Log("OnPickUp Start" + "\n\n");
            if (placeableItem != null) {
                placeableItem.PickupFromPlacementGrid();
            }    

            ik = itemToPickUp.lastUsedInteractionSystem.GetComponent<FullBodyBipedIK>();
            lastIS = itemToPickUp.lastUsedInteractionSystem;
            userInput = lastIS.GetComponentInParent<UserControls>();
            addToInventory();

            while (holdWeight < 1f) {
                holdWeight += Time.deltaTime;
                yield return null;
            }


            // Debug.Log("OnPickUp Finished" + "\n\n");
        }
    }



    public void PuppetMasterPickUp() {
        if (!propGrabbed)
        {
            //Debug.Log("Object grabbed" + "\n\n");

            //this is what's breaking the object on drop
            //removeFromPlacementGrid();
            addToInventory();

            lastIS = itemToPickUp.lastUsedInteractionSystem;

            if (propMuscle == null)
            {
                InteractionController interactionController = lastIS.GetComponentInParent<InteractionController>();

                if (holdInRightHand)
                {
                    propMuscle = interactionController.rightHand;
                }
                else
                {
                    propMuscle = interactionController.leftHand;
                }

                

            }
            //rigIK = lastIS.GetComponentInChildren<FullBodyBipedIK>();

            //check if it's a placement object
            if (placeableItem != null)
            {
                placeableItem.RemoveFromSocket();
            }

            DeactivatePickupTrigger();

            AttachToCharacter(true, lastIS);

            propGrabbed = true;
        }
    }

    public void DeactivatePickupTrigger()
    {
        //disable the trigger collider while it's being held
        trigger.gameObject.SetActive(false);

        componentManager.DisableComponents();

        if (PlacementSystem != null)
        {
            //disable anything attached to the placement system
            PlacementSystem.PropagatePickupToChildren();
        }
    }

    //called through InteractionController.cs
    public void Drop() {
        if (isPuppetmasterProp) {
            PuppetMasterDrop();
        }
        else {
            StartCoroutine(OnDrop());
        }

    }

    IEnumerator OnDrop() {
        Debug.Log("Start drop" + "\n\n");
        stateDropping = true;

        //could parent the object to some other object here
        transform.parent = null;
        RemoveFromInventory();

        while (holdWeight > 0f) {
            holdWeight -= Time.deltaTime * 3f;
            yield return null;
        }
        holdWeight = 0f;

        if (placeableItem != null) {
            placeableItem.EnableCollider();
        }

        stateDropping = false;
        Debug.Log("End drop" + "\n\n");
    }



    public void PuppetMasterDrop() {
        if (propGrabbed) {
            //Debug.Log("Object dropped" + "\n\n");            

            RemoveFromInventory();


            AttachToCharacter(false, lastIS);


            if (placeableItem != null)
            {
                placeableItem.EnableKinematicAsync();
            }

            ActivatePickupTrigger();

            propGrabbed = false;
        }
    }

    public void ActivatePickupTrigger()
    {
        //disable the trigger collider while it's being held
        trigger.gameObject.SetActive(true);

        componentManager.EnableComponents();

        if (PlacementSystem != null)
        {
            //disable anything attached to the placement system
            PlacementSystem.PropagateDropToChildren();
        }
    }




    private void AttachToCharacter(bool attach, InteractionSystem connectionTarget) {
        if (attach) {
            if (propMuscle.currentProp != null)
            {
                PickUpItem prevItem = propMuscle.currentProp.GetComponent<PickUpItem>();
                if (prevItem != null)
                {
                    prevItem.PuppetMasterDrop();
                }

            }
            propMuscle.currentProp = prop;
        }
        else {
            propMuscle.currentProp = null;
        }
    }


    private void addToInventory() {

        ik = itemToPickUp.lastUsedInteractionSystem.GetComponent<FullBodyBipedIK>();
        PlayerInventory inventory = ik.GetComponentInParent<PlayerInventory>();

        inventory.AddItem(this);


    }

    public void RemoveFromInventory() {
        PlayerInventory inventory = ik.GetComponentInParent<PlayerInventory>();
        inventory.RemoveItem(this);
    }


}
