using ScriptableObjLib.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeCup : MonoBehaviour
{

    [Tooltip("Scriptable var reference to the player's Caffeine level.")]
    public ScriptableFloat caffeineLevel;

    [Tooltip("Reference to the volumetric liquid.")]
    public VolumetricFlow VolLiquid;

    [Tooltip("number of sips.")]
    public int NumSips = 1;


    public float sipVolume;

    // Start is called before the first frame update
    void Start()
    {
        if (VolLiquid == null)
        {
            VolLiquid = GetComponent<VolumetricFlow>();
        }

        CalculateSipVolume();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CalculateSipVolume()
    {
        if (VolLiquid != null)
        {
            sipVolume = VolLiquid.MaxVolume / NumSips;
        }
    }

    public void SipCoffee()
    {
        if (VolLiquid != null && VolLiquid.currentLiquid.CurrentVolume > 0)
        {
            //assume we will sip the sip amount
            float volToSip = sipVolume;

            //if we don't have enough, then sip what's left
            if (volToSip > VolLiquid.currentLiquid.CurrentVolume)
            {
                volToSip = VolLiquid.currentLiquid.CurrentVolume;
            }

            //figure out what percentage of the volume we are sipping
            float volPercentage = volToSip / VolLiquid.currentLiquid.CurrentVolume;
            //Debug.Log("current percentage sipped is " + volPercentage);
            float caffAmtSipped = volPercentage * VolLiquid.currentLiquid.CaffeineLevel;

            if (caffAmtSipped > VolLiquid.currentLiquid.CaffeineLevel)
            {
                caffAmtSipped = VolLiquid.currentLiquid.CaffeineLevel;
            }

            //add caffeiene to player
            caffeineLevel.ApplyChange(caffAmtSipped);

            //remove liquid volume and caffeiene from coffee cup
            VolLiquid.currentLiquid.CaffeineLevel -= caffAmtSipped;
            VolLiquid.currentLiquid.CurrentVolume -= volToSip;


        }
    }
}
