using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    [Header("Debug")]
    [SerializeField]
    private List<PickUpItem> itemsInHand = new List<PickUpItem>();

    public List<PickUpItem> ItemsInHand => itemsInHand;


    public bool AddItem(PickUpItem itemToAdd)
    {
        bool addSuccessful = false;

        //check to prevent adding the same item to the inventory multiple times
        bool itemCurrentlyHeld = itemsInHand.Contains(itemToAdd);

        if (!itemCurrentlyHeld)
        {
            itemsInHand.Add(itemToAdd);
            addSuccessful = true;
        }
        else
        {
            Debug.LogWarning("item is already held, not adding it to inventory again", itemToAdd.gameObject);
        }


        return addSuccessful;
    }

    public bool RemoveItem(PickUpItem itemToAdd)
    {
        bool removalSuccssful = false;

        //check to prevent adding the same item to the inventory multiple times
        bool itemCurrentlyHeld = itemsInHand.Contains(itemToAdd);

        if (itemCurrentlyHeld)
        {
            itemsInHand.Remove(itemToAdd);
            removalSuccssful = true;
        }
        else
        {
            Debug.LogWarning("attempting to remove item that is not currently in inventory", itemToAdd.gameObject);
        }


        return removalSuccssful;
    }


}
