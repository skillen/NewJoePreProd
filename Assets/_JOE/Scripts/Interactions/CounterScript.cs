using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CounterScript : MonoBehaviour
{
    public int ActivationsRequired = 1;

    public int activations = 0;

    

    public UnityEvent OnCountReached;

    public void Start()
    {
        activations = 0;
    }


    public void IncreaseCount()
    {
        if (activations < ActivationsRequired)
        {
            activations++;
            if (activations >= ActivationsRequired)
            {
                OnCountReached.Invoke();
            }
        }
        
    }

    public void ResetCount()
    {
        activations = 0;
    }


}
