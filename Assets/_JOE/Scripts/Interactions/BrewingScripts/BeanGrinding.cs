﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RootMotion.FinalIK;
using RootMotion.Dynamics;

public class BeanGrinding : MonoBehaviour
{
    [TagSelector]
    public string[] triggerTags = new string[] { };

    [TagSelector]
    public string[] coffeeGroundsTag = new string[] { };

    public List<PropTypes> ApplicablePropTypes;

    public List<GameObject> UngroundCoffeeObjs;

    public List<GameObject> GroundsList;

    public GameObject groundCoffee;

    [SerializeField]
    private InteractionObject interactionObject;
    private InteractionSystem lastIS;
    public InteractionController interactionController;




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public GameObject GetBeanTarget()
    {
        if (UngroundCoffeeObjs.Count > 0)
        {
            return UngroundCoffeeObjs.First();
        }
        else return null;

    }

    //maybe check if there's already a bag of coffee and increase the amount of grounds if it's not full
    public void GrindBean()
    {
        lastIS = interactionObject.lastUsedInteractionSystem;
        interactionController = lastIS.GetComponentInParent<InteractionController>();

        PuppetMasterProp RHProp = interactionController.rightHand.currentProp;
        PuppetMasterProp LHProp = interactionController.leftHand.currentProp;

        GameObject bean = GetBeanTarget();
        if (bean != null)
        {
            //if (ApplicablePropTypes.Count <= 0 || PropCheck(RHProp) || PropCheck(LHProp))
            //{
                if (GroundsList.Count > 0)
                {
                    foreach (GameObject groundsObject in GroundsList)
                    {
                        CoffeeGrounds grounds = groundsObject.GetComponentInParent<CoffeeGrounds>();

                        CoffeeGrounds beanGrounds = bean.GetComponent<CoffeeGrounds>();

                        if (grounds != null && grounds.CurrentGrounds < grounds.maxGrounds)
                        {
                            

                            //move coffee data from bean to the coffee bag (ground coffee)
                            grounds.TransferGrounds(bean);
                            //grounds.coffeeData.currentGrounds += 1;
                            UngroundCoffeeObjs.Remove(bean);
                            Destroy(bean);
                            return;
                        }
                    }


                    Instantiate(groundCoffee, transform.position, transform.rotation);


                }
                //create a new prop
                else
                {
                   
                    GameObject newCoffeeBag = Instantiate(groundCoffee, transform.position, transform.rotation);
                    CoffeeGrounds bagGrounds = newCoffeeBag.GetComponent<CoffeeGrounds>();

                    CoffeeGrounds beanGrounds = bean.GetComponent<CoffeeGrounds>();


                    bagGrounds.TransferGrounds(bean);


                    UngroundCoffeeObjs.Remove(bean);
                    Destroy(bean);


                }
            //}


        }


    }

    bool PropCheck(PuppetMasterProp PropToCheck)
    {
        bool propTypeMatches = false;
        if (ApplicablePropTypes.Count > 0 && PropToCheck != null)
        {
            PickUpItem itemScript = interactionController.rightHand.currentProp.GetComponent<PickUpItem>();

            if (itemScript != null)
            {
                //check if the prop types of the prop matches the applicable prop type
                propTypeMatches = ApplicablePropTypes.Intersect(itemScript.proptype).Any();
            }

        }

        return propTypeMatches;
    }

    private void OnTriggerStay(Collider other)
    {
        if (triggerTags.Contains(other.tag))
        {
            if (!UngroundCoffeeObjs.Contains(other.gameObject))
            {
                UngroundCoffeeObjs.Add(other.gameObject);
            }


        }
        else if (coffeeGroundsTag.Contains(other.tag))
        {
            if (!GroundsList.Contains(other.gameObject))
            {
                GroundsList.Add(other.gameObject);
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (triggerTags.Contains(other.tag))
        {
            if (UngroundCoffeeObjs.Contains(other.gameObject))
            {
                UngroundCoffeeObjs.Remove(other.gameObject);
            }

        }
        else if (coffeeGroundsTag.Contains(other.tag))
        {
            if (GroundsList.Contains(other.gameObject))
            {
                GroundsList.Remove(other.gameObject);
            }

        }
    }


}
