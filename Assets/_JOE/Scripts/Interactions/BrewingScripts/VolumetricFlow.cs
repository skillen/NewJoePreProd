﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public struct LiquidData
{
    public float CurrentVolume;

    public float CaffeineLevel;

    public float LiquidTemp;

    public Color color;

    public GameObject PourEffect;


}

public class VolumetricFlow : MonoBehaviour
{
    [Header("General Parameters")]

    public float MaxVolume = 100f;

    float currentLiquicdCapacity => MaxVolume - currentLiquid.CurrentVolume;

    public bool ConnectionsChanged = false;

    [SerializeField]
    private bool prevFlowState = false;

    [Header("references")]

    public PipeConnections Pipes;

    [Header("Debug Stuff")]

    public bool TurnWaterOn = false;

    [Header("flow restriction parameters")]

    public float flowRestriction = 10;

    public float FlowRate;

    public LiquidData currentLiquid;

    public float FillPercent => currentLiquid.CurrentVolume / MaxVolume;

    public Color liquidColor => currentLiquid.color;

    public UnityEvent FlowEnterEvent = new UnityEvent();

    //public UnityEvent volTriggerEvent;


    // Start is called before the first frame update
    void Start()
    {

        Pipes = GetComponent<PipeConnections>();


        //if (volTriggerEvent == null)
        //    volTriggerEvent = new UnityEvent();

        //volTriggerEvent.AddListener(OnVolumeChange);

        if (TurnWaterOn)
        {
            currentLiquid.CurrentVolume = MaxVolume;
        }


        Pipes.redirectFlow();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (TurnWaterOn)
        {
            currentLiquid.CurrentVolume = MaxVolume;
        }

        propagateLiquidVol(Pipes);

    }

    void OnVolumeChange()
    {

        if (Pipes != null /*&& pipes.CheckFlowState()*/)
        {
            Pipes.redirectFlow();
        }
    }



    public bool IsFlowPresent()
    {


        bool FlowIsPresent = false;

        //if there was volume passsing through but vol is temporarily 0, but there is still an input
        if (prevFlowState)
        {
            //if there's some volume in the pipe
            //OR there's at least one input (to account for temp 0 as propagation happens)
            if (currentLiquid.CurrentVolume > 0 || Pipes.FlowInputs.Count > 0 )
            {
                //and the current volume is less than max or there is at least one flow output
                //ie the liquid hasn't filled up all available volume
                if (currentLiquid.CurrentVolume < MaxVolume || Pipes.FlowOutputs.Count > 0)
                {
                    FlowIsPresent = true;
                }
               
            }

            
        }
        //there was not previously a flow state,
        //only start a flow state if there's actually liquid in the pipe
        else
        {
            if (currentLiquid.CurrentVolume > 0)
            {
                FlowIsPresent = true;
            }

        }


        //if there is no output, it is likely that we just added flow to the pipe, but haven't calculated the outputs... realistically we need to calculate all outputs before deciding
        //if (there's at least one input and either the pipe is not at full capacity, or there's at least one output) OR the pipe has more than 0 volume (is not empty)
        //there is a flow present


        //use thiss to detect changes in flowstate




        return FlowIsPresent;

    }

    public void propagateLiquidVol(PipeConnections pipeConnections)
    {
        List<PipeOpening> flowOutputs = pipeConnections.FlowOutputs;

        bool isFlowSource = IsFlowPresent();

        if (isFlowSource)
        {



            float volPerOutput = currentLiquid.CurrentVolume / flowOutputs.Count;

            foreach (var opening in flowOutputs)
            {
                if (opening.PipeIsConnected && opening.PipeIsOpen)
                {



                    //prevents outputting too much volume
                    //outputting more than the flow rate should allow
                    if (volPerOutput > currentLiquid.CurrentVolume)
                    {
                        volPerOutput = currentLiquid.CurrentVolume;
                    }
                    VolumetricFlow otherPipes = opening.openingData.Connection.GetComponentInParent<VolumetricFlow>();

                    //calculate the flow rate for the pipe
                    //not used in 
                    otherPipes.FlowRateCalculation(volPerOutput);

                    currentLiquid.CurrentVolume -= otherPipes.AddVolume(volPerOutput, currentLiquid);


                }
                //opening is not connected to any other pipes
                else
                {




                    //apply the flow rate of the opening to the output rate
                    FlowRate = Mathf.Abs(flowRestriction * Time.deltaTime);

                    FlowRate /= flowOutputs.Count;

                    //need to make sure that flow rate doesn't exceed volume
                    if (FlowRate > currentLiquid.CurrentVolume)
                    {
                        FlowRate = currentLiquid.CurrentVolume;
                    }

                    //the pipe is open but not connected, then spill out contents
                    currentLiquid.CurrentVolume -= FlowRate;



                }


            }

            //need to check if the current volume is <= 0 and there are no flow inputs
            //this represents a change after passing liquid out
            //if (pipeConnections.FlowInputs.Count <= 0 && currentLiquid.CurrentVolume <= 0)
            //{
            //    OnVolumeChange();
            //    //volTriggerEvent.Invoke();


            //}
        }

        if (prevFlowState != isFlowSource || ConnectionsChanged)
        {
            prevFlowState = isFlowSource;

            //reset the connections changed state
            ConnectionsChanged = false;

            OnVolumeChange();

            foreach (PipeOpening opening in Pipes.pipeOpenings)
            {
                opening.PropagatePourEffect(this);
            }
        }

    }

    private float AddVolume(float volumeIn, LiquidData liquidDataIn)
    {


        float volTaken = 0;



        //add as much volume as possible, return the amont taken from source
        //float currentLiquicdCapacity = MaxVolume - currentLiquid.CurrentVolume;

        //apply the flow rate of the opening to the output rate
        //use volume in as the maximum amount that the pipe segment will add
        //FlowRateCalculation(volumeIn);

        if (FlowRate > currentLiquicdCapacity)
        {
            volTaken = currentLiquicdCapacity;
        }
        //otherwiste take all fo the new volume
        else volTaken = FlowRate;



        if (currentLiquid.CurrentVolume > 0)
        {
            CalcAvgTemp(liquidDataIn);
        }
        else
        {
            currentLiquid.LiquidTemp = liquidDataIn.LiquidTemp;
        }

        if (liquidDataIn.CaffeineLevel > 0)
        {
            float caffeineToAdd = liquidDataIn.CaffeineLevel;

            currentLiquid.CaffeineLevel += caffeineToAdd;
            liquidDataIn.CaffeineLevel -= caffeineToAdd;
        }

        //if the new liquid has a different pour effect, set this liquid data to that, othewise keep it the same
        //currentLiquid.PourEffect = currentLiquid.PourEffect != liquidDataIn.PourEffect ? liquidDataIn.PourEffect : currentLiquid.PourEffect;
        if (currentLiquid.PourEffect != liquidDataIn.PourEffect)
        {
            currentLiquid.PourEffect = liquidDataIn.PourEffect;

        }
        //maybe propagate pour here?



        currentLiquid.CurrentVolume += volTaken;


        currentLiquid.color = liquidDataIn.color;

        FlowEnterEvent.Invoke();

        return volTaken;
    }

    private void FlowRateCalculation(float volumeIn)
    {
        FlowRate = Mathf.Clamp(Mathf.Abs(flowRestriction * Time.deltaTime), 0, volumeIn);
    }

    //used to pass volume directly to a volume of liquid without the flow
    //eg dispensing coffee to a coffee cup
    public void PassVolume(float volumeOut, VolumetricFlow addToVol)
    {
        float actualVolOut = volumeOut;

        if (volumeOut > currentLiquid.CurrentVolume)
        {
            actualVolOut = currentLiquid.CurrentVolume;
        }

        addToVol.FlowRate = actualVolOut;
        //VolumetricFlow otherPipes = opening.openingData.Connection.GetComponentInParent<VolumetricFlow>();
        currentLiquid.CurrentVolume -= addToVol.AddVolume(actualVolOut, currentLiquid);
    }

    private void CalcAvgTemp(LiquidData liquidDataIn)
    {
        currentLiquid.LiquidTemp = (currentLiquid.LiquidTemp * currentLiquid.CurrentVolume + liquidDataIn.LiquidTemp * liquidDataIn.CurrentVolume)
            / (currentLiquid.CurrentVolume + liquidDataIn.CurrentVolume);
    }

}
