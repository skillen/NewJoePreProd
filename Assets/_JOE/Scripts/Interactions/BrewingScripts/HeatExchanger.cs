﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[RequireComponent(typeof(VolumetricFlow))]
public class HeatExchanger : MonoBehaviour
{

    [Header("General")]


    public float BoilThreshold = 5f;

    //use this to control opening/closing flow to these openings
    [SerializeField]
    private List<PipeOpening> flowOutputs;
    public List<PipeOpening> FlowOutPuts { get { return flowOutputs; } }

    public PipeOpeningState openState;
    public PipeOpeningState closedState;

    [Header("References")]

    public VolumetricFlow CurrentVolFlow;

    [SerializeField]
    bool Boiling = false;
    public ParticleSystem BoilEffect;

    public PipeConnections flaskPipe;


    public bool flowIsActive = false;

    [SerializeField]
    bool heatFlask = false;

    [Header("Heat Exchange parameters")]

    public float MaterialHeatCoeffeicient = 1;
    [SerializeField]
    float internalHeat;

    public float externalHeat = 0;

    public float defaultExternalHeat = 0;

    [SerializeField]
    private float tempGradient;

    //this just lets us limit the maximum change rate by clamping the difference in temp
    //which determines the rate of change
    public float MaxHeatDifference = 100;

    public float maxHeatTransferRate = 15;

    [SerializeField]
    private float heatExchangeRate;






    GameObject boilEffect;


    // Start is called before the first frame update
    void Start()
    {

        internalHeat = 0;



        if (CurrentVolFlow == null)
        {
            CurrentVolFlow = GetComponentInParent<VolumetricFlow>();
            internalHeat = CurrentVolFlow.currentLiquid.LiquidTemp;
        }

    }

    // Update is called once per frame
    void Update()
    {



    }

    private void FixedUpdate()
    {
        //if (flowIsActive)
        //{

        //    CurrentVolFlow.propagateLiquidVol(flowOutputs);
        //}



        CalculateTempChange();

        CurrentVolFlow.currentLiquid.LiquidTemp = internalHeat;

        if (!Boiling && internalHeat >= BoilThreshold)
        {
            Boil();
            Boiling = true;
        }


        if (Boiling && internalHeat <= BoilThreshold)
        {
            Cool();
            Boiling = false;
        }



        //if (!heatFlask && internalHeat >= 0f)
        //{
        //    //internalHeat -= Time.deltaTime;

        

        //}

        //reset the external heat value at end of update loop
        externalHeat = defaultExternalHeat;

        //disable this bool at end of each update cycle
        heatFlask = false;
    }

    public void StartHeating(float ExternalHeat)
    {
        //this is just for debug purposes now
        heatFlask = true;

        externalHeat += ExternalHeat;



    }


    void Boil()
    {
        //Debug.Log("water boiling");

        BoilEffect.Play();

        if (flaskPipe != null)
        {
            flowIsActive = true;
            foreach (PipeOpening output in flowOutputs)
            {
                output.UpdateOpeningState(openState);
            }

        }
    }



    void Cool()
    {
        //Debug.Log("water cooled");

        BoilEffect.Stop();

        if (flaskPipe != null)
        {
            flowIsActive = false;
            foreach (PipeOpening output in flowOutputs)
            {
                output.UpdateOpeningState(closedState);
            }
        }
    }


    private void CalculateTempChange()
    {
        internalHeat = CurrentVolFlow.currentLiquid.LiquidTemp;

        if (internalHeat != externalHeat)
        {
            float tempDiff = externalHeat - internalHeat;
            var sign = Mathf.Sign(tempDiff);


            tempGradient = Mathf.Clamp(Mathf.Abs(tempDiff), 0, MaxHeatDifference);


            heatExchangeRate = MaterialHeatCoeffeicient * sign * Mathf.Abs(tempGradient).Remap(0, Mathf.Abs(MaxHeatDifference), 0, Mathf.Abs(maxHeatTransferRate));

            float heatchExchangeSign = Mathf.Sign(heatExchangeRate);

            //prevent extrely small temp changes
            if (Mathf.Abs(heatExchangeRate) > 0 && Mathf.Abs(heatExchangeRate) < 0.5f)
            {
                heatExchangeRate = 0.5f * heatchExchangeSign;
            }
            

            internalHeat += heatExchangeRate * Time.deltaTime;

            //just make it the same if there's a small enough difference
            if (Mathf.Abs(tempGradient) <= 0.5f)
            {
                internalHeat = externalHeat;
            }

        }


    }


}
