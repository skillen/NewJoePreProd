﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePipeFlow : MonoBehaviour
{

    public PipeOpening flowSource;
    public GameObject flowEffect;
    public PipeConnections pipe;

    public void onToggle()
    {
        pipe.AddFlow(flowSource);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
