﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

[RequireComponent(typeof(CoffeeGrounds))]
public class CoffeeExtractor : MonoBehaviour
{


    [Header("extraction rate variables")]

    public float BrewHeatThreshold;
    //public float internalTemp;

    public float deltaIdealTemp;

    public float extractionRate;

    public float extractedCaffeiene;

    [Header("caffeine burn rate rate variables")]

    public float BurnCaffTemp = 32;

    public float deltaBurnTemp;

    public float BurnCaffRate;

    public float BurnedCaffeiene;

    [Header("general references")]

    public PipeConnections pipeConnections;
    public VolumetricFlow liquidVol;

    [SerializeField]
    public PipeConnections FlowSource { get; private set; }

    public GameObject EndPoint;

    public CoffeeGrounds coffeeGrounds;


    public UnityEvent brewingEvent;
    // Start is called before the first frame update
    void Start()
    {
        coffeeGrounds = GetComponent<CoffeeGrounds>();
        pipeConnections = GetComponent<PipeConnections>();
        liquidVol = GetComponent<VolumetricFlow>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeColor()
    {
        if (coffeeGrounds == null || liquidVol == null)
        {
            return;
        }
        if (coffeeGrounds.coffeeData.Count > 0)
        {
            liquidVol.currentLiquid.color = coffeeGrounds.coffeeData.First().CoffeeColor;
        }

    }


    public void BrewCoffee()
    {
        //FindFlowSource();
        bool flowPresent = pipeConnections.FlowInputs.Count > 0;


        if (flowPresent)
        {

            ChangeColor();

            //need to hook up the variables with the liquid temp
            //and the coffee extraction rates
            if (coffeeGrounds != null && coffeeGrounds.coffeeData.Count > 0)
            {
                //need to set this up for each data in coffee grounds
                foreach (CoffeeData coffeeData in coffeeGrounds.coffeeData)
                {
                    BurnCaffeineCalculation(coffeeData);
                    CalculateCaffeineExtraction(coffeeData);
                }

                brewingEvent.Invoke();

            }


            

            //if (FlowSource)
            //{
               
            //}
        }
    }

    private void CalculateCaffeineExtraction(CoffeeData coffeeData)
    {
        //caffeine extraction rate calculations
        float idealExtractionTemp = coffeeData.IdealExtractionTemp;
        float minExtractionTemp = coffeeData.MinExtractionTemp;
        float idealExtractionRate = coffeeData.IdealExtractionRate;
        float unextractedCaffeiene = coffeeData.Caffeine;
        

        float liquidTemp = liquidVol.currentLiquid.LiquidTemp;

        //get internal temp from the liquid in volumetric flow
        //deltaIdealTemp = Mathf.Abs(liquidTemp - idealExtractionTemp);

        extractionRate = liquidTemp.Remap(minExtractionTemp, idealExtractionTemp, 0, idealExtractionRate);

        extractionRate = Mathf.Clamp(extractionRate, 0, idealExtractionRate);

        float extractuionAmt = extractionRate * Time.deltaTime;


        if (coffeeData.Caffeine >= extractuionAmt)
        {
            //for tracking debugging only
            extractedCaffeiene += extractuionAmt;
            //actually add the amount to the liquid data
            liquidVol.currentLiquid.CaffeineLevel = extractuionAmt;
            coffeeData.Caffeine -= extractuionAmt;
        }
        else if (coffeeData.Caffeine <= extractuionAmt && coffeeData.Caffeine > 0)
        {
            //for tracking debugging only
            extractedCaffeiene += coffeeData.Caffeine;
            //actually add the amount to the liquid data
            liquidVol.currentLiquid.CaffeineLevel = coffeeData.Caffeine;
            coffeeData.Caffeine = 0;
        }
        else if (coffeeData.Caffeine <= 0)
        {
            //don't change the current liquid data
            liquidVol.currentLiquid.CaffeineLevel = 0;
        }

    }

    private void BurnCaffeineCalculation(CoffeeData coffeeData)
    {
        //burn rate calculation
        float internalTemp = liquidVol.currentLiquid.LiquidTemp; ;

        deltaBurnTemp = internalTemp - BurnCaffTemp;

        if (deltaBurnTemp > 0)
        {
            //temp exceeded burning temperature
            BurnCaffRate = deltaBurnTemp * Time.deltaTime;
            
            if (coffeeData.Caffeine >= BurnCaffRate)
            {
                BurnedCaffeiene += BurnCaffRate;
                coffeeData.Caffeine -= BurnCaffRate;
            }
            else if (coffeeData.Caffeine <= BurnCaffRate && coffeeData.Caffeine > 0)
            {
                //for tracking debugging only
                BurnedCaffeiene += coffeeData.Caffeine;
                coffeeData.Caffeine = 0;
            }
        }
    }


}
