﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RootMotion.Dynamics;

[System.Serializable]
public class CoffeeData
{
    public int CurrentGrounds;

    public Color CoffeeColor;

    
    //public float MaxExtractionThreshold;

    [Header("extraction rate variables")]

    public float Caffeine;

    //public float UnextractedCaffeiene = 50;

    public float IdealExtractionTemp = 27;

    public float MinExtractionTemp = 0;

    public float IdealExtractionRate = 3;

    //public float ExtractedCaffeiene;

    [Header("caffeine burn rate rate variables")]

    public float BurnCaffTemp = 32;


    public float BurnedCaffeiene;

    public CoffeeTypeBase CoffeeType;

    public CoffeeData(int currentGrounds, Color coffeeColor, float caffeine,
        float idealExtractionTemp, float minExtractionTemp, /*float maxExtrationThreshold,*/ CoffeeTypeBase coffeeType)
    {
        CurrentGrounds = currentGrounds;
        CoffeeColor = coffeeColor;
        Caffeine = caffeine;
        IdealExtractionTemp = idealExtractionTemp;
        MinExtractionTemp = minExtractionTemp;
        //MaxExtractionThreshold = maxExtrationThreshold;
        CoffeeType = coffeeType;
    }

    public CoffeeData(int currentGrounds, float caffeine, CoffeeData prevData)
    {
        CurrentGrounds = currentGrounds;
        CoffeeColor = prevData.CoffeeColor;
        Caffeine = caffeine;
        IdealExtractionTemp = prevData.IdealExtractionTemp;
        MinExtractionTemp = prevData.MinExtractionTemp;
        //MaxExtractionThreshold = prevData.MaxExtractionThreshold;
        CoffeeType = prevData.CoffeeType;
    }


}

public class CoffeeGrounds : MonoBehaviour
{

    public PickUpItem pickUpScript;

    public int CurrentGrounds => coffeeData.Select(coffee => coffee.CurrentGrounds).Sum();


    public List<CoffeeData> coffeeData;

    //public Color coffeeColor;


    //  public Color liquidColor => currentLiquid.color;


    public int maxGrounds = 10;
    // Start is called before the first frame update
    void Start()
    {
        pickUpScript = GetComponent<PickUpItem>();
    }

    // Update is called once per frame
    void Update()
    {
    }



    public CoffeeData AddGrounds(CoffeeData coffeeToAdd)
    {
        CoffeeData returnCoffee = new CoffeeData(0, 0f, coffeeToAdd);

        if ((CurrentGrounds + coffeeToAdd.CurrentGrounds) > maxGrounds)
        {


            int returnAmount = CurrentGrounds + coffeeToAdd.CurrentGrounds - maxGrounds;
            int amountToAdd = maxGrounds - CurrentGrounds;

            float caffeineToAdd = amountToAdd / coffeeToAdd.CurrentGrounds * coffeeToAdd.Caffeine;
            float caffeineToReturn = coffeeToAdd.Caffeine = caffeineToAdd;

            //populate the retun amount values
            returnCoffee.CurrentGrounds = returnAmount;
            returnCoffee.Caffeine = caffeineToReturn;

            //if we have a prexisting amount of grounds of that type
            if (coffeeData.Any(x => x.CoffeeType == coffeeToAdd.CoffeeType))
            {
                CoffeeData prexistingCoffee = coffeeData.First(x => x.CoffeeType == coffeeToAdd.CoffeeType);

                prexistingCoffee.CurrentGrounds += amountToAdd;
                prexistingCoffee.Caffeine += caffeineToAdd;

            }
            else
            {
                //add a new entry to the coffee grounds list
                CoffeeData addedCoffee = new CoffeeData(amountToAdd, coffeeToAdd.CoffeeColor, caffeineToAdd, coffeeToAdd.IdealExtractionTemp, coffeeToAdd.MinExtractionTemp, /*coffeeToAdd.MaxExtractionThreshold,*/ coffeeToAdd.CoffeeType);
                coffeeData.Add(addedCoffee);
            }





        }
        //don't need to deal with excess coffee
        else
        {

            CoffeeData addedCoffee = new CoffeeData(coffeeToAdd.CurrentGrounds, coffeeToAdd.CoffeeColor, coffeeToAdd.Caffeine, coffeeToAdd.IdealExtractionTemp, coffeeToAdd.MinExtractionTemp,/* coffeeToAdd.MaxExtractionThreshold,*/ coffeeToAdd.CoffeeType);
            coffeeData.Add(addedCoffee);

        }

        return returnCoffee;

    }

    public void TransferGrounds(GameObject groundsObject)
    {
        CoffeeGrounds otherGrounds = groundsObject.GetComponent<CoffeeGrounds>();



        otherGrounds.coffeeData = otherGrounds.coffeeData.Select(otherCoffee => otherCoffee = AddGrounds(otherCoffee)).ToList();



        //if the other object no longer has any grounds, destroy the prop

        if (otherGrounds.CurrentGrounds <= 0)
        {
            otherGrounds.DestroyProp();
        }

    }

    //to destroy need to make sure that the prop is detached, and then destroy

    public void DestroyProp()
    {
        if (pickUpScript != null)
        {
            //only want to destroy the gameobject if it is a prop, not if it's something else;
            pickUpScript.PuppetMasterDrop();
            Destroy(pickUpScript);
            //Destroy(gameObject);
        }

    }
}
