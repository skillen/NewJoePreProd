using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class ExtensionMethods
{

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return from2 + (value - from1) * (to2 - from2) / (to1 - from1);
        //L + (v - l) * (H - L) / (h - l)
        //return newVal;
        //return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}



public class HeatExchangeTest : MonoBehaviour
{

    [Header("Heat Exchange parameters")]

    public float MaterialHeatCoeffeicient = 1;
    public float internalHeat = 0;
    public float externalHeat = 0;

    [SerializeField]
    private float tempGradient;

    public float MaxHeatDifference = 100;

    public float maxHeatTransferRate = 15;

    [SerializeField]
    private float heatExchangeRate;

    [Header("flow restriction parameters")]

    public float resevoirIn = 1500;
    public float flowRestriction;
    public float FlowOut;

    public float flowAmount;


    [Header("extraction rate calculations")]

    public float unextractedCaffeiene = 50;

    public float internalTemp;

    public float MinExtractionTemp = 0;

    public float idealExtractionTemp = 27;

    public float deltaIdealTemp;


    public float idealExtractionRate = 3;

    [Header("this is what you're looking for")]
    public float extractionRate;


    public float extractedCaffeiene;


    public float BurnCaffTemp = 32;

    public float deltaBurnTemp;

    public float BurnCaffRate;

    public float BurnedCaffeiene;





    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        CaldulateTempChangeRate();

        CalculateFlowVol();

        CalculateCaffeineExtraction();

        BurnCaffeineCalculation();
    }

    private void CaldulateTempChangeRate()
    {
        float tempDiff = externalHeat - internalHeat;
        var sign = Mathf.Sign(tempDiff);


        tempGradient = Mathf.Clamp(Mathf.Abs(tempDiff), 0, MaxHeatDifference);


        heatExchangeRate = MaterialHeatCoeffeicient * sign * Mathf.Abs(tempGradient).Remap(0, Mathf.Abs(MaxHeatDifference), 0, Mathf.Abs(maxHeatTransferRate));


        internalHeat += heatExchangeRate * Time.deltaTime;

        //just make it the same if there's a small enough difference
        if (tempGradient <= 0.01f)
        {
            internalHeat = externalHeat;
        }

        //add heatexhange rate * time.deltatime to get actual heat value
    }

    private void CalculateFlowVol()
    {
        //flow rate calculations

        flowAmount = flowRestriction * Time.deltaTime;

        resevoirIn -= flowAmount;

        FlowOut += flowAmount;
    }

    private void BurnCaffeineCalculation()
    {
        //burn rate calculation

        deltaBurnTemp = internalTemp - BurnCaffTemp;

        if (deltaBurnTemp > 0)
        {
            //temp exceeded burning temperature
            BurnCaffRate = deltaBurnTemp * Time.deltaTime;
            BurnedCaffeiene += BurnCaffRate;
            unextractedCaffeiene -= BurnCaffRate;
        }
    }

    private void CalculateCaffeineExtraction()
    {
        //caffeine extraction rate calculations

        //deltaIdealTemp = Mathf.Abs(internalTemp - idealExtractionTemp);

        extractionRate = internalTemp.Remap(MinExtractionTemp, idealExtractionTemp, 0, idealExtractionRate);

        extractionRate = Mathf.Clamp(extractionRate, 0, idealExtractionRate);


        extractedCaffeiene += extractionRate;
        unextractedCaffeiene -= extractionRate;
    }
}
