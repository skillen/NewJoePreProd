﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CoffeeExtractor))]
public class CoffeeExtractorEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        CoffeeExtractor myTarget = (CoffeeExtractor)target;

        if (GUILayout.Button("Find Flow Source"))
        {
            //myTarget.FindFlowSource();
        }

        if (GUILayout.Button("check that end point is connected"))
        {
            //myTarget.CheckIfEndConnected();
        }


    }
}
