using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Transmitter : MonoBehaviour
{

    public UnityEvent TriggerEvent;
    

    public void FireTrigger()
    {
        TriggerEvent.Invoke();
    }
}
