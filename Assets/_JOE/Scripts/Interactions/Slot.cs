﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Slot : MonoBehaviour
{

    [SerializeField] private List<Slot> neighbors = new List<Slot>();
    [SerializeField] private List<Slot> neighborsHorizontal = new List<Slot>();
    [SerializeField] private List<Slot> neighborsVertical = new List<Slot>();
    private SlotPlacementSystem slotPlacementSystem;
    public Vector2Int placeOnGrid;

    public PickUpItem itemPlacedOnSlot;


    public bool IsOccupied = false;
    public MeshRenderer Mesh => GetComponent<MeshRenderer>();
    public float BoundsLegth => GetMaxElement(Mesh.bounds.size);
    public Vector3 BoundsCentre => Mesh.bounds.center;
    public Slot NeighborOpenSlot(List<Slot> alreadyCheckedSlots) => neighbors.Find(x => !alreadyCheckedSlots.Contains(x) && x.IsOccupied == false);

    private float GetMaxElement(Vector3 v3) => Mathf.Max(Mathf.Max(v3.x, v3.y), v3.z);
    public void SetColor(Color color) => Mesh.material.color = color;




    private void Awake()
    {
        slotPlacementSystem = GetComponentInParent<SlotPlacementSystem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        CheckForNeighbor(other);
    }


    private void CheckForNeighbor(Collider other)
    {
        Slot neighbor = other.GetComponent<Slot>();

        if (neighbor != null)
        {
            neighbors.Add(neighbor);
        }

        // Debug.Log($"{this.gameObject.name} has neighbors: {string.Join(", ", neighbors.Select(x => x.gameObject.name).ToList())}" + "\n\n");
    }



    public void PlaceOnGrid(int row, int column)
    {
        placeOnGrid = new Vector2Int(row, column);
    }

    public void FindNeighbors()
    {
        int row = this.placeOnGrid.x;
        int column = this.placeOnGrid.y;

        neighborsHorizontal.AddRange(slotPlacementSystem.slotsList.FindAll(s => s.placeOnGrid.x == row && (s.placeOnGrid.y == column - 1 || s.placeOnGrid.y == column + 1)));
        neighborsVertical.AddRange(slotPlacementSystem.slotsList.FindAll(s => s.placeOnGrid.y == column && (s.placeOnGrid.x == row - 1 || s.placeOnGrid.x == row + 1)));
    }

    public List<Slot> FreeHorizontalNeighbors()
    {
        return neighborsHorizontal.FindAll(x => x.IsOccupied == false);
    }

    public List<Slot> FreeSlotsHorizontal()
    {
        List<Slot> freeHorizontalSlots = new List<Slot>();

        freeHorizontalSlots.AddRange(FreeHorizontalNeighbors().Where(x => x != this &&  !freeHorizontalSlots.Contains(x)));

        for (int i = 0; i < freeHorizontalSlots.Count && i < slotPlacementSystem.slotsList.Count; i++)
        {
            freeHorizontalSlots.AddRange(freeHorizontalSlots[i].FreeHorizontalNeighbors().Where(x => x != this &&  !freeHorizontalSlots.Contains(x)));
        }

        return freeHorizontalSlots;
    }

    public List<Slot> FreeVerticalNeighbors()
    {
        return neighborsVertical.FindAll(x => x.IsOccupied == false);
    }

    public List<Slot> FreeSlotsVertical()
    {
        List<Slot> freeVerticalSlots = new List<Slot>();

        freeVerticalSlots.AddRange(FreeVerticalNeighbors().Where(x => x != this && !freeVerticalSlots.Contains(x)));

        for (int i = 0; i < freeVerticalSlots.Count && i < slotPlacementSystem.slotsList.Count; i++)
        {
            freeVerticalSlots.AddRange(freeVerticalSlots[i].FreeVerticalNeighbors().Where(x => x != this && !freeVerticalSlots.Contains(x)));
        }

        return freeVerticalSlots;
    }









}