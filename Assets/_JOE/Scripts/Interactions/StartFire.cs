﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StartFire : MonoBehaviour
{

    public List<ParticleSystem> fireParticles;
    public bool toggle = false;

    public float Heat = 1;

    public Collider HeatingVolume;

    [TagSelector]
    public string[] triggerTags = new string[] { };

    public void FireInteraction()
    {
        toggle = !toggle;
        if (toggle)
        {
            foreach (ParticleSystem fireEffects in fireParticles)
            {
                fireEffects.Play();
            }
            
            HeatingVolume.enabled = true;
        }
        else
        {
            foreach (ParticleSystem fireEffects in fireParticles)
            {
                fireEffects.Stop();
            }
            
            HeatingVolume.enabled = false;
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    ReliableOnTriggerExit.NotifyTriggerEnter(other, gameObject, OnTriggerExit);

    //    if (triggerTags.Contains(other.tag))
    //    {

    //        Debug.Log(other.name + " trigger detected");
    //        HeatVolume(other.gameObject);
    //    }
    //}

    private void OnTriggerStay(Collider other)
    {
        if (triggerTags.Contains(other.tag))
        {

            //Debug.Log(" Heating " + other.name);
            HeatVolume(other.gameObject);

        }

    }

    void HeatVolume(GameObject HeatTarget)
    {
        HeatTarget.GetComponent<HeatExchanger>().StartHeating(Heat);
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    ReliableOnTriggerExit.NotifyTriggerExit(other, gameObject);

    //    if (triggerTags.Contains(other.tag))
    //    {
    //        Debug.Log(other.name + " left trigger");
    //        CoolTarget(other.gameObject);
    //    }
    //}

}
