﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using RootMotion.Dynamics;
using UnityEngine.Events;

[System.Serializable]
public class PropInteractionEvent : UnityEvent<GameObject>
{
}

public class PropTypeFilter : MonoBehaviour
{
    public List<PropTypes> ApplicablePropTypes;

    public List<PuppetMasterProp> usableProps;

    public string animationName = "Upperbody Actions.SwingProp";

    public PropInteractionEvent propInteractionEvent;

    public InteractionObject interactionObject;
    public InteractionController interactionController;
    private InteractionSystem lastIS;
    //private FullBodyBipedIK rigIK;
    private List<InteractionTarget> targets;
    [SerializeField]
    private Animator charAnim;




    private bool propGrabbed = false;





    private void Awake()
    {
        if (interactionObject == null)
        {
            interactionObject = GetComponent<InteractionObject>();
        }
        
        targets = GetComponentsInChildren<InteractionTarget>().ToList();


    }


    public void OnAttemptInteraction()
    {
        usableProps.Clear();

        Debug.Log(gameObject.name);



        lastIS = interactionObject.lastUsedInteractionSystem;
        interactionController = lastIS.GetComponentInParent<InteractionController>();
        charAnim = lastIS.GetComponent<Animator>();



        PlayerInventory inventory = interactionController.GetComponent<PlayerInventory>();



        //get props that are relevant, get their puppetmasterprop components as a list
        usableProps = inventory.ItemsInHand.Where(item => PropCheck(item)).Select(item => item.prop).ToList();




        //if either there are not relevant prop types (ie anything is fine)
        //or there is a relevant prop type in the players hand
        if (ApplicablePropTypes.Count <= 0 || usableProps.Count > 0)
        {
            //invoke the event with the appropriate prop type
            propInteractionEvent.Invoke(usableProps.First().gameObject);

            //charAnim.SetBool("HammerPose", true);
            if (animationName != null)
            {
                charAnim.Play(animationName);
            }

        }



    }



    bool PropCheck(PickUpItem itemScript)
    {
        bool propTypeMatches = false;
        if (ApplicablePropTypes.Count > 0 && itemScript != null)
        {
            //PickUpItem itemScript = interactionController.rightHand.currentProp.GetComponent<PickUpItem>();
            PuppetMasterProp CurrentProp = itemScript.prop;

            if (CurrentProp != null)
            {
                //check if the prop types of the prop matches the applicable prop type
                propTypeMatches = ApplicablePropTypes.Intersect(itemScript.proptype).Any();

            }

        }

        return propTypeMatches;
    }

    public bool EvaluateUsableProps(PlayerInventory inventory)
    {
        bool RelevantPropPresent = false;

        //get props that are relevant, get their puppetmasterprop components as a list
        usableProps = inventory.ItemsInHand.Where(item => PropCheck(item)).Select(item => item.prop).ToList();

        if (usableProps.Count > 0)
        {
            RelevantPropPresent = true;
        }

        return RelevantPropPresent;
    }



}

