using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Reciever : MonoBehaviour
{
    public UnityEvent OnRecieved;

    public void addTrigger(GameObject triggerObject)
    {
        Transmitter transmitter = triggerObject.GetComponent<Transmitter>();

        Debug.Log(triggerObject + " added to triggers");

        if (transmitter != null)
        {
            transmitter.TriggerEvent.AddListener(ActionRecieved);
        }

    }

    public void removeTrigger(GameObject triggerObject)
    {
        Transmitter transmitter = triggerObject.GetComponent<Transmitter>();

        if (transmitter != null)
        {
            transmitter.TriggerEvent.RemoveListener(ActionRecieved);
        }
    }

    //the serialized event call to raise
    public void ActionRecieved()
    {
        OnRecieved.Invoke();
    }
}
