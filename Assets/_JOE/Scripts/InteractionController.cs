﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using RootMotion.Demos;
using RootMotion.Dynamics;
using Rewired;
using System.Linq;
using System.Threading.Tasks;

public class InteractionController : MonoBehaviour
{

    public InteractionSystem interactionSystem;

    public UserControls playerController;

    GameObject interactionObject;

    PlayerInventory inventory;

    public PropMuscle leftHand, rightHand;

    public CharacterPuppetController charController;



    //keep track of the currently selected Index;
    private int currentTriggerIndex;

    public float menuRepeatRate = 0.3f;
    public float menuTimer = 0f;


    //sip variables
    private bool sipPressed = false;

    //aiming variables
    public Throwable throwable;
    public PickUpItem throwablePickup;
    public BallisticLauncher throwLauncher;
    private bool thrown;


    private bool dropped;

    private void Start()
    {
        inventory = GetComponent<PlayerInventory>();
    }

    private void DebugRepeatInput()
    {
        Debug.Log("Menu timer: " + menuTimer);
        if (interactionSystem.triggersInRange.Count > 1)
        {
            currentTriggerIndex = (currentTriggerIndex + 1) % interactionSystem.triggersInRange.Count;
        }


    }

    private void Update()
    {

        //menu up initial press
        if (playerController.state.menuUpInitialPress)
        {
            DebugRepeatInput();

        }

        //menu up held
        if (playerController.state.menuUp)
        {
            menuTimer += Time.deltaTime;
            if (menuTimer >= menuRepeatRate)
            {
                DebugRepeatInput();
                menuTimer = 0;
            }

        }
        else
        {
            menuTimer = 0;
        }

        //if we dropped the object, and let go of the pick up button
        //this ensures that once drop is pressed we do not fire drop repeatedly,
        //and do not fire a pick up after dropping until the pick up button  
        if (dropped && !playerController.state.pickUp)
        {
            dropped = false;
        }

        currentTriggerIndex = interactionSystem.GetClosestTriggerIndex();



        //no interaction triggers found, exit loop early
        if (currentTriggerIndex == -1)
        {

            //deactivate display of old interaction object if it exists
            if (interactionObject != null)
            {

                InteractionDisplay oldDisplay = interactionObject.GetComponent<InteractionDisplay>();
                if (oldDisplay != null)
                {
                    oldDisplay.DeactivateDisplay();
                }

            }


            if (playerController.state.pickUp)
            {
                callDropInteraction();
            }
            //if not interacting, then attempt to sip
            else if (playerController.state.sip && !sipPressed)
            {
                sipPressed = true;
                CallSip();
            }

            //reset the sippressed flag once button is released
            if (sipPressed && !playerController.state.sip)
            {
                sipPressed = false;
            }


            /* 
             * aiming controls section 
             */

            if (playerController.state.aim)
            {
                CallAim();
            }
            else if (!playerController.state.aim)
            {
                if (throwLauncher)
                {
                    throwLauncher.TargetCursor.SetActive(false);
                    throwLauncher.gameObject.SetActive(false);
                }


                //un set any throable variables
                throwable = null;
                throwablePickup = null;
            }


            //reset the thrown flag once button is released
            if (thrown && !playerController.state.launch)
            {
                thrown = false;
            }

            /* 
             * Throwing/shooting controls 
             */




            interactionObject = null;

            return;
        }

        //get ref to the gameobject with interaction trigger?
        GameObject newinteractionObject = interactionSystem.triggersInRange[currentTriggerIndex].gameObject;

        if (newinteractionObject != interactionObject)
        {
            //deactivate display of old interaction object if it exists
            if (interactionObject != null)
            {

                InteractionDisplay oldDisplay = interactionObject.GetComponent<InteractionDisplay>();
                if (oldDisplay != null)
                {
                    oldDisplay.DeactivateDisplay();
                }

            }


            //activate display of new interaction object
            InteractionDisplay newDisplay = newinteractionObject.GetComponent<InteractionDisplay>();
            if (newDisplay != null)
            {
                PropTypeFilter propFilter = newinteractionObject.GetComponent<PropTypeFilter>();
                //check for relevant item in player's inventory
                if (propFilter != null && propFilter.EvaluateUsableProps(inventory))
                {
                    MeshFilter newMesh = propFilter.usableProps.First().gameObject.GetComponent<PlaceableItem>().DisplayMesh;
                    //set prop mesh to first relevant item in player's inventory
                    newDisplay.UpdateDisplayMesh(newMesh);
                }

                newDisplay.ActivateDisplay();
            }

        }

        interactionObject = newinteractionObject;

        //try to display the interactionObjects InteractionDisplay

        //need a way to deactivate it as well

        PickUpItem pickupItem = interactionObject.GetComponent<PickUpItem>();

        //condition for dropping held items
        bool itemCurrentlyHeld = inventory.ItemsInHand.Contains(pickupItem);

        //if pickup button pressed, the target can be picked up, and item is held, drop item
        if (playerController.state.pickUp /*&& pickupItem != null && itemCurrentlyHeld*/)
        {
            if (pickupItem != null && itemCurrentlyHeld)
            {

                callDropInteraction();

            }


        }

        //get a list of all triggers in range, lets you cycle through them
        //List<InteractionTrigger> interactionTriggers = interactionSystem.triggersInRange;

        if (interactionSystem.IsPaused())
        {
            if (playerController.state.interact)
            {
                interactionSystem.ResumeAll();
                return;
            }
        }

        //non pickup interaction,
        //there is no item pickup script present (not a pickup interaction system)
        //fire interaction
        if (playerController.state.interact && pickupItem == null)
        {

            if (playerController.state.interact)
            {
                //trigger interaction system if not 
                interactionSystem.TriggerInteraction(currentTriggerIndex, false);
            }

        }

        //if player presses pickup, the item is not currently in their hand, and they aren't in the process of dropping an item
        //also check that pickup item is not null (it's a pick up interactable) to differentiate button presses
        //picking up item, fire pick up interaction
        if (playerController.state.pickUp && pickupItem != null && !itemCurrentlyHeld && !dropped)
        {
            //make sure that the player is not holding something before picking something new up
            if (rightHand.currentProp == null)
            {
                interactionSystem.TriggerInteraction(currentTriggerIndex, false);
            }
            else
            {
                callDropInteraction();
            }
            

        }





    }

    private void CallSip()
    {
        //need to get the <PickUpItem> from the inventory that matches the CoffeeCup proptype
        //get first item in inventory where itemInInventory.prop == currentProp
        //PickUpItem itemInHand = inventory.ItemsInHand.Where(item => item.prop == leftHand.currentProp).First();

        PickUpItem firstCoffeeCup = null;

        if (inventory.ItemsInHand.Count > 0)
        {
            //Debug.Log("testing the throw thingee");
            firstCoffeeCup = inventory.ItemsInHand.Where(item => item.prop.GetComponent<CoffeeCup>() != null).First();
        }

        if (firstCoffeeCup)
        {
            CoffeeCup coffeeCup = firstCoffeeCup.prop.GetComponent<CoffeeCup>();
            if (coffeeCup)
            {
                if (firstCoffeeCup.propMuscle == leftHand)
                {
                    //check if the coffee cup is in the left or right hand

                    charController.animState.LeftHandSip = true;
                }
                else
                {
                    charController.animState.LeftHandSip = false;
                }


                //set the animstate to reflect button state
                charController.animState.Sip = true;


                coffeeCup.SipCoffee();
            }
        }


       
    }

    private void CallAim()
    {
        //need to get the <PickUpItem> from the inventory that matches the CoffeeCup proptype
        //get first item in inventory where itemInInventory.prop == currentProp
        //PickUpItem itemInHand = inventory.ItemsInHand.Where(item => item.prop == leftHand.currentProp).First();


        if (!throwablePickup)
        {
            if (inventory.ItemsInHand.Count > 0)
            {
                throwablePickup = inventory.ItemsInHand.Where(item => item.prop.GetComponent<Throwable>() != null).First();
            }
        }

        
        //if there's a reference to the pickup script on the throwable object
        if (throwablePickup)
        {
            if (!throwLauncher.gameObject.activeSelf)
            {
                throwLauncher.gameObject.SetActive(true);
            }
            throwable = throwablePickup.prop.GetComponent<Throwable>();

            //if there's a throwable            
            if (throwable)
            { 
                
                /*
                 * need to activate the aiming cursor etc
                 */

                if (throwLauncher)
                {
                    throwLauncher.TargetCursor.SetActive(true);
                }

                /*
                 * if aiming and the throw button is pressed
                 * drop the object,
                 * then set it to the location of the ballistic launcher
                 */
                if (playerController.state.launch && !thrown)
                {
                    LaunchThrowable();
                }



            }
        }



    }

    private void LaunchThrowable()
    {
        if (throwablePickup && throwable)
        {
            thrown = true;

            //drop the throwable
            throwablePickup.PuppetMasterDrop();
            //set it's positon to the launcher's position and rotation

            //invoke the throwable's throw event
            throwable.Throw();


            LaunchProjectileAsync();

            



        }

       
    }

    public async void LaunchProjectileAsync()
    {



        Rigidbody rb = throwable.GetComponent<Rigidbody>();

        while (rb == null)
        {
            rb = throwable.GetComponent<Rigidbody>();
            await Task.Yield();
        }



        if (rb != null)
        {

            throwable.gameObject.transform.position = throwLauncher.gameObject.transform.position;
            //throwable.gameObject.transform.rotation = throwLauncher.gameObject.transform.rotation;


            //puppetmaster pickup/drop - the puppetmaster system itself
            //adds and removes rigidbody, so this will not work here

            //set the object as dynamic again
            throwLauncher.LauchProjectile(rb);
        }

        //un set throwable 
        throwable = null;

    }


    private void callDropInteraction()
    {
        dropped = true;
        if (leftHand.currentProp != null)
        {


            //need to get the <PickUpItem> from the inventory that matches the item in player's hand
            //get first item in inventory where itemInInventory.prop == currentProp
            PickUpItem itemInHand = inventory.ItemsInHand.Where(item => item.prop == leftHand.currentProp).First();

            itemInHand.PuppetMasterDrop();
        }
        if (rightHand.currentProp != null)
        {

            //need to get the <PickUpItem> from the inventory that matches the item in player's hand
            //get first item in inventory where itemInInventory.prop == currentProp
            PickUpItem itemInHand = inventory.ItemsInHand.Where(item => item.prop == rightHand.currentProp).First();


            itemInHand.PuppetMasterDrop();
        }

        //not dealing with a puppetmaster object
        else
        {
            if (inventory != null && inventory.ItemsInHand.Count > 0)
            {
                inventory.ItemsInHand.Last().Drop();
            }

        }
    }
}



