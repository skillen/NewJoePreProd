﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RootMotion.Dynamics;
using Cinemachine;
using com.ootii.Messages;

namespace Coffee {

    public class PoopShoot : MonoBehaviour {

        [Header("Character")]
        [SerializeField] private PuppetMaster puppetMaster = null;
        [SerializeField] private SkinnedMeshRenderer characterMeshRenderer = null;
        [SerializeField] private CinemachineFreeLook freeLookCamera = null;
        [SerializeField] private Rigidbody characterHipsRigidbody = null;

        [Header("Particles")]
        [SerializeField] private GameObject trailParticles = null;
        [SerializeField] private GameObject explosionPoopParticles = null;
        [SerializeField] private GameObject explosionFeatherParticles = null;

        [Header("Settings")]
        //[Range(1f, 100f)] [SerializeField] private float explodeOnHitThreshold = 5f;    // If character hits something with at least this amount of force it will trigger explosion.
        [Range(0f, 10f)] [SerializeField] private float explodeAfterSeconds = 6f;       // Character will explode after this amount of time has passed while in flight.
        [SerializeField] private AnimationCurve forceCurve = null;                      // Adjusts force and torque applied over time.
        [Range(0f, 2000f)] [SerializeField] private float force = 500f;                 // Flying.
        [Range(0f, 2000f)] [SerializeField] private float torque = 100f;                // Spinning.
        [SerializeField] private ForceMode forceMode = ForceMode.Force;                 // Type of force to apply to both thrust and torque.
        [Range(0f, 90f)] [SerializeField] private float randomLaunchAngleLimit = 45f;   // Limit on random angle applied to Vector3.up for launch direction.
        [SerializeField] private bool followCharacter = false;
    
        private bool _inFlight;
        private bool _exploded;

        private float _lastFrameFixedDeltaTime; // Used to get accurate force applied in collision event.
        private float _elapsedFlightTime;       // Used to get values out of force AnimationCurve.
        private Vector3 _launchDirection;       // Where we be shootin'

        public bool InFlight { get { return _inFlight; } }

        private void FixedUpdate () {

            if (!_exploded && _inFlight) {

                float currentForce = forceCurve.Evaluate(_elapsedFlightTime / explodeAfterSeconds) * force;
        
                // Add increasing thrust.
                characterHipsRigidbody.AddForce(_launchDirection * currentForce, forceMode);

                // Add increasingly crazy spin.
                characterHipsRigidbody.AddRelativeTorque(
                    forceCurve.Evaluate(_elapsedFlightTime / explodeAfterSeconds) * torque, 
                    0f, 
                    0f, 
                    forceMode
                );

                if (_elapsedFlightTime >= explodeAfterSeconds) {
                    Explode();
                }

                _elapsedFlightTime += Time.fixedDeltaTime;
                _lastFrameFixedDeltaTime = Time.fixedDeltaTime;
            }
        }

        private void Explode () {

            //TODO: Spawn gibs.

            // Hide player character.
            characterMeshRenderer.enabled = false;

            // Stop camera look at and follow.
            freeLookCamera.m_LookAt = null;
            freeLookCamera.m_Follow = null;

            // Unparent explosion particles so they stay where explosion happened.
            //TODO: Move feathers slightly in one direction (blowing in the wind).
            explosionFeatherParticles.transform.parent = null;
            explosionPoopParticles.transform.parent = null;

            // Enable explosion particles. Will disable themselves automatically when done playing.
            explosionPoopParticles.SetActive(true);
            explosionFeatherParticles.SetActive(true);
        
            // Disable poop trail.
            trailParticles.SetActive(false);

            // No longer in flight.
            _inFlight = false;

            // Notify listeners. Game over menu should probably be shown, for example.
            //MessageDispatcher.SendMessage(this, MessageDatabase.level_failed, FailType.Poop, 3f);
        }

        public void Launch () {

            puppetMaster.state = PuppetMaster.State.Dead;

            freeLookCamera.m_Follow = followCharacter ? characterHipsRigidbody.transform : null;
            freeLookCamera.m_LookAt = characterHipsRigidbody.transform;

            _launchDirection = GetPointOnUnitSphereCap(Vector3.up, randomLaunchAngleLimit);
            trailParticles.SetActive(true);

            _elapsedFlightTime = 0f;
            _inFlight = true;
        }

        /*private void OnCollisionEnter (Collision collision) {

            // https://docs.unity3d.com/ScriptReference/Collision-impulse.html
            float forceApplied = (collision.impulse / _lastFrameFixedDeltaTime).magnitude;

            if (_inFlight && forceApplied >= explodeOnHitThreshold) {

                Debug.Log("Trying to trigger explosion..");
                //Explode();
            }
        }*/

        // For randomizing trajectory.
        private Vector3 GetPointOnUnitSphereCap (Quaternion targetDirection, float angle) {
		
		    float angleInRad = Random.Range (0f, angle) * Mathf.Deg2Rad;
		    Vector3 PointOnCircle = (Random.insideUnitCircle.normalized) * Mathf.Sin(angleInRad);
		    Vector3 V = new Vector3(PointOnCircle.x, PointOnCircle.y, Mathf.Cos(angleInRad));
		    return targetDirection * V;
	    }
	    private Vector3 GetPointOnUnitSphereCap (Vector3 targetDirection, float angle) {
		
		    return GetPointOnUnitSphereCap(Quaternion.LookRotation(targetDirection), angle);
	    }
    }
}