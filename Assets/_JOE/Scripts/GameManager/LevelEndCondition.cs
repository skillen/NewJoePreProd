﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelEndCondition 
{
    public string testString;

    public string LevelToLoad;

    public GameEventListener eventListener = new GameEventListener();

    public bool Assess(string input)
    {
        bool result = false;

        if (input == testString)
        {
            result = true;
        }

        return result;
    }
}
