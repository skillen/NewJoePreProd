using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TestEventTrigger))]
public class TestEventTriggerEditor : Editor
{



    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TestEventTrigger myTarget = (TestEventTrigger)target;


        if (GUILayout.Button("Fire Event"))
        {
            myTarget.FireEvents();
        }


    }
}
