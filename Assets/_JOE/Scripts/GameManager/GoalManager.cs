using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GoalManager : MonoBehaviour
{
  
    public Goal[] goals;

    public UnityEvent OnGoalsAccomplished;

    public bool GoalCompleted = false;



    void Awake()
    {
        goals = GetComponents<Goal>();
    }

    void OnGUI()
    {
        foreach (var goal in goals)
        {
            goal.DrawHUD();
        }
    }

    void Update()
    {
        foreach (var goal in goals)
        {
            if (goal.GoalAchieved)
            {
                goal.Complete();
                //Destroy(goal);
            }
        }

        if (!GoalCompleted)
        {
            if (goals.Any(goal => !goal.GoalAchieved))
            {

                //not all goals accomplished, return
                return;

            }
            //all goals are accomplished, mark complete, and raise event
            else
            {
                GoalCompleted = true;
                OnGoalsAccomplished.Invoke();
            }
        }
        
    }
}
