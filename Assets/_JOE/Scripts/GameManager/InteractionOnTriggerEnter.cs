using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class InteractionOnTriggerEnter : MonoBehaviour
{

    [TagSelector]
    public string[] triggerTags = new string[] { };


    Animator anim;
    public UnityEvent triggerEntered;

    


    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggerTags.Contains(other.tag))
        {
            Debug.Log("collapse platform");
            triggerEntered.Invoke();
        }
    }
}
