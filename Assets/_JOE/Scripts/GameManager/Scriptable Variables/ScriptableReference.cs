using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ScriptableObjLib.Variables
{
    [Serializable]
    public class ScriptableReference 
    {
        public bool UseConstant = true;
        public float ConstantValue;
        public ScriptableFloat Variable;

        public ScriptableReference()
        { }

        public ScriptableReference(float value)
        {
            UseConstant = true;
            ConstantValue = value;
        }

        public float Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
        }

        public static implicit operator float(ScriptableReference reference)
        {
            return reference.Value;
        }
    }
}
