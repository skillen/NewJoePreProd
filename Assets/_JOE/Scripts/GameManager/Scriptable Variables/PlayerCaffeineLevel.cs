using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjLib.Variables;
using UnityEngine.Events;

public class PlayerCaffeineLevel : MonoBehaviour
{
    [Tooltip("Scriptable var reference to the player's Caffeine level.")]
    public ScriptableFloat caffeineLevel;

    [Tooltip("The caffeine level that the player starts with.")]
    public FloatReference StartLevel;

    [Tooltip("Decay rate of the player's caffeine bar.")]
    public FloatReference CaffeineDecayRate;

    [Tooltip("The minimum that the caffeine bar will decay without being attacked.")]
    public FloatReference Min;

    [Tooltip("Maximum caffeine value that the player can have.")]
    public FloatReference Max;

    [Tooltip("Maximum caffeine value that the player can have.")]
    public ScriptableSpeed SpeedMapper;

    public float caffeinePercentile;

    public UnityEvent onCaffeineDepleted;


    // Start is called before the first frame update
    void Start()
    {
        caffeineLevel.SetValue(StartLevel);

        if (SpeedMapper == null)
        {
            SpeedMapper = GetComponent<ScriptableSpeed>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //prevent value from exceeding maximum
        if (caffeineLevel.Value > Max)
        {
            caffeineLevel.Value = Max;
        }

        //iff caffiene value is greater than min decrease it according to the decay
        if (caffeineLevel.Value > Min)
        {
            caffeineLevel.ApplyChange(0 - (Time.deltaTime * CaffeineDecayRate));

            //if it dips below the min, then set it to the min
            if (caffeineLevel.Value < Min)
            {
                caffeineLevel.SetValue(Min);
            }

        }

        if (SpeedMapper != null)
        {
            /*float*/ caffeinePercentile = Mathf.InverseLerp(Min.Value, Max.Value, caffeineLevel.Value); //(caffeineLevel.Value / Max)  - (Min / 50);
            SpeedMapper.RemapSpeed(caffeinePercentile);
        }
       
    }

    public void TakeDamage(float dmgAmt)
    {
        caffeineLevel.ApplyChange((Mathf.Abs(dmgAmt) * -1));

        if (caffeineLevel.Value <= 0)
        {
            onCaffeineDepleted.Invoke();
        }
    }

}
