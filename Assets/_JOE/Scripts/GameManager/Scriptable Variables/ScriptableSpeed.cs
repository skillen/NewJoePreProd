using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjLib.Variables
{
    [ExecuteInEditMode]
    public class ScriptableSpeed : MonoBehaviour
    {
        //see AudioParameterSetting from unite2017 project

        [Tooltip("Name of the parameter to set in the player stats.")]
        public string ParameterName = "Speed";

        [Tooltip("The value to set the parameter.")]
        [Range(0, 1)]
        public float Value;

        public ScriptableFloat ScriptableInputValue;

        [Tooltip("Variable to send to the mixer parameter.")]
        public float Variable;

        

        [Tooltip("Minimum value of the Variable that is mapped to the curve.")]
        public FloatReference MinSpeed;

        [Tooltip("Maximum value of the Variable that is mapped to the curve.")]
        public FloatReference MaxSpeed;




        [Tooltip("Curve to evaluate in order to look up a final value to send as the parameter.\n" +
                 "T=0 is when Variable == Min\n" +
                 "T=1 is when Variable == Max")]
        public AnimationCurve Curve;


        public void RemapSpeed(float CaffeinePercentile)
        {
            //set this so I can check it in the inspector
            Value = CaffeinePercentile;

            //take the percentage value and curve it
            float t = Curve.Evaluate(Value);

            //then use linear interpolation to translate the curved % value to the actual speed
            Variable = Mathf.Lerp(MinSpeed, MaxSpeed, t);

            //Mixer.SetFloat(ParameterName, value);
        }
    }
}
