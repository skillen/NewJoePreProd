using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjLib.Variables
{
    [CreateAssetMenu]
    public class ScriptableFloat : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public float Value;

        public void SetValue(float value)
        {
            Value = value;
        }

        public void SetValue(ScriptableFloat value)
        {
            Value = value.Value;
        }

        public void ApplyChange(float amount)
        {
            Value += amount;
        }

        public void ApplyChange(ScriptableFloat amount)
        {
            Value += amount.Value;
        }
    }
}