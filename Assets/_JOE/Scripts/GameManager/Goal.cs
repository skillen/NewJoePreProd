using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Goal : MonoBehaviour
{
    [Header("Debug")]
    [SerializeField]
    private bool goalAchieved;

    public bool GoalAchieved => goalAchieved = IsAchieved();




    //check for criteria
    public abstract bool IsAchieved();
    //results when achived
    public abstract void Complete();
    //display quest progress
    public abstract void DrawHUD();


}
