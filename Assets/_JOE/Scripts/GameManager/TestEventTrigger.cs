using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestEventTrigger : MonoBehaviour
{
    //If you want to pass UnityEvent to the AddListener method, you can use lambda expresions like this:
    //btn.onClick.AddListener(() => unityEvent?.Invoke());
    //Notice the "?" to avoi calling null (empty) actions!



    public UnityAction testAction;


    public UnityEvent TestEvent;

    

    public void FireEvents()
    {
        TestEvent.Invoke();
    }


    void Update()
    {
        // Press Q to close the Listener
        //if (Input.GetKeyDown("q") && TestEvent != null)
        //{
        //    //Debug.Log("Quitting");
        //    TestEvent.AddListener(MyAction);
        //}

    }

    public void MyAction()
    {
        //Output message to the console
        Debug.Log("Do Stuff");
    }



}
