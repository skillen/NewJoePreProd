using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjLib.Variables;


public class BrewThresholdGoal : Goal
{
    public ScriptableFloat brewedCoffee;

    public float requiredAmount;

   


    public override bool IsAchieved()
    {
        return (brewedCoffee.Value >= requiredAmount);
    }

    public override void Complete()
    {
        Debug.Log("coffee brewed goal completed");
    }

    public override void DrawHUD()
    {
        GUILayout.Label(string.Format("Brewed {0}/{1} Coffee", brewedCoffee.Value, requiredAmount));
    }

    public void debugMessage()
    {
        Debug.Log("brew threshold met");
    }

    

   
}
