﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelOutcome : MonoBehaviour
{
    public List<LevelEndCondition> conditions;

    public string resultName;

    public string SceneToLoad;




    public void LoadNewScene()
    {
        LevelLoader sceneManager = GameObject.Find("SceneManager").GetComponent<LevelLoader>();
        if (sceneManager != null)
        {
            sceneManager.SceneName = SceneToLoad;
            sceneManager.loadScene();
        }
    }

    public void example(string testString)
    {
        foreach (LevelEndCondition condition in conditions)
        {
            if (condition.Assess(testString))
            {
                SceneToLoad = condition.LevelToLoad;
                LoadNewScene();
            }
        }
    }

   
}
