﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using com.ootii.Messages;
using Rewired;

namespace Coffee {

    public class CoffeeDoor : MonoBehaviour {

        [Header("Transition")]
        //[SerializeField] private float transitionSpeed = 1f;    //TODO: Not used yet.     
        [SerializeField] private CoffeeDoor destinationDoor = null;
        [SerializeField] private Transform playerPutLocation = null;   // Where player character ends up when this door is the destination for some other door.

        [Header("Prompt")]
        [SerializeField] private SpriteRenderer promptIcon = null;
        [SerializeField] private Sprite promptSpriteXbox = null;
        [SerializeField] private Sprite promptSpritePlaystation = null;
        [SerializeField] private Sprite promptSpriteKeyboard = null;

        [Header("Trigger")]
        [SerializeField] private GameObject highlight;
        [SerializeField] private string triggerTag = "Puppet Activation";

        private Player _playerInput;
        private bool _isTriggered = false;
        private bool _isTransition = false;

        public Transform Location { get { return playerPutLocation; } }

        private void Awake() {

            _playerInput = ReInput.players.GetPlayer(0);

            promptIcon.gameObject.SetActive(false);
            highlight.SetActive(false);
        }

        private void Start () {

            //CoffeeInputController newController = InputDeviceChecker.Instance.CurrentController;
            //if (newController == CoffeeInputController.Keyboard) {
            //    promptIcon.sprite = promptSpriteKeyboard;
            //}
            //else if (newController == CoffeeInputController.Playstation) {
            //    promptIcon.sprite = promptSpritePlaystation;
            //}
            //else if (newController == CoffeeInputController.Xbox) {
            //    promptIcon.sprite = promptSpriteXbox;
            //}
        }

        private void Update() {
            
            if (_isTriggered && !_isTransition) {

                if (_playerInput.GetButtonDown("Interact")) {
                    StartCoroutine(UseDoor());
                }
            }
        }

        private IEnumerator UseDoor () {

            _isTransition = true;

            //TODO: A nice transition.
            //CoffeePlayer.Instance.TeleportTo(destinationDoor.Location);

            yield return null;

            _isTransition = false;
        }

        private void OnEnable () {

            MessageDispatcher.AddListener(MessageDatabase.input_controller_changed, OnInputDeviceChanged);
        }

        private void OnDisable () {

            MessageDispatcher.RemoveListener(MessageDatabase.input_controller_changed, OnInputDeviceChanged);
        }

        private void OnInputDeviceChanged (IMessage message) {

            //CoffeeInputController newController = (CoffeeInputController)message.Data;
            //if (newController == CoffeeInputController.Keyboard) {
            //    promptIcon.sprite = promptSpriteKeyboard;
            //}
            //else if (newController == CoffeeInputController.Playstation) {
            //    promptIcon.sprite = promptSpritePlaystation;
            //}
            //else if (newController == CoffeeInputController.Xbox) {
            //    promptIcon.sprite = promptSpriteXbox;
            //}
        }

        private void OnTriggerEnter(Collider other) {
            
            if (other.tag == triggerTag) {

                promptIcon.gameObject.SetActive(true);
                highlight.SetActive(true);
                _isTriggered = true;
            }
        }

        private void OnTriggerExit(Collider other) {
            
            if (other.tag == triggerTag) {

                promptIcon.gameObject.SetActive(false);
                highlight.SetActive(false);
                _isTriggered = false;
            }
        }
    }
}