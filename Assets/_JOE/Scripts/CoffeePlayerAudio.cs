﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Coffee {

    public enum PlayerAudioType {

        Sip = 0,
        Footstep = 1,
        Water_Splash = 2,
        Use_Door = 3,
        Bean_Collect = 4,
        Fire_Start = 5,
        Fire_Burn = 6,
        Fire_End = 7,
        Collision = 8,
    }

    public class CoffeePlayerAudio : MonoBehaviour {

        [SerializeField] private List<AudioClip> sipClips;
        [SerializeField] private List<AudioClip> footStepClips;
        [SerializeField] private List<AudioClip> waterSplashClips;
        [SerializeField] private AudioClip useDoorClip = null;
        [SerializeField] private AudioClip beanCollectClip = null;
        [SerializeField] private AudioClip fireStartClip = null;
        [SerializeField] private AudioClip fireBurnClip = null;
        [SerializeField] private AudioClip fireStopClip = null;
        [SerializeField] private List<AudioClip> collisionClips;

        private Dictionary<PlayerAudioType, List<AudioClip>> _map = new Dictionary<PlayerAudioType, List<AudioClip>>();

        // The key is the ID returned by Play() when loop parameter is true.
        private Dictionary<int, AudioSource> _playingLoopedAudio = new Dictionary<int, AudioSource>();

        private static int nextLoopedClipID = 0;

        private void Awake () {

            _map.Add(PlayerAudioType.Sip, sipClips);
            _map.Add(PlayerAudioType.Footstep, footStepClips);
            _map.Add(PlayerAudioType.Water_Splash, waterSplashClips);
            _map.Add(PlayerAudioType.Use_Door, new List<AudioClip> { useDoorClip });
            _map.Add(PlayerAudioType.Bean_Collect, new List<AudioClip> { beanCollectClip });
            _map.Add(PlayerAudioType.Fire_Start, new List<AudioClip> { fireStartClip });
            _map.Add(PlayerAudioType.Fire_Burn, new List<AudioClip> { fireBurnClip });
            _map.Add(PlayerAudioType.Fire_End, new List<AudioClip> { fireStopClip });
            _map.Add(PlayerAudioType.Collision, collisionClips);
        }
        
        /// <summary>
        /// Starts playing a looping audio clip with 2D spatial blend for given type/category.
        /// Audio will be heard clearly no matter the distance from AudioListener to AudioSource.
        /// </summary>
        public int PlayLooping (PlayerAudioType id) {

            GameObject newObj = new GameObject();
            newObj.name = "Looped " + id.ToString() + " Audio";

            AudioSource audioSource = newObj.AddComponent<AudioSource>();
            audioSource.spatialBlend = 0f;  // 2D (global audio)
            audioSource.clip = GetRandomClip(id);
           
            _playingLoopedAudio.Add(nextLoopedClipID, audioSource);
            nextLoopedClipID++;

            audioSource.Play();

            return nextLoopedClipID - 1;
        }

        /// <summary>
        /// Starts playing a looping audio clip with 3D spatial blend for given audio type/category. 
        /// Sets new AudioSource as a child of given parent so that audio follows it.
        /// Audio will be heard most clearly when AudioListener is near the generated AudioSource.
        /// </summary>
        public int PlayLooping (PlayerAudioType type, Transform parent) {

            GameObject newObj = new GameObject();
            newObj.name = "Looped " + type.ToString() + " Audio";
            newObj.transform.parent = parent;
            newObj.transform.localPosition = Vector3.zero;

            AudioSource audioSource = newObj.AddComponent<AudioSource>();
            audioSource.spatialBlend = 1f;  // 3D (local audio)
            audioSource.clip = GetRandomClip(type);

            _playingLoopedAudio.Add(nextLoopedClipID, audioSource);
            nextLoopedClipID++;

            audioSource.Play();

            return nextLoopedClipID - 1;
        }

        /// <summary>
        /// Stops playing a looped audio clip given looped audio ID.
        /// </summary>
        public void Stop (int id) {

            if (_playingLoopedAudio.ContainsKey(id)) {

                AudioSource source = _playingLoopedAudio[id];
                _playingLoopedAudio.Remove(id);
                Destroy(source.gameObject);
            }
            else {
                Debug.LogWarning("CoffeePlayerAudio: Trying to stop looped audio with ID=" + id + " but it doesn't exist in dictionary.");
            }
        }

        /// <summary>
        /// Plays a 3D audio clip matching given category at the given position.
        /// </summary>
        public void PlayOneShot (PlayerAudioType type, Vector3 position) {
            
            AudioClip clip = GetRandomClip(type);
            if (clip != null) {
                AudioSource.PlayClipAtPoint(GetRandomClip(type), position);
            }
            else {
                Debug.LogWarning("CoffeePlayerAudio: Audio clip still needs to be added for " + type.ToString());
            }
        }

        /// <summary>
        /// Plays a 3D audio clip matching given category at position of Camera.main transform.
        /// Will do nothing if a Camera with "Main Camera" tag does not exist in scene.
        /// Main camera should have an AudioListener component.
        /// </summary>
        public void PlayOneShot (PlayerAudioType type) {
            
            AudioClip clip = GetRandomClip(type);
            if (clip != null) {

                if (Camera.main != null) {
                    AudioSource.PlayClipAtPoint(GetRandomClip(type), Camera.main.transform.position);
                }
                else {
                    Debug.LogWarning("CoffeePlayerAudio.PlayOneShot(PlayerAudioType) couldn't find main camera in scene. Not playing audio.");
                }
            }
            else {
                Debug.LogWarning("CoffeePlayerAudio: Audio clip still needs to be added for " + type.ToString());
            }
        }

        /// <summary>
        /// Returns a random audio clip under given category (PlayerAudioType).
        /// </summary>
        private AudioClip GetRandomClip (PlayerAudioType type) {

            return _map.ContainsKey(type) ? _map[type][Random.Range(0, _map[type].Count)] : null;
        }
    }
}
