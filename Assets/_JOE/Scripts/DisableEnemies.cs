﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RootMotion.Dynamics;
using UnityEngine.Events;

public class DisableEnemies : MonoBehaviour
{



    [TagSelector]
    public string[] TagFilterArray = new string[] { };

    public bool TrapIsActive;

    public bool ActiveForOneFrame = false;
    public bool trapActivated = false;

    public bool isProjectile = false;

    public int CanDisable;

    public int numberDisabled = 0;

    public UnityEvent TriggerAfterActivation;

    private void OnTriggerStay(Collider other)
    {


        if (TrapIsActive && TagFilterArray.Contains(other.tag))
        {
            PuppetMaster enemyPuppet = other.GetComponentInParent<PuppetMaster>();

            if (numberDisabled < CanDisable && enemyPuppet != null && enemyPuppet.state != PuppetMaster.State.Dead)
            {
                enemyPuppet.state = PuppetMaster.State.Dead;
                numberDisabled++;

            }


            

        }

        trapActivated = true;
        //TriggerAfterActivation.Invoke();

    }

    public void triggerForOneFrame()
    {
        ActiveForOneFrame = true;
        trapActivated = false;
    }

    public void Update()
    {
       
        if (ActiveForOneFrame)
        {
            TrapIsActive = true;
            
        }
        
    }

    private void LateUpdate()
    {
        if (ActiveForOneFrame && trapActivated && TrapIsActive)
        {

            TriggerAfterActivation.Invoke();


            TrapIsActive = false;
            ActiveForOneFrame = false;
        }
    }


}
