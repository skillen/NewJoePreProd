﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class DecaffeinatedAttackTrigger : MonoBehaviour
{
    [TagSelector]
    public string[] TagFilterArray = new string[] { };

    [SerializeField] private Animator anim = null;
    public NavMeshAgent meshAgent;
    [Range(0f, 2.5f)] [SerializeField] private float agentSpeed = 1f;

    private Transform target = null;

    private void OnTriggerEnter(Collider other)
    {

        if (TagFilterArray.Contains(other.tag))
        {

            //target = other.gameObject.transform;

            anim.SetTrigger("Attack");
            //meshAgent.isStopped = true; ;
        }
    }

    public void resumeFollow()
    {
        meshAgent.isStopped = false;
    }

    private void Update()
    {

        if (target != null)
        {

        }
    }

    public void OnRecovered()
    {

        // Get back up into follow animation.
        if (target != null)
        {
            anim.Play("Zombie_Walk_01");
        }
    }
}
