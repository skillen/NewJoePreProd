﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Coffee {

	public static class MessageDatabase {

		// Input.
		public static string input_controller_changed = "CONTROLLER_CHANGED";

		// Tutorial.
		public static string tutorial_step_triggered = "TUT_STEP_ENTERED";				// Player entered trigger collider and a message is displayed.
		public static string tutorial_blocking_step_completed = "TUT_STEP_COMPLETED";	// Player completed a blocking tutorial step (time frozen, prompt to continue).

		// Coffee Select.
		public static string coffee_selected = "COFFEE_SELECTED";

		// Level.
		public static string level_loaded = "LEVEL_LOADED";
		public static string level_start = "LEVEL_START";       // This is maybe not needed. "coffee_selected" message does the same.
		public static string level_paused = "LEVEL_PAUSED";
		public static string level_resumed = "LEVEL_RESUMED";
		public static string level_failed = "LEVEL_FAILED";
		public static string level_completed = "LEVEL_COMPLETED";
        public static string level_timer_tick = "LEVEL_TIMER_TICK";
        
        // Interactions.
        public static string item_collected = "ITEM_COLLECTED";
        public static string water_touched = "WATER_TOUCHED";
        public static string fire_touched = "FIRE_TOUCHED";
        public static string mascot_knockdown = "MASCOT_KNOCKDOWN";
        public static string on_fire_update = "ON_FIRE_UPDATE";
        public static string on_fire_extinguished = "ON_FIRE_EXTINGUISH";

        // Menus.
        public static string menu_title_continue = "TITLE_CONTINUE";
        public static string menu_main_back = "MAIN_MENU_BACK";

        public static string level_select_open = "LEVEL_SELECT_OPEN";
        public static string level_select_back = "LEVEL_SELECT_BACK";

        // -----------------------------------------------------------------
        // MULTIPLAYER
        // -----------------------------------------------------------------

        // All players are in the ready state in character select menu.
        public static string mp_character_select_all_ready = "MP_ALL_CHARACTERS_READY";

        // Tag multiplayer mode.
        public static string mp_tag_coffee_dropped = "MP_TAG_COFFEE_DROPPED";
        public static string mp_tag_coffee_collected = "MP_TAG_COFFEE_COLLECTED";
    }
}
