using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

namespace Coffee {

    public enum PathType {
        Looped, PingPong, OneShot,
    }

    public class MovingNPC : MonoBehaviour {

        [SerializeField] private Animator anim = null;
        [SerializeField] private NavMeshAgent agent = null;
        [SerializeField] private CoffeePedestrian pedestrian = null;
        [Range(0f, 2.5f)] [SerializeField] private float agentSpeed = 1f;

        private Transform target = null;

        [SerializeField] private Transform waypoints = null;

        [SerializeField] private PathType _pathType = PathType.Looped;

        private int currrentWaypoint = 0;
        private AnimationType _defaultAnim;

        private bool ppForward = true;                  // Ping Pong Direction

        private void Start() {

            _defaultAnim = pedestrian.AnimType;
            agent.speed = agentSpeed;
            agent.angularSpeed = 300;
            agent.enabled = true;
            target = NextWaypoint(0);

            anim.Play("male_move_walk_front");
        }

        private Transform NextWaypoint(int newWaypoint) {
            return waypoints.GetChild(newWaypoint);
        }


        private void Update() {

            if (target != null) {
                agent.SetDestination(target.position);
            }
        }

        private void OnTriggerEnter(Collider other) {
            if (other.tag == "Waypoint") {

                if (_pathType == PathType.Looped) {
                    other.transform.SetAsLastSibling();
                    target = NextWaypoint(0);
                }

                else if (_pathType == PathType.PingPong) {

                    if (ppForward) {

                        if (currrentWaypoint < (waypoints.childCount - 1)) {
                            currrentWaypoint++;
                        }
                        else {
                            ppForward = false;
                            currrentWaypoint--;
                        }
                    }

                    else {
                        if (currrentWaypoint > 0) {
                            currrentWaypoint--;
                        }
                        else {
                            ppForward = true;
                            currrentWaypoint++;
                        }
                    }                    
                    target = NextWaypoint(currrentWaypoint);
                }

                else if (_pathType == PathType.OneShot) {
                    if (waypoints.childCount > 1) {
                        target = NextWaypoint(1);
                        Destroy(other.gameObject);
                    }
                    else {
                        target = null;
                        Destroy(other.gameObject);
                        ReachedDestination();
                    }
                }
            }
        }

        private void ReachedDestination() {
            anim.Play(_defaultAnim.ToString());
            agent.enabled = false;
        }
    }
}