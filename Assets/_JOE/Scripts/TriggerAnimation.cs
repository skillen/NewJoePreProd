﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class TriggerAnimation : MonoBehaviour
{
    Animator anim;
    public UnityEvent triggerEntered;

    [TagSelector]
    public string[] triggerTags = new string[] { };


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
            if (triggerTags.Contains(other.tag))
        {
            Debug.Log("collapse platform");
            triggerEntered.Invoke();
        }
    }

}
