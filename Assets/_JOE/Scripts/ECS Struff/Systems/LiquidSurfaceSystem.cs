using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;
using Unity.Transforms;


[MaterialProperty("_ShallowColor", MaterialPropertyFormat.Float4)]
public struct ShallowColor : IComponentData
{
    public float4 Value;
}

[MaterialProperty("_DeepColor", MaterialPropertyFormat.Float4)]
public struct DeepColor : IComponentData
{
    public float4 Value;
}


public class LiquidSurfaceSystem : SystemBase
{
    protected override void OnUpdate()
    {
        // Assign values to local variables captured in your job here, so that it has
        // everything it needs to do its work when it runs later.
        // For example,
        //     float deltaTime = Time.DeltaTime;

        // This declares a new kind of job, which is a unit of work to do.
        // The job is declared as an Entities.ForEach with the target components as parameters,
        // meaning it will process all entities in the world that have both
        // Translation and Rotation components. Change it to process the component
        // types you want.



        //Entities.WithAll<LiquidSurfaceData>().
        //   ForEach((ref DeepColor deepColor, ref ShallowColor shallowColor, ref Translation currentPosition, in LiquidSurfaceData surfaceData) =>
        //   {
        //       //getting data from our target entity to update the 
        //       //ComponentDataFromEntity<Translation> allTranslations = GetComponentDataFromEntity<Translation>(true);
        //       //Translation targetPos = allTranslations[surfaceData.target];

        //       ComponentDataFromEntity<CutoffPlane> allCutoffPlanes = GetComponentDataFromEntity<CutoffPlane>(true);
        //       CutoffPlane targetCutoff = allCutoffPlanes[surfaceData.target];

        //       ComponentDataFromEntity<MainColor> allMainColors = GetComponentDataFromEntity<MainColor>(true);
        //       MainColor targetDeepColor = allMainColors[surfaceData.target];

        //       ComponentDataFromEntity<CutoffColor> allSurfaceColors = GetComponentDataFromEntity<CutoffColor>(true);
        //       CutoffColor targetSurfaceColor = allSurfaceColors[surfaceData.target];


        //       shallowColor.Value = targetSurfaceColor.Value;
        //       deepColor.Value = targetDeepColor.Value;

        //       //calculate the surface height of the 

        //       float surfaceHeight = -1 * targetCutoff.Value.w;

        //       currentPosition.Value = new Vector3(currentPosition.Value.x, surfaceHeight, currentPosition.Value.z);


        //   })
        //.Schedule();
    }
}
