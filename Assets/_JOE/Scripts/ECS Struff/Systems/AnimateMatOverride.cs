using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;
using Unity.Transforms;


[MaterialProperty("_Color", MaterialPropertyFormat.Float4)]
public struct MainColor : IComponentData
{
    public float4 Value;
}

[MaterialProperty("_CutoffColor", MaterialPropertyFormat.Float4)]
public struct CutoffColor : IComponentData
{
    public float4 Value;
}

[MaterialProperty("_MyAnimationTime", MaterialPropertyFormat.Float)]
public struct MyAnimationTime : IComponentData
{
    public float Value;
    
}

[MaterialProperty("_Plane", MaterialPropertyFormat.Float4)]
public struct CutoffPlane : IComponentData
{
    public float4 Value;

}



public class AnimateMatOverrideBase : SystemBase
{

   



    protected override void OnUpdate()
    {


        float dT = Time.DeltaTime;

        Entities.ForEach((ref MainColor color, ref MyAnimationTime t, ref CutoffPlane cutoffPlane, in Translation translation, in FillLevel fillLevel, in WorldRenderBounds renderBounds) =>
        {

            float top = renderBounds.Value.Center.y + renderBounds.Value.Extents.y + 0.001f;
            float bottom = renderBounds.Value.Center.y - renderBounds.Value.Extents.y - 0.001f;

            float planeHeight = math.clamp(  math.lerp(bottom, top, fillLevel.Value), bottom, top);

            Plane plane = new Plane(Vector3.up, new Vector3(translation.Value.x, planeHeight, translation.Value.z));

            Vector4 planeRepresentation = new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance);

            cutoffPlane.Value = planeRepresentation;

            t.Value = t.Value + dT;
            color.Value = new float4(
                math.cos(t.Value + 1.0f),
                math.cos(t.Value + 2.0f),
                math.cos(t.Value + 3.0f),
                1.0f);
        })
            .Schedule();

        //update the liquid surface position and color
        Entities.WithAll<LiquidSurfaceData>().
           ForEach((ref DeepColor deepColor, ref ShallowColor shallowColor, ref Translation currentPosition, ref Rotation currentRotation, in LiquidSurfaceData surfaceData) =>
           {


               //getting data from our target entity to update the 
               //ComponentDataFromEntity<Translation> allTranslations = GetComponentDataFromEntity<Translation>(true);
               //Translation targetPos = allTranslations[surfaceData.target];

             
               ComponentDataFromEntity<CutoffPlane> allCutoffPlanes = GetComponentDataFromEntity<CutoffPlane>(true);
               

               ComponentDataFromEntity<MainColor> allMainColors = GetComponentDataFromEntity<MainColor>(true);
               

               ComponentDataFromEntity<CutoffColor> allSurfaceColors = GetComponentDataFromEntity<CutoffColor>(true);
               
               //prevent errors
               if (!allCutoffPlanes.HasComponent(surfaceData.target) || !allMainColors.HasComponent(surfaceData.target) || !allSurfaceColors.HasComponent(surfaceData.target))
               {
                   return;
               }

               CutoffPlane targetCutoff = allCutoffPlanes[surfaceData.target];
               MainColor targetDeepColor = allMainColors[surfaceData.target];
               CutoffColor targetSurfaceColor = allSurfaceColors[surfaceData.target];


               shallowColor.Value = targetSurfaceColor.Value;  //targetDeepColor.Value;

               float percent = -60;
               var color = targetSurfaceColor.Value;

               var R = color.x;
               var G = color.y; ;
               var B = color.z; ;

               R = (R * (100 + percent) / 100);
               G = (G * (100 + percent) / 100);
               B = (B * (100 + percent) / 100);

               R = (R < 255) ? R : 255;
               G = (G < 255) ? G : 255;
               B = (B < 255) ? B : 255;


               Vector4 newDeepColor = new Vector4(R, G, B, color.w);

               deepColor.Value = newDeepColor;

               //calculate the surface height of the 

               float surfaceHeight = -1 * targetCutoff.Value.w;

               currentPosition.Value = new Vector3(currentPosition.Value.x, surfaceHeight, currentPosition.Value.z);

               //currentRotation.Value = targetCutoff.Value;


           })
        .Schedule();



    }


}
