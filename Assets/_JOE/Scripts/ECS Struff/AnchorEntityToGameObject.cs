using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class AnchorEntityToGameObject : MonoBehaviour
{

    [SerializeField]
    private ConvertedEntityHolder convertedEntityHolder;
      

    Entity entity;
    EntityManager entityManager;

    //[SerializeField]
    //private List<ConvertedEntityHolder> convertedEntityHolders;

    List<Entity> entities;
    List<EntityManager> entityManagers;


    // Start is called before the first frame update
    void Start()
    {
        entity = convertedEntityHolder.GetEntity();
        entityManager = convertedEntityHolder.GetEntityManager();

        //foreach (ConvertedEntityHolder entityHolder in convertedEntityHolders)
        //{
        //    entities.Add(entityHolder.GetEntity());
        //    entityManagers.Add(convertedEntityHolder.GetEntityManager());
        //}
    }

    // Update is called once per frame
    void Update()
    {

        //update the rotation and position of the entity

        //foreach (Entity entity in entities)
        //{
        //    entityManagers[entities.IndexOf(entity)].SetComponentData(entity, new Rotation { Value = gameObject.transform.rotation });
        //    entityManagers[entities.IndexOf(entity)].SetComponentData(entity, new Translation { Value = gameObject.transform.position });
        //}

        entityManager.SetComponentData(entity, new Rotation { Value = gameObject.transform.rotation });
        entityManager.SetComponentData(entity, new Translation { Value = gameObject.transform.position });

    }
}
