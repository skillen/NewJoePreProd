﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using com.ootii.Messages;

namespace Coffee {

    public enum CollectibleItemType {
        Golden_Coffee_Bean,
    }

    public class CollectibleItem : MonoBehaviour {

        [SerializeField] private CollectibleItemType type;
        [SerializeField] private GameObject collectionParticles;
        
        public CollectibleItemType Type { get { return type; } }
        public bool IsCollected => _collected;

        private bool _collected = false;

        public void Collect () {

            if (!_collected) {

                _collected = true;

                // Spawn particles, if set. Particles should be set up to automatically destroy itself when finished.
                if (collectionParticles != null) {
                    GameObject obj = Instantiate(collectionParticles) as GameObject;
                    obj.transform.position = transform.position;
                }

                // Clean up.
                Destroy(gameObject);
            }
        }
    }
}
