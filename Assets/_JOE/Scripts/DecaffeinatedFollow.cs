﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using System;

namespace Coffee {

    public class DecaffeinatedFollow : MonoBehaviour {
        [TagSelector]
        public string[] TagFilterArray = new string[] { };

        [SerializeField] private Animator anim = null;
        [SerializeField] private NavMeshAgent agent = null;
        [SerializeField] private bool followEnabled = false;
        [Range(0f, 2.5f)] [SerializeField] private float agentSpeed = 1f;

        float followCountdown;
        bool targetDestination = false;

        private Transform target = null;
       
        private void OnTriggerEnter (Collider other) {
            
            if (followEnabled && TagFilterArray.Contains(other.tag))
            {
                MoveToTarget(other.gameObject.transform);
            }
        }

        public void MoveToTarget(Transform moveTarget)
        {
            agent.isStopped = false;
            agent.speed = agentSpeed;
            agent.angularSpeed = 300;
            agent.enabled = true;
            target = moveTarget;

            anim.Play("Zombie_Walk_01");
        }

        private void Update () {

            followCountdown -= Time.deltaTime;
            if (followCountdown <= 0f && targetDestination)
            {
                StopFollowing();
            }

            if (followEnabled && target != null) {
                agent.SetDestination(target.position);

                anim.SetFloat("Forward", agent.velocity.magnitude);
            }
        }

        

        public void OnRecovered () {

            // Get back up into follow animation.
            if (target != null) {
                anim.Play("Zombie_Walk_01");
            }
        }

        public void GoToDestinationForSetTime(Transform moveTarget, float time)
        {
            agent.isStopped = false;
            followCountdown = time;
            targetDestination = true;

            agent.speed = agentSpeed;
            agent.angularSpeed = 300;
            agent.enabled = true;
            agent.SetDestination(moveTarget.position); 

            anim.Play("Zombie_Walk_01");
        }
        
        private void StopFollowing()
        {
            if (target != null)
            {
                agent.isStopped = true;
                anim.Play("Idle_Male_1");
            }
            targetDestination = false;
            
        }
    }
}