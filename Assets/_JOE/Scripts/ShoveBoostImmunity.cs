﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.Dynamics;

public class ShoveBoostImmunity : MonoBehaviour
{

    public Animator anim;
    //use to activate the effects only once per animation cycle
    bool first = true;

    public float immunity = 50;
    public float impulseMlp = 50;

    public BehaviourPuppet puppet;

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponentInChildren<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (first && anim.GetCurrentAnimatorStateInfo(1).IsName("ShoveAnimation"))
        {
            //Debug.Log("shoving is active");
            first = false;

            puppet.Boost(immunity, impulseMlp);


        }
        if (!anim.GetCurrentAnimatorStateInfo(1).IsName("ShoveAnimation"))
        {
            first = true;
        }
    }
}
