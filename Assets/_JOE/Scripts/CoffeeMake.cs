﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Rewired;
using com.ootii.Messages;
using TMPro;

namespace Coffee {

	public class CoffeeMake : MonoBehaviour {

		[Header("Camera")]
		public GameObject cameraAngle;

        [Header("Animation")]
        [Tooltip("This should be the animator for the coffee maker and the k-cup. When player is ready and gives input a quick animation will play loading the coffee maker and making player's coffee for the level.")]
        public Animator coffeeMakerAnimator;

		[Header("Input")]
		public GameObject selectPromptXbox;
		public GameObject selectPromptPlaystation;
		public GameObject selectPromptKeyboard;
		
		private Player _playerInput;
        private bool _isExiting;

		public bool IsRunning { get; private set; }

		private void Awake () {

			_playerInput = ReInput.players.GetPlayer(0);
		}

        private void Start() {

            RefreshPrompts();
        }

        private void OnEnable () {
			
			MessageDispatcher.AddListener(MessageDatabase.input_controller_changed, OnControllerChanged);
			MessageDispatcher.AddListener(MessageDatabase.level_loaded, OnLevelLoaded);
		}

		private void OnDisable () {

			MessageDispatcher.RemoveListener(MessageDatabase.input_controller_changed, OnControllerChanged);
			MessageDispatcher.RemoveListener(MessageDatabase.level_loaded, OnLevelLoaded);
		}

        private void RefreshPrompts () {

            //CoffeeInputController newController = InputDeviceChecker.Instance.CurrentController;
            //selectPromptKeyboard.SetActive(newController == CoffeeInputController.Keyboard);
            //selectPromptXbox.SetActive(newController == CoffeeInputController.Xbox);
            //selectPromptPlaystation.SetActive(newController == CoffeeInputController.Playstation);
        }

		private void OnControllerChanged (IMessage message) {

            Debug.Log("<b>Coffee Make got controller changed message.</b>");
            RefreshPrompts();
		}

		private void OnLevelLoaded (IMessage message) {

            //Debug.Log("Level loaded message. Toggling coffee make ON");
			Toggle(true);
		}

		public void Toggle (bool show) {

			// Toggle camera.
			cameraAngle.SetActive(show);

			// Should this script be updating and checking for player input?
			IsRunning = show;
		}

		private void Update () {
			
			if (IsRunning && !_isExiting && _playerInput.GetButtonDown("Menu Select")) {
                Select();
			}
		}

		private void Select () {

			// Dispatch message for listeners. Player's got their coffee and is ready to go!
			MessageDispatcher.SendMessage(MessageDatabase.coffee_selected);
            
            // Toggle menu.
			Toggle(false);
		}
	}
}




















