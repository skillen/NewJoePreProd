﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleWheels : MonoBehaviour {

	[SerializeField] private SWS.splineMove mover;
	[SerializeField] private List<Transform> frontWheels;
	[SerializeField] private List<Transform> rearWheels;
	[SerializeField] private Vector3 localRotationAxis = Vector3.right;
	[Range(0f, 360f)] [SerializeField] private float rotationAngle = 180f;

	private void Update () {
		
		for (int i = 0; i < frontWheels.Count; i++) {
			frontWheels[i].Rotate(localRotationAxis, rotationAngle * mover.speed * Time.deltaTime, Space.Self);
		}

		for (int i = 0; i < rearWheels.Count; i++) {
			rearWheels[i].Rotate(localRotationAxis, rotationAngle * mover.speed * Time.deltaTime, Space.Self);
		}
	}
}
