﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Can b useful for debugging if there's something you want to be visible in the editor
/// but hidden at runtime.
/// </summary>
public class DestroyOnStart : MonoBehaviour {
    
    void Start () {
        Destroy(gameObject);
    }
}
