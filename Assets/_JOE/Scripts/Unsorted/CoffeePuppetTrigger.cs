﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using com.ootii.Messages;

namespace Coffee {

    public class CoffeePuppetTrigger : MonoBehaviour {

	    private int _waterLayer;
	    private int _fireLayer;
        private int _itemLayer;

	    void Awake () {

		    _waterLayer = LayerMask.NameToLayer ("Water");
		    _fireLayer = LayerMask.NameToLayer ("Fire");
            _itemLayer = LayerMask.NameToLayer("Item");
	    }

	    void OnTriggerEnter (Collider other) {

		    int otherLayer = other.gameObject.layer;

            if (otherLayer == _itemLayer) {

                // Get the item reference and run collection function on it.
                CollectibleItem item = other.GetComponent<CollectibleItem>();
                
                // Check if item is already collected.
                // This is necessary because there is a collection trigger on each player limb and multiple
                // limbs can trigger an item in a single frame.
                if (!item.IsCollected) {

                    // Collect item and notify listeners.
                    item.Collect();
                    Debug.Log("Item Collected: " + item.Type);
                    MessageDispatcher.SendMessage(this, MessageDatabase.item_collected, item.Type, 0f);
                }
            }
		    else if (otherLayer == _waterLayer) {
                MessageDispatcher.SendMessage(this, MessageDatabase.water_touched, this, 0f);
		    } 
		    else if (otherLayer == _fireLayer) {
                MessageDispatcher.SendMessage(this, MessageDatabase.fire_touched, this, 0f);
		    }
	    }
    }
}
