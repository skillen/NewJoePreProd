﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCollision : MonoBehaviour {

    public delegate void ThiefEscapeEvent();
    public static event ThiefEscapeEvent ThiefEscaped;


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 16)
        {
            Debug.Log("THIEF ESCAPED");
            OnThiefEscape();
            
        }
        
    }

    protected virtual void OnThiefEscape()
    {
        if(ThiefEscaped != null)
        {
            ThiefEscaped();
        }
    }


    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
