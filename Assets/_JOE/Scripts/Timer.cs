using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{

    [Header("General")]
    public float delay = 10f;
    [SerializeField]
    float countdown;

    public bool active;

    [Header("References")]    
    bool Activated = false;

    public UnityEvent OnTriggerActivated;


    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {

            countdown -= Time.deltaTime;

            if (countdown <= 0f && !Activated)
            {
                OnTriggerActivated.Invoke();
                Activated = true;

                active = false;
            }

        }
    }

    public void StartTimer()
    {
        active = true;
    }

    public void test()
    {
        Debug.Log("timer triggered");
    }
}
