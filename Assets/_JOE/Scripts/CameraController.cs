﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Cinemachine;
using Coffee;

public class CameraController : MonoBehaviour
{
    [SerializeField] int PlayerID = 0;
    [SerializeField] private CoffeePlayerSettings settings = null;
    private Player _playerInput;
    [SerializeField] private CinemachineFreeLook freeLookCamera = null; // Third person orbit virtual camera.

    // Start is called before the first frame update
    void Start()
    {
        freeLookCamera = GetComponent<CinemachineFreeLook>();
        _playerInput = ReInput.players.GetPlayer(PlayerID);

    }

    // Update is called once per frame
    void Update()
    {
        // Update camera.
        HandleCameraInput();
    }

    private void HandleCameraInput()
    {

        // Create the look input vector for the camera
        float lookAxisUp = _playerInput.GetAxis("Camera Vertical");
        float lookAxisRight = _playerInput.GetAxis("Camera Horizontal");

        freeLookCamera.m_XAxis.m_InputAxisValue = -lookAxisRight * (1f - settings.cameraDamping);
        freeLookCamera.m_YAxis.m_InputAxisValue = lookAxisUp * ( 1f) * (1f - settings.cameraDamping);
    }
}
