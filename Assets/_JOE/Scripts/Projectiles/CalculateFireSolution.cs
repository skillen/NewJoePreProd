﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculateFireSolution : MonoBehaviour
{

    public AnimationCurve curve;


    public Rigidbody projectile;
    public Transform target;

    public float ArcApex = 25;
    public float gravity = Physics.gravity.y;
    

    public Vector3 CalculateLaunchVelocity()
    {
        float displacementY = target.position.y - projectile.position.y;
        Vector3 displacementXZ = new Vector3(target.position.x - projectile.position.x, 0, target.position.z - projectile.position.z);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * ArcApex);
        Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt(-2 * ArcApex / gravity) + Mathf.Sqrt(2 * (displacementY - ArcApex) / gravity));

        return velocityXZ + velocityY;
    }
    

}
