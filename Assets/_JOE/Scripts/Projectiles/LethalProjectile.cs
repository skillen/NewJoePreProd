using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RootMotion.Dynamics;
using UnityEngine.Events;

public class LethalProjectile : MonoBehaviour
{
    public Rigidbody rb;
    public float lethalForce = 50;

    public GameObject rootObject;


    [TagSelector]
    public string[] TagFilterArray = new string[] { };

    public UnityEvent OnProjectileFired;

    // Start is called before the first frame update
    void Start()
    {
        rb = rootObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ProjectileFired()
    {
        OnProjectileFired.Invoke();
    }

    public void test()
    {
        Debug.Log("timer triggered");
    }

    void OnCollisionEnter(Collision col)
    {
        if (TagFilterArray.Contains(col.gameObject.tag))
        {

            //Debug.Log("Collision Detected. Magnitude: " + col.relativeVelocity.magnitude);
            float collisionForce = col.relativeVelocity.magnitude * GetComponent<Rigidbody>().mass;
            Debug.Log("Collision Force is : " + collisionForce);

            if (collisionForce > lethalForce)
            {
                PuppetMaster enemyPuppet = col.gameObject.GetComponentInParent<PuppetMaster>();
                enemyPuppet.state = PuppetMaster.State.Dead;
            }

            

        }
        
    }





}
