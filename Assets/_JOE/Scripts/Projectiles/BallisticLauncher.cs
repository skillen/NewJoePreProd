﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class BallisticLauncher : MonoBehaviour
{

    [Header("General")]

    public bool CalculateByMaxHeight = false;

    [Header("launch parameters")]
    public float maxDistance;
    public float VerticalCursorOffset = 0.1f;

    [Header("used in airtime calculation")]
    public float ProjectileAirtime = 1f;

    [Header("used in max height calculation")]
    public float ArcApex = 10f;


    [Header("projectile settings")]

    public Rigidbody ProjectilePrefab;
    public GameObject TargetCursor;
    public LayerMask TargetLayers;
    public Transform ProjectileSpawner;






    [Header("arc visualization settings")]

    [SerializeField]
    private LineRenderer trajectoryLine;
    [SerializeField]
    private float maxCurveLength;
    private float trajectoryVertDis;

    [SerializeField]
    private float TrajectoryLineResolution = 150;






    private Vector3 launchTrajectory;

    private Vector3 projectileOrigin;
    private Vector3 targetCoordinates;

    private Camera cam;
    // Start is called before the first frame update

    [Header("Debug")]
    [SerializeField]
    private bool _debugAlwaysDrawTrajectory = false;
    [SerializeField]
    private bool debugPath;
    public float horozontalAimOffset;
    public float verticalAimOffset;

    private void Awake()
    {
        trajectoryLine = GetComponent<LineRenderer>();
    }

    void Start()
    {
        cam = Camera.main;
        trajectoryVertDis = maxCurveLength / TrajectoryLineResolution;
    }

    // Update is called once per frame
    void Update()
    {

        LaunchData data = AimProjectile();

        if (debugPath && data.LaunchVelocity != Vector3.zero && this.isActiveAndEnabled)
        {
            DrawPath(data);
        }
       

        // Draw trajectory while pressing button
        if (TargetCursor.activeSelf && (/*Input.GetButton("Fire2") ||*/ _debugAlwaysDrawTrajectory))
        {
            // Draw trajectory
            DrawTrajectory();
        }
        // Clear trajectory after releasing button
        if (TargetCursor.activeSelf && (/*Input.GetButtonUp("Fire2") &&*/ !_debugAlwaysDrawTrajectory))
        {
            // Clear trajectory
            ClearTrajectory();
        }
    }



    LaunchData AimProjectile()
    {
        //modify this to take into account some other input
        //Ray camRay = cam.ScreenPointToRay(Input.mousePosition);

        //change this to be the center of the viewport
        Ray camRay = cam.ScreenPointToRay(new Vector3(cam.scaledPixelWidth / 2 + horozontalAimOffset, cam.scaledPixelHeight / 2 + verticalAimOffset, 0));

        RaycastHit hit;

        LaunchData launchData;

        if (Physics.Raycast(camRay, out hit, maxDistance, TargetLayers))
        {
            

            //raycast has hitsomething in target layer
            TargetCursor.SetActive(true);

            projectileOrigin = ProjectileSpawner.position;
            targetCoordinates = hit.point;

            TargetCursor.transform.position = hit.point + Vector3.up * VerticalCursorOffset;


            if (!CalculateByMaxHeight)
            {
                launchData = CalculateTrajectoryByAirtime(targetCoordinates, projectileOrigin, ProjectileAirtime);
                
            }
            else
            {
                launchData = CalculateTrajectoryByMaxHeight(targetCoordinates, projectileOrigin, ArcApex);
            }

            launchTrajectory = launchData.LaunchVelocity;

            transform.rotation = Quaternion.LookRotation(launchTrajectory);

            //fix this later
            if (Input.GetMouseButtonDown(0))
            {
                Rigidbody projectile = Instantiate(ProjectilePrefab, ProjectileSpawner.position, Quaternion.identity);
                projectile.velocity = launchTrajectory;
            }
        }
        else
        {
            launchData = new LaunchData(Vector3.zero, 0);
            TargetCursor.SetActive(false);
        }

        return launchData;
    }

    public void LauchProjectile(Rigidbody projectile)
    {

        //Rigidbody projectile = Instantiate(ProjectilePrefab, ProjectileSpawner.position, Quaternion.identity);
        projectile.velocity = AimProjectile().LaunchVelocity;
    }


    //create alternative calculation for fixed groundplane coords and vert parameters (this one has fixed ground plane coords and time params)
    //t = given arc height and coords
    LaunchData CalculateTrajectoryByAirtime(Vector3 targetVector, Vector3 origin, float projectileHangtime)
    {
        Vector3 distance = targetVector - origin;
        Vector3 groundPlaneDistance = distance;
        groundPlaneDistance.y = 0f;

        float MaxVerticalHeight = distance.y;
        float HorizontalDistance = groundPlaneDistance.magnitude;

        float HorizontalVelocity = HorizontalDistance / projectileHangtime;
        float VerticalVelocity = MaxVerticalHeight / projectileHangtime + 0.5f * Mathf.Abs(Physics.gravity.y) * projectileHangtime;

        Vector3 result = groundPlaneDistance.normalized;
        result *= HorizontalVelocity;
        result.y = VerticalVelocity;

        //return result;

        return new LaunchData(result, projectileHangtime);

    }

    LaunchData CalculateTrajectoryByMaxHeight(Vector3 target, Vector3 origin, float ArcApex)
    {
        float gravity = Physics.gravity.y;

        float displacementY = target.y - origin.y;
        Vector3 displacementXZ = new Vector3(target.x - origin.x, 0, target.z - origin.z);
        


        bool ApexAboveTarget = ArcApex > target.y;

        if (!ApexAboveTarget)
        {
            ArcApex = target.y;
            Debug.Log("setting arc apex to target height");
        }

        float time = Mathf.Sqrt(-2 * ArcApex / gravity) + Mathf.Sqrt(2 * (displacementY - ArcApex) / gravity);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * ArcApex);
        Vector3 velocityXZ = displacementXZ / time;

        return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
    }

    struct LaunchData
    {
        public readonly Vector3 LaunchVelocity;
        public readonly float timeToTarget;

        public LaunchData(Vector3 launchVelocity, float timeToTarget)
        {
            LaunchVelocity = launchVelocity;
            this.timeToTarget = timeToTarget;
        }
    }

    void DrawPath(LaunchData launchData)
    {
        projectileOrigin = ProjectileSpawner.position;
        Vector3 previousDrawPoint = projectileOrigin;

        int resolution = Mathf.RoundToInt(TrajectoryLineResolution);

        for (int i = 1; i <= resolution; i++)
        {
            float simulationTime = i / (float)resolution * launchData.timeToTarget;
            Vector3 displacement = launchData.LaunchVelocity * simulationTime + Vector3.up * Physics.gravity.y * simulationTime * simulationTime / 2f;
            
            Vector3 drawPoint = projectileOrigin + displacement;
            Debug.DrawLine(previousDrawPoint, drawPoint, Color.green);
            previousDrawPoint = drawPoint;
        }
    }

    public void DrawTrajectory()
    {
        trajectoryVertDis = maxCurveLength / TrajectoryLineResolution;

        // Create a list of trajectory points
        var curvePoints = new List<Vector3>();
        curvePoints.Add(projectileOrigin);
        // Initial values for trajectory
        var currentPosition = projectileOrigin;
        var currentVelocity = launchTrajectory;
        // Init physics variables
        RaycastHit hit;
        Ray ray = new Ray(currentPosition, currentVelocity.normalized);
        // Loop until hit something or distance is too great
        while (!Physics.Raycast(ray, out hit, trajectoryVertDis) && Vector3.Distance(projectileOrigin, currentPosition) < maxCurveLength)
        {
            // Time to travel distance of trajectoryVertDist
            var t = trajectoryVertDis / currentVelocity.magnitude;
            // Update position and velocity
            currentVelocity = currentVelocity + t * Physics.gravity;
            currentPosition = currentPosition + t * currentVelocity;
            // Add point to the trajectory
            curvePoints.Add(currentPosition);
            // Create new ray
            ray = new Ray(currentPosition, currentVelocity.normalized);
        }
        // If something was hit, add last point there
        if (hit.transform)
        {
            curvePoints.Add(hit.point);
        }
        // Display line with all points
        trajectoryLine.positionCount = curvePoints.Count;
        trajectoryLine.SetPositions(curvePoints.ToArray());
    }

    private void ClearTrajectory()
    {
        // Hide line
        trajectoryLine.positionCount = 0;
    }


}
