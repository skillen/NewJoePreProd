using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PlaceableItemEvent : UnityEvent<PlaceableItem>
{
}

public class ProjectileLauncherTrap : MonoBehaviour
{
    public List<LethalProjectile> projectiles;

    public float forceToAdd = 0f;

    public List<ProjectileThrower> Throwers;

    public PlaceableItemEvent OnProjectileLaunched;

    // Start is called before the first frame update
    void Start()
    {
        ////for debug
        //LaunchProjectiles();
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void LaunchProjectiles()
    {
        foreach (LethalProjectile projectile in projectiles)
        {

            projectile.ProjectileFired();

            if (projectile.rootObject != null)
            {
                Rigidbody projRB = projectile.rootObject.GetComponent<Rigidbody>();
                
                if (projRB != null)
                {
                    projRB.isKinematic = false;
                    Vector3 newForce = forceToAdd * gameObject.transform.forward;
                    projRB.AddForce(newForce, ForceMode.Impulse);
                }

                //pass the projectile placeable to the event for removal
                PlaceableItem placeable = projectile.rootObject.GetComponent<PlaceableItem>();

                if (placeable != null)
                {
                    OnProjectileLaunched.Invoke(placeable);
                }
            }

        }
    }





    private void OnTriggerStay(Collider other)
    {
        LethalProjectile projectile = other.GetComponent<LethalProjectile>();
        if (projectile != null && !projectiles.Contains(projectile))
        {
            projectiles.Add(projectile);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        LethalProjectile projectile = other.GetComponent<LethalProjectile>();
        if (projectiles.Contains(projectile))
        {
            projectiles.Remove(projectile);
        }
    }

    public void AddThrower(GameObject placeableObj)
    {
        ProjectileThrower thrower = placeableObj.GetComponent<ProjectileThrower>();

        if (thrower != null)
        {
            AddThrower(thrower);
        }
    }

    public void AddThrower(ProjectileThrower thrower)
    {
        if (thrower != null)
        {
            Throwers.Add(thrower);
            RecalculateLaunchForce();
        }
    }

    public void RemoveThrower(GameObject placeableObj)
    {
        ProjectileThrower thrower = placeableObj.GetComponent<ProjectileThrower>();

        if (thrower != null)
        {
            RemoveThrower(thrower);
        }
    }

    public void RemoveThrower(ProjectileThrower thrower)
    {
        if (thrower != null && Throwers.Contains(thrower))
        {
            Throwers.Remove(thrower);
            RecalculateLaunchForce();
        }
    }

    public void RecalculateLaunchForce()
    {
        float newForce = Throwers.Select(x => x.TotalThrust).Sum();
        forceToAdd = newForce;
    }
}
