﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coffee;

public class SmokeGrenade : MonoBehaviour
{
    [Header("General")]
    public float delay = 3f;
    public float blastRadius = 13f;
    public float ActiveTime = 3f;
    public float EffectTime = 3f;
    public float reducedVisionRadius = .5f;

    public bool active;


    [Header("References")]
    public GameObject SmokeBall;


    float countdown;
    bool hasExploded = false;


    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;

    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {

            countdown -= Time.deltaTime;
            if (countdown <= 0f && !hasExploded)
            {
                Explode();
                hasExploded = true;
            }

        }

    }

    private void Explode()
    {
        GameObject smokeBall = Instantiate(SmokeBall, transform.position, transform.rotation);

        SmokeEffect smoke = smokeBall.GetComponent<SmokeEffect>();
        smoke.ActiveTime = ActiveTime;
        smoke.blastRadius = blastRadius;

        Collider[] colliders = Physics.OverlapSphere(transform.position, blastRadius);

        foreach (Collider nearbyObject in colliders)
        {

            if (nearbyObject.gameObject.layer == LayerMask.NameToLayer("Zombie"))
            {

                //spherecast, check for enemy layers/tags
                //then cause them to move towards it


                enemyLOS enemyVision = nearbyObject.GetComponent<enemyLOS>();
                if (enemyVision != null)
                {
                    enemyVision.BlindForSeconds(EffectTime, reducedVisionRadius);
                }
            }

        }


    }

}
