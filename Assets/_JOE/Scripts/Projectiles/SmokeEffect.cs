﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeEffect : MonoBehaviour
{
    [Header("General")]
    public float blastRadius = 5f;
    public float ActiveTime = 3f;

    [Header("References")]
    public GameObject explosionEffect;

    public CapsuleCollider smokeCollider;

    bool smokeActive = false;
    float SmokeActiveCountdown;

    GameObject smokeEffect;


    // Start is called before the first frame update
    void Start()
    {
        SmokeActiveCountdown = ActiveTime;

        smokeCollider.radius = blastRadius;

        StartSmoke();

    }

    // Update is called once per frame
    void Update()
    {

        if (smokeActive)
        {
            SmokeActiveCountdown -= Time.deltaTime;

            if (SmokeActiveCountdown <= 0f && smokeActive)
            {
                EndSmoke();
            }
        }
    }

    private void StartSmoke()
    {
        smokeEffect = Instantiate(explosionEffect, transform.position, transform.rotation);
        //smokeEffect.transform.parent = gameObject.transform;

        smokeCollider.enabled = true;

        smokeActive = true;

       

    }

    private void EndSmoke()
    {
        smokeCollider.enabled = false;
        smokeActive = false;

        Destroy(smokeEffect);

        Destroy(gameObject);
    }
}

