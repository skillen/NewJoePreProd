﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Coffee;

public class ThrownBean : MonoBehaviour
{
    [Header("General")]
    public float delay = 3f;
    public float blastRadius = 5f;
    public float lureTime = 3f;

    public bool active;

    [Header("References")]
    public GameObject explosionEffect;

    float countdown;
    bool hasExploded = false;


    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {

            countdown -= Time.deltaTime;
            if (countdown <= 0f && !hasExploded)
            {
                Explode();
                hasExploded = true;
            }

        }
        
    }

    private void Explode()
    {
        //Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, blastRadius);

        foreach (Collider nearbyObject in colliders)
        {
            if (nearbyObject.gameObject.layer == LayerMask.NameToLayer("Zombie"))
            {

                //spherecast, check for enemy layers/tags
                //then cause them to move towards it


                DecaffeinatedFollow agent = nearbyObject.GetComponentInChildren<DecaffeinatedFollow>();
                if (agent != null)
                {
                    agent.GoToDestinationForSetTime(this.transform, lureTime);
                }
            }


        }

        //Destroy(gameObject);
    }
}
