﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.Dynamics;
using System.Linq;

public class Grenade : MonoBehaviour
{
    [Header("General")]
    public float delay = 10f;
    public float blastRadius = 5f;
    public float explosionForce = 700f;

    public bool active;

    [Header("References")]
    public GameObject explosionEffect;

    float countdown;
    bool hasExploded = false;


    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {

            countdown -= Time.deltaTime;
            if (countdown <= 0f && !hasExploded)
            {
                Explode();
                hasExploded = true;
            }

        }
        
    }

    public void Explode()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, blastRadius);

        foreach (Collider nearbyObject in colliders)
        {
            if (nearbyObject.gameObject.layer == LayerMask.NameToLayer("Ragdoll"))
            {
                MuscleCollisionBroadcaster muscleCollisionBroadcaster = nearbyObject.GetComponent<MuscleCollisionBroadcaster>();

                var test = muscleCollisionBroadcaster.puppetMaster.behaviours; //GetValue //("  behaviourPuppet.SetState(BehaviourPuppet.State.Unpinned)

                BehaviourPuppet behav;
                for (int i = 0; i < test.Length; i++)
                {
                    if (test[i] is BehaviourPuppet)
                    {
                        behav = (BehaviourPuppet) test[i];
                        behav.SetState(BehaviourPuppet.State.Unpinned);
                        break;
                    }
                }

                

                Rigidbody rb = nearbyObject.GetComponentInChildren<Rigidbody>();
                if (rb != null)
                {

                    rb.AddExplosionForce(explosionForce, transform.position, blastRadius);
                }
            }

            else
            {
                Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.AddExplosionForce(explosionForce, transform.position, blastRadius);
                }
            }
            
        }

        Destroy(gameObject);
    }
}
