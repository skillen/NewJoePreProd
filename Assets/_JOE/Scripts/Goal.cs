﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using com.ootii.Messages;

namespace Coffee {

    public class Goal : MonoBehaviour {

        [SerializeField] private LayerMask checkLayers;
        [SerializeField] private string checkTag = "Player Puppet";

        private void OnTriggerEnter (Collider other) {
         
            if (LayermaskContains(checkLayers, other.gameObject.layer) && other.tag == checkTag) {
                MessageDispatcher.SendMessage(MessageDatabase.level_completed);
            }
        }

        private bool LayermaskContains (LayerMask layerMask, int layer) {
            return layerMask == (layerMask | (1 << layer));
        }
    }
}





