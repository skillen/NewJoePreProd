﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using RootMotion.Dynamics;  // PuppetMaster

public class AutoCrouch : MonoBehaviour
{
	public LayerMask groundLayers;
	[SerializeField] bool m_Crouching;
	[SerializeField] Rigidbody m_Rigidbody;
	[SerializeField] CapsuleCollider m_Capsule;
	bool m_IsGrounded = true;
    float m_CapsuleHeight;
    Vector3 m_CapsuleCenter;
    const float k_Half = 0.5f;

    private Player _playerInput;


    // Start is called before the first frame update
    void Start()
    {
        m_Capsule = GetComponent<CapsuleCollider>();
        m_CapsuleHeight = m_Capsule.height;
        m_CapsuleCenter = m_Capsule.center;
        m_Rigidbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
		ScaleCapsuleForCrouching();
		PreventStandingInLowHeadroom();
	}

	void ScaleCapsuleForCrouching()
	{
		Vector3 origin = m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half;
		float radius = m_Capsule.radius * k_Half;
		RaycastHit hit;

		if (m_IsGrounded && m_Crouching)
		{
			if (m_Crouching) return;
			m_Capsule.height = m_Capsule.height / 2f;
			m_Capsule.center = m_Capsule.center / 2f;
			m_Crouching = true;
		}
		else
		{
			Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
			float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
			if (Physics.SphereCast(origin, radius, Vector3.up, out hit, crouchRayLength, groundLayers, QueryTriggerInteraction.Ignore))
			{
				sphereCastHitDistance = hit.distance;
				m_Crouching = true;
				return;
			}
			m_Capsule.height = m_CapsuleHeight;
			m_Capsule.center = m_CapsuleCenter;
			m_Crouching = false;
		}
	}

	void PreventStandingInLowHeadroom()
	{
		// prevent standing up in crouch-only zones
		if (!m_Crouching)
		{
			Vector3 origin = m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half;
			float radius = m_Capsule.radius * k_Half;
			float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;

			RaycastHit hit;

			Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);

			if (Physics.SphereCast(origin, radius, Vector3.up, out hit, crouchRayLength, groundLayers, QueryTriggerInteraction.Ignore))
			{
				m_Crouching = true;
				sphereCastHitDistance = hit.distance;
			}
			else
			{
				sphereCastHitDistance = crouchRayLength;
			}


		}
	}


	private float sphereCastHitDistance;

	private void OnDrawGizmos()
	{
		Vector3 rayOrigin = m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half;
		Vector3 sphereOrigin = rayOrigin + (Vector3.up * sphereCastHitDistance);
		float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;

		Gizmos.color = Color.yellow;
		//Debug.DrawLine(origin, origin + direction * distance);
		Debug.DrawRay(rayOrigin, Vector3.up, Color.red, 1.0f, false);
		//sphere origin is origin + (direction * currentHitDistance)
		Gizmos.DrawWireSphere(sphereOrigin, m_Capsule.radius * k_Half);
	}



}
