﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using RootMotion.Dynamics;

public class ikPuppetmasterIntegration : MonoBehaviour
{

    [SerializeField]
    private FullBodyBipedIK ik;
    [SerializeField]
    private AimIK aimIk;
    [SerializeField]
    private PuppetMaster puppetMaster;

    public bool aimActive = false;


    void Start()
    {
        aimIk = GetComponent<AimIK>();
        if (aimIk != null)
        {
            aimIk.Disable();
        }
        
        ik.Disable();
        puppetMaster.OnRead += OnPuppetMasterRead;
    }

    private void OnPuppetMasterRead()
    {
        
        if (aimIk != null && aimActive)
        {
            aimIk.solver.Update();
        }

        ik.solver.Update();

    }
}
