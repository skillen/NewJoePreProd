﻿using UnityEngine;
using System.Collections;
using Rewired;

namespace RootMotion.Demos
{

    /// <summary>
    /// User input for a third person character controller.
    /// </summary>
    public class UserControls : MonoBehaviour
    {

        // Input state
        [System.Serializable]
        public struct State
        {
            public Vector3 move;
            public Vector3 lookPos;
            public bool crouch;
            public bool jump;
            public bool interact;
            public bool pickUp;
            public bool shove;
            public bool sip;
            public int actionIndex;

            public bool aim;
            public bool launch;

            public bool menuUpInitialPress;

            public bool menuUp;
            public bool menuDown;
            public bool menuRight;
            public bool menuLeft;
        }

        [SerializeField] int PlayerID = 0;
        private Player _playerInput;

        public bool walkByDefault;        // toggle for walking state
        public bool canCrouch = true;
        public bool canJump = true;


        public State state = new State();           // The current state of the user input

        protected Transform cam;                    // A reference to the main camera in the scenes transform

        protected virtual void Start()
        {
            // get the transform of the main camera
            cam = Camera.main.transform;

            _playerInput = ReInput.players.GetPlayer(PlayerID);
        }

        protected virtual void Update()
        {
            // read inputs

            //float move = _playerInput.GetAxis("Forward");
            //float turn = _playerInput.GetAxis("Turn");
            //bool walk = _playerInput.GetButton("Walk");

            //bool jump = _playerInput.GetButton("Jump");


            //bool walk = _playerInput.GetButton("Walk");

            state.jump = canJump && _playerInput.GetButton("Jump");
            state.crouch = canCrouch && _playerInput.GetButton("Crouch");
            state.interact = canCrouch && _playerInput.GetButton("Interact");
            state.pickUp = canCrouch && _playerInput.GetButton("Pick Up");
            state.shove = canCrouch && _playerInput.GetButton("Shove");

            state.sip = _playerInput.GetButton("Sip");

            state.aim = _playerInput.GetButton("Aim");
            state.launch = _playerInput.GetButton("Launch");


            state.menuUpInitialPress = _playerInput.GetButtonDown("Menu Up");

            state.menuUp = _playerInput.GetButton("Menu Up");
            state.menuDown = _playerInput.GetButton("Menu Down");
            state.menuLeft = _playerInput.GetButton("Menu Left");
            state.menuRight = _playerInput.GetButton("Menu Right");


            float h = _playerInput.GetAxis("Turn");
            float v = _playerInput.GetAxis("Forward");

            // calculate move direction
            Vector3 move = cam.rotation * new Vector3(h, 0f, v).normalized;

            // Flatten move vector to the character.up plane
            if (move != Vector3.zero)
            {
                Vector3 normal = transform.up;
                Vector3.OrthoNormalize(ref normal, ref move);
                state.move = move;
            }
            else state.move = Vector3.zero;

            bool walkToggle = _playerInput.GetButton("Walk");

            // We select appropriate speed based on whether we're walking by default, and whether the walk/run toggle button is pressed:
            float walkMultiplier = (walkByDefault ? walkToggle ? 1 : 0.5f : walkToggle ? 0.5f : 1);

            state.move *= walkMultiplier;

            // calculate the head look target position
            state.lookPos = transform.position + cam.forward * 100f;
        }
    }
}

