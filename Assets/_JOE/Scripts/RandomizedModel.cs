﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Coffee {

    /// <summary>
    /// Takes a list of GameObjects and randomly chooses one to keep enabled.
    /// Useful for things like advertisements where each ad is a separate child mesh.
    /// </summary>
    public class RandomizedModel : MonoBehaviour {

        [Tooltip("Set this to force a specific model to be used.")]
		[SerializeField] private GameObject overrideModel = null;

		[Tooltip("The available models. Each is its own separate mesh. A model will be chosen at random unless override model is set.")]
		[SerializeField] private List<GameObject> models;

		private void Awake () {
			
			if (overrideModel != null) {

				// Force a specific model.
				for (int i = 0; i < models.Count; i++) {
					models[i].SetActive(false);
				}
                overrideModel.SetActive(true);
			}
			else {

				// Disable all model meshes except for a randomly chosen model.
				int random = Random.Range(0, models.Count);
				for (int i = 0; i < models.Count; i++) {
					models[i].SetActive(i == random);
				}
			}
		}    
    }
}