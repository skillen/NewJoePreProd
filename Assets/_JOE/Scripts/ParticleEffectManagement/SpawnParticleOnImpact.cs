using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpawnParticleOnImpact : MonoBehaviour
{

    [TagSelector]
    public string[] TagFilterArray = new string[] { };


    public GameObject EffectOnCollision;

    public GameObject ContinuousEffect;
    [SerializeField]
    private GameObject continuousEffectInstance;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {


        if (EffectOnCollision != null && TagFilterArray.Contains(collision.gameObject.tag))
        {

            SpawnEffect(collision, EffectOnCollision);


        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (ContinuousEffect != null &&  TagFilterArray.Contains(collision.gameObject.tag) )
        {
            ContactPoint contact = collision.GetContact(0);


            //spawn effect if it's not active
            if (continuousEffectInstance == null)
            {



                continuousEffectInstance = GameObject.Instantiate(ContinuousEffect, contact.point, Quaternion.LookRotation(contact.normal));
                //parent to this 
                continuousEffectInstance.transform.SetParent(collision.gameObject.transform);
            }

            else
            {
                continuousEffectInstance.GetComponent<ParticleSystem>().Play();
                continuousEffectInstance.transform.position = contact.point;
                continuousEffectInstance.transform.rotation = Quaternion.LookRotation(contact.normal);
            }
            


        }
    }


    void SpawnEffect(Collision collision, GameObject prefab)
    {
        ContactPoint contact = collision.GetContact(0);
        GameObject spawnedDecal = GameObject.Instantiate(prefab, contact.point, Quaternion.LookRotation(contact.normal));
        spawnedDecal.transform.SetParent(collision.gameObject.transform);
    }
}
