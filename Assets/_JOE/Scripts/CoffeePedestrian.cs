﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.AI;

using com.ootii.Messages;

namespace Coffee {

	public enum AnimationType {

        Idle_Male_1 = 0,
		Idle_Male_2 = 1,
		Idle_Female_1 = 2,
		Idle_Female_2 = 3,
		Idle_Talking_1 = 4,
		Idle_Talking_2 = 5,

        Sitting_1 = 11,
        Sitting_2 = 12,

        Dancing_1 = 21,
		Dancing_2 = 22,
	}

	[RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(NavMeshAgent))]
	public class CoffeePedestrian : MonoBehaviour {

		[Tooltip("What animation this character should be using.")]
		[SerializeField] private AnimationType animType;
        public AnimationType AnimType { get { return animType; } }


        [SerializeField] private bool isMascot = false;

		private Animator _anim;
        private NavMeshAgent _agent;

		private List<Rigidbody> _rigidbodies = new List<Rigidbody>();
        private NavMeshObstacle _navMeshObstacle;

		// When a collision happens the character will be knocked out if the magnitude of the relative velocity is greater than this value.
		private const float c_knockout_threshold = 4f;

		private void Awake () {

			_anim = GetComponent<Animator>();
            _agent = GetComponent<NavMeshAgent>();
            _navMeshObstacle = GetComponent<NavMeshObstacle>();
		}

		private void Start () {

			// Change to animation state set via inspector.
			_anim.SetInteger("state", (int)(animType));

			// Add a random speed variance so that NPCs running the same animation don't match 100% (things seem less robotic this way).
			_anim.SetFloat("StateSpeedMultiplier", Random.Range(0.9f, 1.1f));
		}

		public void OnKnockout () {

            if (isMascot) {
                MessageDispatcher.SendMessage(MessageDatabase.mascot_knockdown);
            }

            //TODO: This should be somewhere else. 
            if (_navMeshObstacle != null) {

                // If pedestrian has a NavMeshObstacle component it means that it's a non-moving NPC and cannot get up.
                // When this NPC is knocked down, the puppet is frozen and shouldn't block nav mesh calculations anymore.
                _navMeshObstacle.enabled = false;
            }
        }
	}
}