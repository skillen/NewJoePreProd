﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using RootMotion.Dynamics;

public class StressTestNPC : MonoBehaviour {

    public Rigidbody collisionSphere;
    public float sphereMoveSpeed = 1f;
    [Space]
    public KeyCode keyForward = KeyCode.I;
    public KeyCode keyBackward = KeyCode.K;
    public KeyCode keyLeft = KeyCode.J;
    public KeyCode keyRight = KeyCode.L;
    [Space]
    public List<GameObject> prefabs;
    public int gridWidth = 10;
    public int gridHeight = 10;
    [Range(0f, 5f)] public float spacing = 2f;
    
    private List<PuppetMaster> _puppets = new List<PuppetMaster>();
    private int _row = 0;
    private int _col = 0;

    private void Awake () {

        int count = gridWidth * gridHeight;
        for (int i = 0; i < count; i++) {

            // Spawn random NPC prefab.
            SpawnNPC(i, prefabs[Random.Range(0, prefabs.Count)]);
        }
    }

    private void FixedUpdate () {

        Vector3 flatCameraForward = Camera.main.transform.forward;  { flatCameraForward.y = 0f; }
        Vector3 flatCameraRight = Camera.main.transform.right;      { flatCameraRight.y = 0f; }

        Vector3 moveDelta = Vector3.zero;

        if (Input.GetKey(keyForward)) {
            moveDelta.z += (sphereMoveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(keyBackward)) {
            moveDelta.z -= (sphereMoveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(keyRight)) {
            moveDelta.x += (sphereMoveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(keyLeft)) {
            moveDelta.x -= (sphereMoveSpeed * Time.deltaTime);
        }

        collisionSphere.MovePosition(collisionSphere.position + (moveDelta.z * flatCameraForward) + (moveDelta.x * flatCameraRight));
    }

    private void SpawnNPC (int index, GameObject prefab) {

        // Spawn the object.
        GameObject obj = Instantiate(prefab) as GameObject;
        obj.transform.position = transform.position +
            (transform.right * _col * spacing) +
            (transform.forward * _row * spacing);

        // Get the puppet.
        PuppetMaster pm = obj.GetComponentInChildren<PuppetMaster>();

        // Store the puppet.
        _puppets.Add(pm);
        
        // Is this the last NPC in current row?
        if (_col + 1 == gridWidth) {
            _row++;
            _col = 0;
        }
        else { _col++; }
    }
}
