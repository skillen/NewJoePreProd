﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using RootMotion.Dynamics;

//TODO: REFACTOR
//      - Flag for "canGetUp" means that puppet is a "decaf" pedestrian.
//      - Clean this up.

public class OptimizedPuppet : MonoBehaviour {

    public PuppetMaster puppetMaster;
    public string activationTriggerTag = "Puppet Activation";

    [Tooltip("This is how long to wait after puppet is knocked out to freeze the PuppetMaster scripts and let the built-in ragdoll take over.")]
    [Range(0f, 10f)] public float freezeDelay = 0f;

    [Space]

    public bool destroyAfterDeath = false;
    [Range(0f, 30f)] public float destroyAfterDeathDelay = 5f;

    private List<GameObject> _activatorsInRange = new List<GameObject>();
    private bool _canGetUp = false;
    private bool _dead = false;

    private void Awake () {

        // Start off disabled.
        puppetMaster.mode = PuppetMaster.Mode.Disabled;
    }

    public void SetCanGetUp (bool canGetUp) {
        _canGetUp = canGetUp;
    }

    private void OnTriggerEnter (Collider other) {

        if (_dead) return;

        if (other.tag == activationTriggerTag) {

            if (!_activatorsInRange.Contains(other.gameObject)) {

                // Save the triggering object so we know when it does trigger exit.
                _activatorsInRange.Add(other.gameObject);

                // Enable PuppetMaster.
                puppetMaster.mode = PuppetMaster.Mode.Active;
            }
        }
    }

    private void OnTriggerExit (Collider other) {

        if (_dead) return;

        if (other.tag == activationTriggerTag) {

            if (_activatorsInRange.Contains(other.gameObject)) {
                _activatorsInRange.Remove(other.gameObject);
            }

            bool validActivatorLeft = false;
            for (int i = 0; i < _activatorsInRange.Count; i++) {

                if (_activatorsInRange[i] != null) {
                    validActivatorLeft = true;
                    break;
                }
            }

            // Nothing with activation tag is currently within trigger collider.
            if (!validActivatorLeft) {

                // Clear out any references that might be null now (destroyed puppets, etc).
                _activatorsInRange.Clear();

                // Disable PuppetMaster.
                puppetMaster.mode = PuppetMaster.Mode.Disabled;
            }
        }
    }

    public void Die () {

        if (!_dead && !_canGetUp) {

            //Debug.Log("Dying..");
            StartCoroutine(DoDie());   
        }
    }

    private IEnumerator DoDie () {

        _dead = true;
        puppetMaster.state = PuppetMaster.State.Dead;

        // Wait to freeze puppet?
        yield return new WaitForSeconds(freezeDelay);

        // PuppetMaster "State Settings" should be set up to "Freeze Permanently" when state is set to Frozen. Components and colliders are removed.
        puppetMaster.state = PuppetMaster.State.Frozen;

        // Clean up?
        if (destroyAfterDeath) {
            yield return new WaitForSeconds(destroyAfterDeathDelay);
            Destroy(puppetMaster.transform.parent.gameObject);
        }
    }
}
