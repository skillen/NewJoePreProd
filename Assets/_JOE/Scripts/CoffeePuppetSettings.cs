﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using RootMotion.Dynamics;

namespace Coffee {

    /// <summary>
    /// This component is for easily setting puppet settings for many puppets at once since the puppet components are on child objects and don't support multiple component editing.
    /// </summary>
    public class CoffeePuppetSettings : MonoBehaviour {

        [SerializeField] private BehaviourPuppet puppetBehaviour;
        [SerializeField] private OptimizedPuppet optimizedPuppet;

        [Tooltip("How much resistance puppet has to things applying force to it (rigidity)")]
        [Range(1f, 1500f)] [SerializeField] private float collisionResistance = 500f;

        [Tooltip("If false the puppet will stay down when knocked out and freeze the puppet (remove joints, disable puppet behaviours, etc)")]
        [SerializeField] private bool canGetUp = false;

        [Tooltip("If puppet can get up, this is how long it takes before puppet starts getting up after being knocked out.")]
        [Range(0f, 10f)] [SerializeField] private float getUpDelay = 0f;

        [Tooltip("This is how far a limb of the puppet has to move away from its target position to trigger a knock out event.")]
        [Range(0.1f, 5f)] [SerializeField] private float knockOutDistance = 1.5f;

        private void Awake () {

            // Apply settings.
            puppetBehaviour.collisionResistance = new Weight(collisionResistance);
            puppetBehaviour.canGetUp = canGetUp;
            puppetBehaviour.getUpDelay = getUpDelay;
            puppetBehaviour.knockOutDistance = knockOutDistance;
        }

        private void Start () {
            
            if (canGetUp) {
                optimizedPuppet.SetCanGetUp(true);
            }
        }
    }
}






