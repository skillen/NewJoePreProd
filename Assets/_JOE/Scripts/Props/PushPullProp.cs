﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class PushPullProp : MonoBehaviour {


    public Collider meshCollider;
    public Collider moveCollider;


    private InteractionObject interactionObject;
    private InteractionSystem lastIS;
    private FullBodyBipedIK rigIK;
    private List<InteractionTarget> targets;
    
    
    private Joint joint;
    private Rigidbody rb;


    private bool propGrabbed = false;





    private void Awake() {
        interactionObject = GetComponentInChildren<InteractionObject>();
        targets = GetComponentsInChildren<InteractionTarget>().ToList();

        joint = GetComponent<Joint>();
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;

        meshCollider.enabled = true;
        moveCollider.enabled = false;
    }


    public void OnGrab() {

        if (!propGrabbed) {
            Debug.Log("Object grabbed" + "\n\n");
            propGrabbed = true;

            lastIS = interactionObject.lastUsedInteractionSystem;
            rigIK = lastIS.GetComponentInChildren<FullBodyBipedIK>();

            AttachToCharacter(true, lastIS);
        }
    }

    public void OnDrop() {
        if (propGrabbed) {
            Debug.Log("Object dropped" + "\n\n");
            propGrabbed = false;

            meshCollider.enabled = true;
            moveCollider.enabled = false;

            AttachToCharacter(false, lastIS);
        }
    }

    public void SwitchColliders() {
        if (!propGrabbed) {
            Debug.Log("Switch colliders" + "\n\n");
            meshCollider.enabled = false;
            moveCollider.enabled = true;
        }
    }

    private void AttachToCharacter(bool attach, InteractionSystem connectionTarget) {
        if (attach) {
            joint.connectedBody = connectionTarget.GetComponent<Rigidbody>();
            rb.isKinematic = false;
        }
        else {
            joint.connectedBody = null;
            rb.isKinematic = true;
        }
    }

}
