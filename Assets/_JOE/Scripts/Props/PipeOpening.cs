﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using System;

public class PipeOpening : MonoBehaviour
{

    [System.Serializable]
    public struct Opening
    {
        public GameObject Location;
        public PipeOpeningState OpeningState;
        public PipeOpening Connection;
        //public PipeConnections connections;


    }

    public Opening openingData;

    //public UnityEvent OpeningStateChanged;

    public PipeOpeningState open;
    public PipeOpeningState closed;
    //public PipeOpeningState Connected;

    [SerializeField]
    public GameObject pourEffect;

    [SerializeField]
    public PipeConnections pipeSegment; // { get; private set; }



    public bool PipeIsOpen
    {
        get
        {
            return openingData.OpeningState == open ? true : false;
        }
    }

    public bool PipeIsConnected
    {
        get
        {
            return openingData.Connection != null ? true : false;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        openingData.Location = this.gameObject;
        if (openingData.OpeningState == null)
        {
            openingData.OpeningState = open;
        }

        //if (OpeningStateChanged == null)
        //    OpeningStateChanged = new UnityEvent();

        //OpeningStateChanged.AddListener(OnOpeningStateChange);


        pipeSegment = GetComponentInParent<PipeConnections>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToggleOpening()
    {
        openingData.OpeningState = openingData.OpeningState == open ? closed : open;
    }


    public void UpdateOpeningState(PipeOpeningState newState)
    {
        if (openingData.OpeningState != newState)
        {
            openingData.OpeningState = newState;
            //OpeningStateChanged.Invoke();
            OnOpeningStateChange();
        }

    }



    private void OnOpeningStateChange()
    {
        pipeSegment.redirectFlow();
    }





    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PipeOpening"))
        {
            PipeOpening otherPipeOpening = other.GetComponent<PipeOpening>();

            PipeConnections otherPipeConnections = otherPipeOpening.pipeSegment;



            //Debug.Log(" connecting pipe opening " + other.gameObject.name);


            //actually connects this pipe opening to the other pipe opening
            openingData.Connection = otherPipeOpening;


            //need to set the flag for pour effects to update
            pipeSegment.CurrentVolFlow.ConnectionsChanged = true;



            bool otherPipeHasFlow = otherPipeOpening.pipeSegment.CurrentVolFlow.IsFlowPresent();
            //if other is a source to this pipe, 
            bool ThisPipeIsSourceToOther = otherPipeOpening.pipeSegment.FlowInputs.Any(inputOpening => inputOpening.openingData.Connection.pipeSegment == pipeSegment);


            //other pipe has no flow, or this pipe is a source to it
            //don't add other pipe as input to this pipe
            if (!otherPipeHasFlow || ThisPipeIsSourceToOther)
            {
                //Debug.Log(thisPipeSegment.name + " outputs to " + otherPipeOpening.name + " and will not add an output as an input source");
                //still need to call this pipe segments reroute flow
                //pipeSegment.redirectFlow();
            }
            //add other pipe as source to this one
            else
            {
                //Debug.Log(thisPipeSegment.name + " doesn't output to " + otherPipeOpening.name + " it is safe to add it as an input source");
                //if (otherPipeOpening.pourEffect != null)
                //{
                //    otherPourEffect = other.GetComponentInParent<PipeConnections>().PourEffect;

                //    //this was moved to the end of pipeconnections.propagateflow()
                //    //otherPipe.PropagatePourEffect(otherPourEffect);

                //}

                if (otherPipeConnections.CurrentVolFlow.IsFlowPresent())
                {
                    pipeSegment.AddFlow(this);

                }
            }




        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PipeOpening"))
        {
            PipeOpening otherPipe = other.GetComponent<PipeOpening>();

            //Debug.Log("disconnecting pipe opening " + other.gameObject.name);
            openingData.Connection = null;

            //PipeConnections thisPipe = GetComponentInParent<PipeConnections>();

            pipeSegment.RemoveFlow(this);



        }
    }

    //should probably call this more sparingly tbh
    public bool PropagatePourEffect(VolumetricFlow volFlow)
    {
        bool pour = false;

        

        
        //if there is flow present, pipe is sealed, or the pipe is connected
        //destroy the pour effect
        if (!volFlow.IsFlowPresent() || !PipeIsOpen || PipeIsConnected)
        {
            Destroy(pourEffect);
        }

        //if (volFlow.IsFlowPresent() && PipeIsOpen && !PipeIsConnected)
        //otherwise we need to spawn the new pour effect
        else
        {

            GameObject newPourEffect = volFlow.currentLiquid.PourEffect;

            

            //first check if it's actually different
            if (newPourEffect != null && newPourEffect != pourEffect)
            {
                //if it is different get rid of the old effect
                //remove old pour effect
                if (pourEffect != null)
                {
                    Destroy(pourEffect);
                }

                //Debug.Log("pouring from " + openingState.Location.name);


                //spawn the water effect at the opening origin aligned with the transform.position
                pourEffect = Instantiate(newPourEffect, openingData.Location.transform.position, openingData.Location.transform.rotation);
                pourEffect.transform.parent = gameObject.transform;

                pour = true;

            }


        }

        return pour;
    }
}
