﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;



[RequireComponent(typeof(VolumetricFlow))]
public class PipeConnections : MonoBehaviour
{
    [Header("General Parameters")]

    public PipeOpeningState open;
    public PipeOpeningState closed;

    //public LiquidData liquidData;

    [Header("references")]

    public VolumetricFlow CurrentVolFlow;

    public List<PipeOpening> pipeOpenings;

    //debug stuff
    //public GameObject PourEffect;
    public PipeOpening FlowInput;

    [SerializeField]
    private List<PipeOpening> flowInputs;
    public List<PipeOpening> FlowInputs { get { return flowInputs; } }

    [SerializeField]
    private List<PipeOpening> flowOutputs;
    public List<PipeOpening> FlowOutputs { get { return flowOutputs; } }

    // Start is called before the first frame update
    void Start()
    {
        // Disable mesh renderers that help for placement
        DisableOpeningMeshes();

        CurrentVolFlow = GetComponent<VolumetricFlow>();
        if (CurrentVolFlow == null)
        {
            CurrentVolFlow = gameObject.AddComponent(typeof(VolumetricFlow)) as VolumetricFlow;
        }

       


    }

    // Update is called once per frame
    void Update()
    {



    }

   

    private void LateUpdate()
    {
    }

    private void DisableOpeningMeshes()
    {
        foreach (PipeOpening pipeOpening in pipeOpenings)
        {
            MeshRenderer mesh = pipeOpening.GetComponent<MeshRenderer>();
            if (mesh != null)
            {
                mesh.enabled = false;
            }
        }
    }



    //actually adds the flow effect to the pipe,
    //if the input opening isn't in the input list, add it.
    //then call redirect flow to propagate the flow
    public void AddFlow(PipeOpening inputOpening)
    {


        //is the opening in our list of this pipe's openings
        bool contains = pipeOpenings.Any(opening => opening == inputOpening);

        if (contains)
        {
            //Debug.Log("opening in list");

            //if flow inputs doesn't contain this input
            if (!flowInputs.Any(inopening => inopening == inputOpening))
            {
                //add it to the list
                flowInputs.Add(inputOpening);
                //redirectFlow();

                CurrentVolFlow.ConnectionsChanged = true;

            }


            


        }
    }



    public void RemoveFlow(PipeOpening inputOpening)
    {

        bool contains = pipeOpenings.Any(opening => opening == inputOpening);

        if (contains)
        {
            //Debug.Log("removing from list");
            Debug.Log(gameObject + " might be in an infinite Loop");

            flowInputs.Remove(inputOpening);

            CurrentVolFlow.ConnectionsChanged = true;


            //redirectFlow();
        }

    }

    

    //propagates the flow state to each of the pipe openings
    //returns true if inputs/outputs changed
    public bool redirectFlow()
    {

        bool connectionsChanged = false;

        String objname = gameObject.name;

        //need a copy of the previous outputs to check for change
        List<PipeOpening> prevOutputs = new List<PipeOpening>(flowOutputs);
        flowOutputs.Clear();

        bool currentFlowState = CurrentVolFlow.IsFlowPresent();


        //there is a flow present
        if (currentFlowState)
        {
            //set pipe openings that aren't flow inputs as outputs
            flowOutputs = pipeOpenings.Where(opening => opening.enabled && opening.PipeIsOpen && !flowInputs.Contains(opening)).ToList();

            //check if the outputs have changed
            bool outputsEqual = prevOutputs.SequenceEqual(flowOutputs);
            if(!outputsEqual) //(!prevOutputs.OrderBy(m => m).SequenceEqual(flowOutputs.OrderBy(m => m)))
            {
                CurrentVolFlow.ConnectionsChanged = true;
            }


            //iterate through the openings
            foreach (PipeOpening opening in pipeOpenings)
            {
                //checks if opening is connected, open, and spawns a pour effect if it should
                //bool openingPour = opening.PropagatePourEffect(CurrentVolFlow);


                //checks if the current opening is in the list of inputs
                bool openingIsInput = flowInputs.Any(inopening => inopening == opening);

                //if so
                if (!openingIsInput)
                {



                    //if the opening doesn't have a pour effect object, and is connected
                    if (opening.PipeIsConnected)
                    {
                        //Debug.Log("checking flow input to " + opening.openingState.Location.name);
                        PipeConnections otherPipes = opening.openingData.Connection.pipeSegment;

                        //if the opening is open (and no pour effect, and is connected, and not an input)
                        if (opening.PipeIsOpen)
                        {

                            //we pass the current liquid data to the next segment
                            //the next segment then derives it's data from this
                            otherPipes.AddFlow(opening.openingData.Connection);



                        }
                        //if pipe is not open but other pipe has this as one of the input sources                        
                        else if (!opening.PipeIsOpen && otherPipes.flowInputs.Any(inputOpening => inputOpening.openingData.Connection.pipeSegment == this))
                        {
                            //need to remove flow from sealed outputs
                            otherPipes.RemoveFlow(opening.openingData.Connection);
                        }


                    }



                }


                //opening.PropagatePourEffect(PourEffect);
            }

        }
        //no flow, do not add to outputs
        else
        {
            Debug.Log("need to remove all flow related things here");

            foreach (PipeOpening opening in pipeOpenings)
            {

                //propagate flow through other pipe segments
                if (opening.openingData.Connection != null )
                {
                    PipeConnections otherPipes = opening.openingData.Connection.pipeSegment;

                   
                    //if there's flow present in the other pipes, and any of the inputs are connected to this
                    if (otherPipes.CurrentVolFlow.IsFlowPresent() && otherPipes.flowInputs.Any(inputOpening => inputOpening.openingData.Connection.pipeSegment == this))
                    {
                        otherPipes.RemoveFlow(opening.openingData.Connection);
                    }
                        
                }
            }

            //need to then propagate the flow for this particular segment to avoid issues where the liquid data doesn't get propagated properly
            CurrentVolFlow.propagateLiquidVol(this);

        }


        return connectionsChanged;


    }


}
