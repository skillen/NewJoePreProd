using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentActivationManager : MonoBehaviour
{
    public List<MonoBehaviour> Components;

    public List<GameObject> gameObjects;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableComponents()
    {
        foreach (MonoBehaviour component in Components)
        {
            component.enabled = true;
        }

        foreach (GameObject gameObject in gameObjects)
        {
            gameObject.SetActive(true);
        }
    }

    public void DisableComponents()
    {
        foreach (MonoBehaviour component in Components)
        {
            component.enabled = false;
        }

        foreach (GameObject gameObject in gameObjects)
        {
            gameObject.SetActive(false);
        }
    }
}
