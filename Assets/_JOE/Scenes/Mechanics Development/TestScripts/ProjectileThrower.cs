using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileThrower : MonoBehaviour
{

    public float ThrustValue = 20;

    [SerializeField]
    float AddedThrust = 0;

    [SerializeField]
    float totalThrust;

    public float TotalThrust => totalThrust;

    public void Awake()
    {
        AddedThrust = 0;
        totalThrust = ThrustValue + AddedThrust;
    }

    public void AddThrower(GameObject placeableObj)
    {
        ProjectileThrower thrower = placeableObj.GetComponent<ProjectileThrower>();

        if (thrower != null)
        {
            AddThrower(thrower);
        }
    }

    public void AddThrower(ProjectileThrower thrower)
    {
        if (thrower != null)
        {
            AddedThrust = thrower.totalThrust;

            RecalculateLaunchForce();
        }
    }

    public void RemoveThrower(GameObject placeableObj)
    {
        ProjectileThrower thrower = placeableObj.GetComponent<ProjectileThrower>();

        if (thrower != null)
        {
            RemoveThrower(thrower);
        }
    }

    public void RemoveThrower(ProjectileThrower thrower)
    {
        if (thrower != null)
        {
            AddedThrust = 0;

            RecalculateLaunchForce();
        }
    }

    public void RecalculateLaunchForce()
    {
        totalThrust = ThrustValue + AddedThrust;
    }


}
