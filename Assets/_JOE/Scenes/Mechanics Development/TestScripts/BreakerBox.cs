using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BreakerBox : MonoBehaviour
{

    public int currentThreshold = 0; 

    public List<FuseScript> fuses;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //called from object placement script events
    public void AddFuse(GameObject placeableObj)
    {
        FuseScript fuse = placeableObj.GetComponent<FuseScript>();

        if (fuse != null)
        {
            AddFuse(fuse);
        }
    }

    public void AddFuse(FuseScript fuse)
    {
        if (fuse != null)
        {
            fuses.Add(fuse);
            RecalculateCurrentThreshold();
        }
    }

    public void RemoveFuse(GameObject placeableObj)
    {
        FuseScript fuse = placeableObj.GetComponent<FuseScript>();

        if (fuse != null)
        {
            RemoveFuse(fuse);            
        }
    }

    public void RemoveFuse(FuseScript fuse)
    {
        if (fuse != null && fuses.Contains(fuse))
        {
            fuses.Remove(fuse);
            RecalculateCurrentThreshold();
        }
    }

    public void RecalculateCurrentThreshold()
    {
        int newThreshold = fuses.Select(x => x.currentThreshold).Sum();
        currentThreshold = newThreshold;
    }
}
