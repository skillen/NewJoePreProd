using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion;
using RootMotion.FinalIK;


public class TwoHandedPickUp : MonoBehaviour
{


    public InteractionObject obj; // The object to pick up
    public ColliderEventDispatcher triggerCollider; // Surrounding collider
    public Transform pivot; // The pivot point of the hand targets
    private Transform holdPoint; // The point where the object will lerp to when picked up
    public float pickUpTime = 0.3f; // Maximum lerp speed of the object. Decrease this value to give the object more weight

    private float holdWeight, holdWeightVel;
    private Vector3 pickUpPosition;
    private Quaternion pickUpRotation;

    [SerializeField] private InteractionSystem subscribedIS; // The InteractionSystem of the character


    void Start()
    {
        if (triggerCollider != null)
        {
            triggerCollider.OnTriggerEnterEvent += InteractionSystemSubscribe;
            triggerCollider.OnTriggerExitEvent += InteractionSystemUnsubscribe;
        }
    }

    private void InteractionSystemSubscribe(Collider other)
    {
        if (other.tag == "Player" && other.TryGetComponent<InteractionController>(out var controller))
        {
            subscribedIS = controller.interactionSystem;

            // Start listening to interaction events
            subscribedIS.OnInteractionStart += OnStart;
            subscribedIS.OnInteractionPause += OnPause;
            subscribedIS.OnInteractionResume += OnDrop;

            FindPlayerHoldPoint(controller);

            Debug.Log("InteractionSystemSubscribe sucessful" + "\n\n");
        }
    }

    private void InteractionSystemUnsubscribe(Collider other)
    {
        if (other.tag == "Player" && other.TryGetComponent<InteractionController>(out var controller))
        {
            if (subscribedIS == controller.interactionSystem)
            {
                // Stop listening to interaction events
                subscribedIS.OnInteractionStart -= OnStart;
                subscribedIS.OnInteractionPause -= OnPause;
                subscribedIS.OnInteractionResume -= OnDrop;

                subscribedIS = null;
                holdPoint = null;

                Debug.Log("InteractionSystemUnsubscribe sucessful" + "\n\n");
            }
        }
    }

    private void FindPlayerHoldPoint(InteractionController controller) {
        holdPoint = controller.GetComponent<HoldPointFor2Hands>().holdPoint;
    }


    // Called by the InteractionSystem when an interaction is paused (on trigger)
    private void OnPause(FullBodyBipedEffector effectorType, InteractionObject interactionObject)
    {
        if (effectorType != FullBodyBipedEffector.LeftHand) return;
        if (interactionObject != obj) return;

        // Make the object inherit the character's movement
        obj.transform.parent = subscribedIS.transform;

        // Make the object kinematic
        var r = obj.GetComponent<Rigidbody>();
        if (r != null) r.isKinematic = true;

        // Set object pick up position and rotation to current
        pickUpPosition = obj.transform.position;
        pickUpRotation = obj.transform.rotation;
        holdWeight = 0f;
        holdWeightVel = 0f;
    }

    // Called by the InteractionSystem when an interaction starts
    private void OnStart(FullBodyBipedEffector effectorType, InteractionObject interactionObject)
    {
        if (effectorType != FullBodyBipedEffector.LeftHand) return;
        if (interactionObject != obj) return;

        // Rotate the pivot of the hand targets
        // RotatePivot();

        // Rotate the hold point so it matches the current rotation of the object
        holdPoint.rotation = obj.transform.rotation;
    }

    // Called by the InteractionSystem when an interaction is resumed from being paused
    private void OnDrop(FullBodyBipedEffector effectorType, InteractionObject interactionObject)
    {
        Debug.Log( "OnDrop" + "\n\n" );
        if (holding) return;
        if (interactionObject != obj) return;

        // Make the object independent of the character
        obj.transform.parent = null;

        // Turn on physics for the object
        if (obj.GetComponent<Rigidbody>() != null) obj.GetComponent<Rigidbody>().isKinematic = false;
    }

    void LateUpdate()
    {
        if (subscribedIS != null)
        {
            if (holding)
            {
                // Smoothing in the hold weight
                holdWeight = Mathf.SmoothDamp(holdWeight, 1f, ref holdWeightVel, pickUpTime);

                // Interpolation
                obj.transform.position = Vector3.Lerp(pickUpPosition, holdPoint.position, holdWeight);
                obj.transform.rotation = Quaternion.Lerp(pickUpRotation, holdPoint.rotation, holdWeight);
            }
        }
    }

    // Are we currently holding the object?
    private bool holding
    {
        get
        {
            return holdingLeft || holdingRight;
        }
    }

    // Are we currently holding the object with left hand?
    private bool holdingLeft
    {
        get
        {
            return subscribedIS.IsPaused(FullBodyBipedEffector.LeftHand) && subscribedIS.GetInteractionObject(FullBodyBipedEffector.LeftHand) == obj;
        }
    }

    // Are we currently holding the object with right hand?
    private bool holdingRight
    {
        get
        {
            return subscribedIS.IsPaused(FullBodyBipedEffector.RightHand) && subscribedIS.GetInteractionObject(FullBodyBipedEffector.RightHand) == obj;
        }
    }

    /// Picking up a box shaped object with both hands.
    // Rotate the pivot of the hand targets by 90 degrees so we could grab the object from any direction
    private void RotatePivot()
    {
        // Get the flat direction towards the character
        Vector3 characterDirection = (pivot.position - subscribedIS.transform.position).normalized;
        characterDirection.y = 0f;

        // Convert the direction to local space of the object
        Vector3 characterDirectionLocal = obj.transform.InverseTransformDirection(characterDirection);

        // QuaTools.GetAxis returns a 90 degree ortographic axis for any direction
        Vector3 axis = QuaTools.GetAxis(characterDirectionLocal);
        Vector3 upAxis = QuaTools.GetAxis(obj.transform.InverseTransformDirection(subscribedIS.transform.up));

        // Rotate towards axis and upAxis
        pivot.localRotation = Quaternion.LookRotation(axis, upAxis);
    }

    private void OnDestroy()
    {
        if (triggerCollider != null)
        {
            triggerCollider.OnTriggerEnterEvent -= InteractionSystemSubscribe;
            triggerCollider.OnTriggerExitEvent -= InteractionSystemUnsubscribe;
        }
    }
}
