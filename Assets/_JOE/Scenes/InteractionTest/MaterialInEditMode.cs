using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteAlways]
public class MaterialInEditMode : MonoBehaviour {
    public Material material;
    public Transform meshesRoot;
    public bool runAgain;

    private void OnValidate() {

        if (material != null && meshesRoot != null) {

            MeshRenderer[] renderers = meshesRoot.GetComponentsInChildren<MeshRenderer>();
            if (renderers != null) {

                foreach (var item in renderers) {
                    if (item.sharedMaterial != material) {
                        item.sharedMaterial = material;
                    }
                }
            }
        }
    }
}
