using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class ColliderEventDispatcher : MonoBehaviour
{
    public bool debugMessages;

    public event Action<Collision> OnCollisionEnterEvent;
    public event Action<Collision> OnCollisionStayEvent;
    public event Action<Collision> OnCollisionExitEvent;
    public event Action<Collider> OnTriggerEnterEvent;
    public event Action<Collider> OnTriggerStayEvent;
    public event Action<Collider> OnTriggerExitEvent;


    private void OnCollisionEnter(Collision other)
    {
        if (debugMessages) { Debug.Log($"OnCollisionEnter on {gameObject.name} from {other.gameObject.name}" + "\n\n"); }

        OnCollisionEnterEvent?.Invoke(other);
    }

    private void OnCollisionStay(Collision other)
    {
        // if (debugMessages) { Debug.Log($"OnCollisionStay on {gameObject.name} from {other.gameObject.name}" + "\n\n"); }

        OnCollisionStayEvent?.Invoke(other);
    }

    private void OnCollisionExit(Collision other)
    {
        if (debugMessages) { Debug.Log($"OnCollisionExit on {gameObject.name} from {other.gameObject.name}" + "\n\n"); }

        OnCollisionExitEvent?.Invoke(other);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (debugMessages) { Debug.Log($"OnTriggerEnter on {gameObject.name} from {other.gameObject.name}" + "\n\n"); }
        
        OnTriggerEnterEvent?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        // if (debugMessages) { Debug.Log($"OnTriggerStay on {gameObject.name} from {other.gameObject.name}" + "\n\n"); }
        
        OnTriggerStayEvent?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (debugMessages) { Debug.Log($"OnTriggerExit on {gameObject.name} from {other.gameObject.name}" + "\n\n"); }
        
        OnTriggerExitEvent?.Invoke(other);
    }
}
