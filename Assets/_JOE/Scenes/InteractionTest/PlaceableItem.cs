using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

//[RequireComponent(typeof(PickUpItem))]
public class PlaceableItem : MonoBehaviour
{
    [Header("Variables")]
    public float mass = 1;


    //public List<PropTypes> PropTypes;

    public ObjectPlacement mountingPoint;



    [Header("Scene references")]
    public MeshFilter DisplayMesh;

    public PickUpItem pickUpItem;

    public Transform pivot;
    public Transform meshRoot;

    public Vector3 placementOffset
    {
        get
        {
            if (pivot != null)
            {
                return pivot.position - transform.position;
            }
            else
            {
                return transform.position;
            }
            
        }
    }


    public PlacementItemEvent ObjectPlacedEvent = new PlacementItemEvent();
    public PlacementItemEvent ObjectRemovedEvent = new PlacementItemEvent();


    [Header("Debug")]
    public Bounds itemBounds;
    public SlotPlacementSystem placementGrid;
    public List<Slot> placedOnSlots;


    [HideInInspector] public bool statePlacing = false;

    
    [HideInInspector] public List<MeshCollider> meshColliders;
    private BoxCollider outerCollider;
    [HideInInspector] public List<MeshFilter> meshes;




    private void Start()
    {

        //pickUpItem = GetComponent<PickUpItem>();
        //meshes = meshRoot.GetComponentsInChildren<MeshFilter>().ToList();

        //if (meshes != null)
        //{
        //    foreach (var item in meshes)
        //    {
        //        //item.gameObject.tag = "PickUpItem";
        //        if (item.GetComponent<MeshCollider>() == null)
        //        {
        //            MeshCollider collider = item.gameObject.AddComponent<MeshCollider>();
        //            collider.convex = true;
        //        }
        //    }
        //}

        //meshColliders = meshRoot.GetComponentsInChildren<MeshCollider>().ToList();

        //CalculateBounds();
        //AddOuterCollider();
    }


    public IEnumerator Place(Transform placePivot)
    {
        // Debug.Log("Start Place" + "\n\n");

        statePlacing = true;
        DisableCollider();


        pickUpItem.RemoveFromInventory();



        transform.parent = null;

        // set object transform to item offset - mounting point offset
        transform.position = placePivot.position - (pivot.position - transform.position);
        transform.rotation = Quaternion.Inverse(placePivot.rotation) * (Quaternion.Inverse(pivot.rotation) * meshes[0].transform.rotation);

        // IS THIS STILL NECESSARY ???
        // pickUpItem.interactionTrigger.transform.localEulerAngles = -transform.rotation.eulerAngles;

        while (pickUpItem.holdWeight > 0f)
        {
            pickUpItem.holdWeight -= Time.deltaTime * 3f;
            yield return null;
        }

        statePlacing = false;
        // Debug.Log("End Place" + "\n\n");
    }



    public void RemoveFromSocket()
    {
        if (mountingPoint != null)
        {
            //questionable doing this here, but may be neccessary to access object functions
            mountingPoint.gameObject.SetActive(true);

            mountingPoint.RemoveItem(this);

            mountingPoint = null;
        }

    }



    //enable kinematic when dropped
    //call from pick up item
    public async void EnableKinematicAsync()
    {



        Rigidbody rb = GetComponent<Rigidbody>();

        while (rb == null)
        {
            rb = GetComponent<Rigidbody>();
            await Task.Yield();
        }



        if (rb != null)
        {
            //puppetmaster pickup/drop - the puppetmaster system itself
            //adds and removes rigidbody, so this will not work here

            //set the object as dynamic again
            rb.isKinematic = false;
        }



    }



    ////generates bounds based on a composite of the children meshes
    //private void CalculateBounds()
    //{

    //    List<MeshRenderer> renderers = meshRoot.GetComponentsInChildren<MeshRenderer>().ToList();
    //    if (renderers.Count > 0)
    //    {

    //        Quaternion prevRot = renderers[0].transform.localRotation;
    //        renderers[0].transform.localRotation = Quaternion.Euler(Vector3.zero);

    //        itemBounds = renderers[0].bounds;

    //        for (int i = 1; i < renderers.Count; i++)
    //        {
    //            itemBounds.Encapsulate(renderers[i].bounds);
    //        }

    //        renderers[0].transform.localRotation = prevRot;
    //    }
    //}

    //public void AddOuterCollider()
    //{

    //    if (meshColliders.Count > 0)
    //    {
    //        foreach (var item in meshColliders)
    //        {
    //            item.isTrigger = true;
    //        }
    //    }
    //    else
    //    {
    //        Debug.LogError("there are no meshcollider assigned ", this.gameObject);
    //    }

    //    outerCollider = gameObject.AddComponent<BoxCollider>();
    //    outerCollider.size = itemBounds.size;
    //    outerCollider.isTrigger = false;

    //    Rigidbody rb = gameObject.AddComponent<Rigidbody>();
    //    rb.mass = mass;
    //    rb.drag = 1;

    //    // Debug.Log( "Added Outer Collider" + "\n\n" );
    //}

    public void EnableCollider()
    {
        outerCollider.isTrigger = false;
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }

    private void DisableCollider()
    {
        outerCollider.isTrigger = true;
    }


    private void OnJointBreak(float breakForce)
    {
        Debug.Log("OnJointBreak - Force: " + breakForce + "\n\n");
        if (placementGrid != null)
        {
            placementGrid.OnPickUp(this, placedOnSlots);
        }
        EnableCollider();
    }


    public void PickupFromPlacementGrid()
    {
        // Debug.Log( "PickupFromPlacementGrid" + "\n\n" );
        DisableCollider();

        if (placementGrid != null)
        {
            placementGrid.OnPickUp(this, placedOnSlots);
        }
    }
}
