﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Rendering.PostProcessing;

using RootMotion.Dynamics;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Coffee
{
    [CreateAssetMenu(fileName = "New Player Settings", menuName = "Coffee/Player Settings", order = 1)]
    public class CoffeePlayerSettings : ScriptableObject
    {

        // ---------------------------------------------------------------

        //[Header("Setup")]
        //public LevelData tutorialLevelData = null;

        [Tooltip("If true, player character will not react to player input until an event (like coffee loadout selected) enables it.")]
        public bool startFrozen = false;

        // ---------------------------------------------------------------

        [Header("Input")]
        public float recoverDelayTime = 1.0f; // How long to wait after puppet recovery before move input is taken into account again.
        [Range(0.1f, 15f)] public float turnNoiseSpeed = 3f;
        [Range(0f, 135f)] public float maxTurnNoiseAngle = 45f;

        //TODO: Rename
        [Tooltip("How much time has to pass since last coffee sip to be able to safely sip again. Sipping too fast causes game over.")]
        [Range(0.1f, 5f)] public float poopShootSipInterval = 2f;

        [Tooltip("If true, direction character moves will match player's input less and less as character becomes more fatigued.")]
        public bool randomizeDirection = false;

        [Range(0.1f, 1.0f)] public float digitalWalkMagnitude = 0.5f;

        // ---------------------------------------------------------------

        [Header("Fatigue")]
        [Range(0.001f, 0.1f)] public float fatigueRate = 0.0325f; // How fast character becomes fatigued.
        [Range(0f, 1f)] public float coffeeFatigueValue = 0.3f;       // Reduce fatigue by this amount after each sip of coffee.

        // ---------------------------------------------------------------

        [Header("Puppet")]
        [Range(0.1f, 0.9f)] public float minPinWeight = 0.5f;
        public float minCollisionResistance = 500f;
        public float maxCollisionResistance = 1500f;

        [Tooltip("This is how much force to apply when player moves off of something like a building roof. It will push character forward in whatever direction character is facing. Force is multiplied by input value.")]
        public float moveOffEdgeForce = 1f;

        // ---------------------------------------------------------------

        [Header("Image Effects")]
        //public PostProcessProfile globalPostProcessProfile = null;
        [Space]
        public bool useDoubleVisionEffect = false;
        public float maxDoubleVision = 0.35f;
        [Range(0.1f, 5f)] public float doubleVisionSpeed = 1f;      // How fast double vision oscillates from 0 to value from curve based on player energy.
        [Space]
        public bool useKnockdownSplashEffect;
        public float knockdownSplashFadeTime = 2f;

        // ---------------------------------------------------------------

        [Header("Ground Check")]
        public bool checkGround = false;
        public LayerMask groundCheckLayers;
        [Range(0.1f, 25f)] public float groundCheckAdjustmentSpeed = 10f;
        [Range(0f, 2f)] public float groundCheckRayStartOffsetY = 0.5f;
        [Range(0.1f, 10f)] public float groundCheckRayDistance = 7.5f;

        // ---------------------------------------------------------------

        [Header("Camera")]
        public bool invertFreeLookCameraY = false;
        [Range(0f, 1f)] public float cameraDamping = 0f;

        // ---------------------------------------------------------------

        [Header("Fire")]
        public float fireBodyMaxScale = 5f;
        public float fireGameOverTime = 10f;  // How many seconds player can be on fire for before game over.

        // ---------------------------------------------------------------

        private static string newSettingsDataPath = "Assets/_COFFEE/_Release/Data/Player/";

        // ---------------------------------------------------------------

#if UNITY_EDITOR
        [MenuItem("Coffee/Create Player Settings")]
        public static void CreateMyAsset()
        {

            CoffeePlayerSettings asset = CreateInstance<CoffeePlayerSettings>();

            AssetDatabase.CreateAsset(asset, newSettingsDataPath + "new_player_settings.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }
#endif
    }
}